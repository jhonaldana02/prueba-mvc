<?php 

class conexion{

	private $conexion;
	// private $host = "server3.hostingfacil.co";
	// private $user = "jhonaldana_user_jm";
	// private $pass = "Jhonaldana2017";
	// private $db = "jhonaldana_sistembd_jm";

	private $host = "localhost";
	private $user = "root";
	private $pass = "";
	private $db = "procesosmvc";

	private $usuario;
	private $password;

		public function __construct(){

			session_start();
			$this->conexion = new mysqli($this->host, $this->user, $this->pass, $this->db);
			$this->conexion->set_charset("utf8");
			if($this->conexion->connect_errno){
				die("FALLO CONEXION A LA BASE DE DATOS: (". $this->conexion->connect_errno.")");
			}
		}

		public function cerrar(){
			$this->conexion->close();
		}

// FUNCION PARA INICIO DE SESION

	public function login($usuario, $contrasena){

		// 1 = Datos correctos
		// 2 = Datos incorrectos

		$query = "SELECT ID_USUARIO, IDENTIFICACION, NOMBRES, APELLIDOS, USUARIO, CONTRASENA 
				  FROM USUARIO 
				  WHERE 
				  USUARIO = '".$usuario."' AND 
				  CONTRASENA = '".$contrasena."'
				  ";
		// exit($query);

		$consulta = $this->conexion->query($query);

			if($row = mysqli_fetch_array($consulta)){ // Consulta se transforme en un array y guarda en row
				// Si existen datos se crean sesiones
					echo 1;
					$_SESSION["ID_USUARIO"] = $row['ID_USUARIO'];
					$_SESSION["IDENTIFICACION"] = $row['IDENTIFICACION'];
					$_SESSION["NOMBRES"] = $row['NOMBRES'];
					$_SESSION["APELLIDOS"] = $row['APELLIDOS'];	
					$_SESSION["USUARIO"] = $row['USUARIO'];

			}else{
				echo 2;
				 }

	} //End function login



// LISTADO DE PROCESO DEL USUARIO

	public function proceso_usuario($id_usuario){

		$query_vista = "SELECT P.ID_REGISTRO, P.NUM_PROCESO, P.DESCRIPCION, P.FECHA_CREACION, P.SEDE, FORMAT(P.PRESUPUESTO, 2) PRESUPUESTO, P.ID_USUARIO
						FROM REGISTRO P
						WHERE P.ID_USUARIO = ".$id_usuario ;

						// exit($query_vista);

		$vista = $this->conexion->query($query_vista);
		$array= array();

		while($row = $vista->fetch_assoc()){
			$array[] = $row;
		}

		$json_query = json_encode($array);
		$json_vista = html_entity_decode(json_encode($json_query));
		echo $json_query;	 

	} // End function ListadoProductos



// LISTADO DE PROCESO ENGENERAL

	public function listado_procesos(){

		$query_vista = "SELECT P.ID_REGISTRO, P.NUM_PROCESO, P.DESCRIPCION, P.FECHA_CREACION, P.SEDE, FORMAT(P.PRESUPUESTO, 2) PRESUPUESTO, P.ID_USUARIO,
		                       U.NOMBRES, U.APELLIDOS
						FROM REGISTRO P
						INNER JOIN USUARIO U
						ON P.ID_USUARIO = U.ID_USUARIO
						ORDER BY P.FECHA_CREACION DESC ";

						// exit($query_vista);

		$vista = $this->conexion->query($query_vista);
		$array= array();

		while($row = $vista->fetch_assoc()){
			$array[] = $row;
		}

		$json_query = json_encode($array);
		$json_vista = html_entity_decode(json_encode($json_query));
		echo $json_query;	 

	} // End function ListadoProductos	


// VALIDAR ULTIMO PROCESO REGISTRADO - SI NO EXISTE GENERA UNO POR DEFECTO

	public function validar_ultimo_proceso(){

		$query = "SELECT COUNT(P.NUM_PROCESO) NUM_PROCESO
				  FROM PROCESO P ";
		// exit($query);

		$consulta = $this->conexion->query($query);
		$row = mysqli_fetch_array($consulta);
		$num_procesos = $row['NUM_PROCESO'];

		if($num_procesos == 0){
			// echo 'No existen procesos';
			$proceso = '12345678';

			// INSERTAMOS POR PRIMERA VEZ EL PROCESO
			$query_registro = "INSERT INTO PROCESO (NUM_PROCESO)
								VALUES (
								'".$proceso."'
								)";
								// exit($query_registro);
			$result_query = $this->conexion->query($query_registro);

			
			// LO CONSULTAMOS PARA MOSTRARSELO AL USUARIO
			$query_vista = "SELECT MAX(P.NUM_PROCESO) NUM_PROCESO
							FROM PROCESO P; ";

						// exit($query_vista);

			$vista = $this->conexion->query($query_vista);
			$array= array();

			while($row = $vista->fetch_assoc()){
				$array[] = $row;
			}

			$json_query = json_encode($array);
			$json_vista = html_entity_decode(json_encode($json_query));
			echo $json_query;	


		}else{
			// echo 'Existen registros';
			// SI EXISTEN PROCESOS, SE CONSULTA EL ULTIMO Y LE SUMAMOS EL CONSECUTIVO
			// LO CONSULTAMOS PARA MOSTRARSELO AL USUARIO
			$query_vista = "SELECT MAX(P.NUM_PROCESO) NUM_PROCESO
							FROM PROCESO P ";
						// exit($query_vista);

			$vista = $this->conexion->query($query_vista);

			$row = mysqli_fetch_array($vista);
		    $num_procesos = $row['NUM_PROCESO'] + 1;

		    // LO INSERTAMOS

		    $query_registro = "INSERT INTO PROCESO (NUM_PROCESO)
								VALUES (
								'".$num_procesos."'
								)";
								// exit($query_registro);
			$result_query = $this->conexion->query($query_registro);

			echo $num_procesos;

		    
		}


	} // End function validar_ultimo_proceso



public function nuevo_proceso($proceso,$descripcion,$sede,$presupuesto,$id_usuario){


		$query_registro = "INSERT INTO REGISTRO (NUM_PROCESO, DESCRIPCION, FECHA_CREACION, SEDE, PRESUPUESTO, ID_USUARIO) 
							VALUES (
							'".$proceso."',
							'".$descripcion."', 
							NOW(), 
							'".$sede."',
							".$presupuesto.",
							".$id_usuario."	
							)";
							// exit($query_registro);

		$result_query = $this->conexion->query($query_registro);



	} // End function MoverProductos	








} // Fin clase conexion

?>