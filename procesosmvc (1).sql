-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-10-2018 a las 08:52:50
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `procesosmvc`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso`
--

CREATE TABLE IF NOT EXISTS `proceso` (
  `NUM_PROCESO` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proceso`
--

INSERT INTO `proceso` (`NUM_PROCESO`) VALUES
('12345678'),
('12345679'),
('12345680'),
('12345681'),
('12345682'),
('12345683'),
('12345684'),
('12345685'),
('12345686'),
('12345687'),
('12345688'),
('12345689'),
('12345690'),
('12345691'),
('12345692');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro`
--

CREATE TABLE IF NOT EXISTS `registro` (
  `ID_REGISTRO` int(11) NOT NULL,
  `NUM_PROCESO` varchar(8) NOT NULL,
  `DESCRIPCION` varchar(200) NOT NULL,
  `FECHA_CREACION` datetime NOT NULL,
  `SEDE` varchar(45) NOT NULL,
  `PRESUPUESTO` decimal(10,0) NOT NULL,
  `ID_USUARIO` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `registro`
--

INSERT INTO `registro` (`ID_REGISTRO`, `NUM_PROCESO`, `DESCRIPCION`, `FECHA_CREACION`, `SEDE`, `PRESUPUESTO`, `ID_USUARIO`) VALUES
(5, '12345682', 'Prueba descri´´cion', '2018-10-19 01:23:36', 'Perú', '85000', 1),
(6, '12345678', 'Prueba para usuario id 2', '2018-10-19 01:25:36', 'México', '90000', 2),
(10, '12345692', 'Prueba 2 usuario2 asdf asdf safs a fa', '2018-10-19 01:48:40', 'México', '412000', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `ID_USUARIO` int(11) NOT NULL,
  `IDENTIFICACION` int(11) NOT NULL,
  `NOMBRES` varchar(50) NOT NULL,
  `APELLIDOS` varchar(50) NOT NULL,
  `USUARIO` varchar(45) NOT NULL,
  `CONTRASENA` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`ID_USUARIO`, `IDENTIFICACION`, `NOMBRES`, `APELLIDOS`, `USUARIO`, `CONTRASENA`) VALUES
(1, 1012358158, 'Jhon Alexander', 'Aldana', 'jhonaldana', '123456'),
(2, 1144444554, 'Pedro julian', 'Ramirez', 'pedro', '123456'),
(3, 1122334455, 'Usuario', 'Prueba', 'usuario', '123456'),
(4, 2147483647, 'Usuario 2', 'Prueba 2', 'usuario2', '123456');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD PRIMARY KEY (`NUM_PROCESO`);

--
-- Indices de la tabla `registro`
--
ALTER TABLE `registro`
  ADD PRIMARY KEY (`ID_REGISTRO`), ADD KEY `fk_REGISTRO_USUARIO_idx` (`ID_USUARIO`), ADD KEY `fk_REGISTRO_PROCESO1_idx` (`NUM_PROCESO`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`ID_USUARIO`), ADD UNIQUE KEY `IDENTIFICACION_UNIQUE` (`IDENTIFICACION`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `registro`
--
ALTER TABLE `registro`
  MODIFY `ID_REGISTRO` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `ID_USUARIO` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `registro`
--
ALTER TABLE `registro`
ADD CONSTRAINT `fk_REGISTRO_PROCESO1` FOREIGN KEY (`NUM_PROCESO`) REFERENCES `proceso` (`NUM_PROCESO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_REGISTRO_USUARIO` FOREIGN KEY (`ID_USUARIO`) REFERENCES `usuario` (`ID_USUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
