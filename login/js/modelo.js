var appLogin = angular.module ("Login",['ngRoute']);

	appLogin.config(function($routeProvider) {

		$routeProvider

	    .when("/", {
	        templateUrl : "login.php",
	        controller : "loginCtrl"
	    })

	    .otherwise({
	    	templateUrl : "login.php"
	    });

	});