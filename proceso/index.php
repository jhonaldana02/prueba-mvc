<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->  <!--<![endif]-->
<html lang="en" ng-app ="PruebaMVC">
<head>
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" type="image/png" href="img/favicon.ico" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="../assets/css/animate.css">
    <link rel="stylesheet" href="../assets/css/alert/sweetalert/sweetalert.css">
    <link rel="stylesheet" href="../assets/css/fontawesome/css/font-awesome.min.css">
  <title>Bienvenido</title>
</head>         

<body>

  <div ng-view></div>
  <script src = "../assets/js/jquery.js"></script>
  <script src = "../assets/js/materialize/js/materialize.js"></script>
  <script src = "../assets/js/angular/angular.min.js"></script>
  <script src = "../assets/js/angular/angular_ngRoute.js"></script>
  <script src = "../assets/js/alert/sweetalert/sweetalert.js"></script>
  <script src = "js/modelo_gen.js"></script>
  <script src = "js/ctrl_gen.js"></script>

</body>
</html>