// CONTROLADOR PRINCIPAL DE PROCESOS

appGeneral.controller('CtrlProcesos',['$http', '$scope', 'Proceso', function($http, $scope, Proceso){

	// Variable donde se almacenan los sql
    var request = '../request/sql/';

// FUNCION PARA CONSULTAR EL PROCESO REGISTRADO POR EL USUARIO ************************************************************
	
	$scope.proceso_usuario = function(){

		var id_usuario = $('#id_usuario').val();

		Proceso.list_proceso_usuario(id_usuario).success(function(data){
			
			// console.log(data);			

			if(data == ''){
				// console.log('No tiene proceso registrado');
				$scope.viewproceso = false;
				$scope.noproceso = true;
			}else{
				$scope.noproceso = false;
				$scope.viewproceso = true;
				$scope.procesousers = data;		
			}


		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.proceso_usuario();


// FUNCION PARA CONSULTAR LOS PROCESOS REGISTRADOS ************************************************************
	
	$scope.listado_procesos = function(){

		Proceso.listado_procesos_gen().success(function(data){
			
			// console.log(data);		

			if(data == ''){
				console.log('No hay procesos registrado');
				$scope.viewprocesogen = false;
				$scope.noprocesogen = true;
			}else{
				$scope.noprocesogen = false;
				$scope.viewprocesogen = true;
				$scope.listadoprocesos = data;		
			}


		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.listado_procesos();

// FUNCION PARA CONSULTAR LOS PROCESOS REGISTRADOS ************************************************************
	
	$scope.validar_ultimo_proceso = function(){

		Proceso.ultimo_proceso().success(function(data){
			
			// console.log(data);
			$scope.ultimo_proceso = data;

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.listado_procesos();		


	$scope.nuevo_proceso = function(){


		var proceso = $('#proceso').val();
		var id_usuario = $('#id_usuario').val();
		var descripcion = $scope.descripcion;
		var sede = $scope.sede;
		var newpresupuesto = $scope.presupuesto;

		var presupuesto = newpresupuesto;
		var res_presupuesto = newpresupuesto.replace(',', "");
		presupuesto = res_presupuesto;

		var datos = {
			proceso:proceso,
			descripcion:descripcion,
			sede:sede,
			presupuesto:presupuesto,
			id_usuario:id_usuario
		}

		// console.log(datos);

		$http({
			method:'POST',
			url: request + 'proceso/nuevo_proceso.php',
			header:{
			'Content-Type': undefined
		},
			data:datos

			}).success(function(respuesta){
				$scope.procesos = respuesta;
				console.log(respuesta)
			
				window.location = "../proceso/#";

			}).error(function(err){

				console.log("Error: "+err);
			});	

		
	}

}]); // End Controller CtrlProcesos
