var appGeneral = angular.module("WebGen",['ngRoute']);

    appGeneral.config(function($routeProvider) {
        
        $routeProvider

        .when("/", {
            templateUrl : "plantillas/contenido/ContPrincipal.php",
            controller : "ContPrincipalCtrl"
        })

        .when("/pagos/", {
            templateUrl : "plantillas/contenido/pagos/pruebapagos.php",
            controller : "PagosCtrl"
        })

        .when("/pagos/perfecto/", {
            templateUrl : "plantillas/contenido/pagos/pagoperfecto.php",
            controller : "PagosCorrectCtrl"
        })

        .when("/pagos/crearpago/", {
            templateUrl : "plantillas/contenido/pagos/crearpago.php"
            // controller : "PagosCorrectCtrl"
        })

// PRODUCTOS *******************************************************************************+

        .when("/productos/", {
            templateUrl : "plantillas/contenido/productos/ListProductos.php",
            controller : "ProductosCtrl"
        })
        .when("/NewProduct", {
            templateUrl : "plantillas/contenido/productos/NewProduct.php",
            controller : "ProductosCtrl"
        })
        .when("/EditProduct/:idproductos", {
            templateUrl : "plantillas/contenido/productos/EditProducto.php",
            controller : "ProductosCtrl"
        })
        .when("/entradas", {
            templateUrl : "plantillas/contenido/productos/ListEntradasProduct.php",
            controller : "ProductosCtrl"
        })
        .when("/EntradaServ", {
            templateUrl : "plantillas/contenido/productos/ListEntradasProductServ.php",
            controller : "ProductosCtrl"
        })
        .when("/NewEntrada", {
            templateUrl : "plantillas/contenido/productos/NewEntradaProd.php",
            controller : "ProductosCtrl"
        })
        .when("/NewEntradaServ", {
            templateUrl : "plantillas/contenido/productos/NewEntradaProdServ.php",
            controller : "ProductosCtrl"
        })
        .when("/EditEntrada/:idingreso_productos/:idproductos", {
            templateUrl : "plantillas/contenido/productos/EditEntradaProduct.php",
            controller : "ProductosCtrl"
        })
        .when("/ProductVitrina", {
            templateUrl : "plantillas/contenido/productos/ListProductosVitrina.php",
            controller : "ProductosCtrl"
        })
        .when("/NewProductVitrina", {
            templateUrl : "plantillas/contenido/productos/NewProductVitrina.php",

            controller : "ProductosCtrl"
        })
        .when("/ProductService", {
            templateUrl : "plantillas/contenido/productos/ListProductosServ.php",
            controller : "ProductosCtrl"
        })
        .when("/NewProductServ", {
            templateUrl : "plantillas/contenido/productos/NewProductServ.php",
            controller : "ProductosCtrl"
        })
        .when("/VentaProduct", {
            templateUrl : "plantillas/contenido/productos/ListVentasProduct.php",
            controller : "ProductosCtrl"
        })
        .when("/NewVentaProduct", {
            templateUrl : "plantillas/contenido/productos/NewVentaProd.php",
            controller : "ProductosCtrl"
        })
        .when("/ControlVentas", {
            templateUrl : "plantillas/contenido/productos/ListControlVentas.php",
            controller : "ControlCtrl"
        })
        .when("/inventory", {
            templateUrl : "plantillas/contenido/productos/inventario/ListInventario.php",
            controller : "ProductosCtrl"
        })
        .when("/InventoryServ", {
            templateUrl : "plantillas/contenido/productos/inventario/ListInventarioServ.php",
            controller : "ProductosCtrl"
        })
        .when("/DetalleSalidaProduct/:idproductos", {
            templateUrl : "plantillas/contenido/productos/inventario/DetalleSalidaProduct.php",
            controller : "ProductosCtrl"
        })
        
        // PORCIONES
        .when("/Productos&Porciones/", {
            templateUrl : "plantillas/contenido/productos/ListControlProdPorciones.php",
            controller : "ControlCtrl"
        })

// EMPLEADOS *******************************************************************************+

        .when("/employee", {
            templateUrl : "plantillas/contenido/empleados/ListEmployee.php",
            controller : "EmpleadosCtrl"
        })
        .when("/NewEmployee", {
            templateUrl : "plantillas/contenido/empleados/NewEmployer.php",
            controller : "EmpleadosCtrl"
        })
        .when("/EditEmployee/:idempleados", {
            templateUrl : "plantillas/contenido/empleados/EditEmployee.php",
            controller : "EmpleadosCtrl"
        })

// CLIENTES *******************************************************************************+

        .when("/clientes", {
            templateUrl : "plantillas/contenido/clientes/ListClientes.php",
            controller : "ClientesCtrl"

        })
        .when("/NewCliente", {
            templateUrl : "plantillas/contenido/clientes/NewCliente.php",
            controller : "ClientesCtrl"
        })
        .when("/ServicesClient/:idclientes", {
            templateUrl : "plantillas/contenido/clientes/ServiciosClientes.php",
            controller : "ClientesCtrl"
        })

// SERVICIOS *******************************************************************************+

        .when("/services", {
            templateUrl : "plantillas/contenido/servicios/ListServicios.php",
            controller : "ServiciosCtrl"
        })
        .when("/NewService", {
            templateUrl : "plantillas/contenido/servicios/NewService.php",
            controller : "ServiciosCtrl"
        })
        .when("/OtrosServices", {
            templateUrl : "plantillas/contenido/servicios/ListOtrosServicios.php",
            controller : "ServiciosCtrl"
        })
        .when("/NewOtroService", {
            templateUrl : "plantillas/contenido/servicios/NewOtroService.php",
            controller : "ServiciosCtrl"
        })
        .when("/ControlService", {
            templateUrl : "plantillas/contenido/servicios/ListControlService.php",
            controller : "ControlCtrl"
        })
        .when("/DetalleService/:idempleados/:idservicio", {
            templateUrl : "plantillas/contenido/servicios/Detalle_Servicio.php",
            controller : "ServiciosCtrl"
        })
        .when("/ControlOtroService", {
            templateUrl : "plantillas/contenido/servicios/ListControlOtroService.php",
            controller : "ControlCtrl"
        })
        .when("/Propinas", {
            templateUrl : "plantillas/contenido/servicios/ListPropinas.php",
            controller : "ServiciosCtrl"
        })
        .when("/ControlPropinas", {
            templateUrl : "plantillas/contenido/servicios/ListControlPropinas.php",
            controller : "ControlCtrl"
        })
        .when("/NewPropina", {
            templateUrl : "plantillas/contenido/servicios/NewPropina.php",
            controller : "ServiciosCtrl"
        })

// CONTROL *******************************************************************************+

        .when("/control", {
            templateUrl : "plantillas/contenido/control/Control.php",
            controller : "ControlCtrl"
        })
        .when("/control1", {
            templateUrl : "plantillas/contenido/control/Control.php",
            controller : "ControlCtrl"
        })
        .when("/control2", {
            templateUrl : "plantillas/contenido/control/Control.php",
            controller : "ControlCtrl"
        })
        .when("/control3", {
            templateUrl : "plantillas/contenido/control/Control.php",
            controller : "ControlCtrl"
        })
        .when("/ControlDay", {
            templateUrl : "plantillas/contenido/control/ControlCajaDay.php",
            controller : "ControlCtrl"
        })
        .when("/LiquidacionIndiv/:idempleados", {
            templateUrl : "plantillas/contenido/control/LiquidacionIndividual.php",
            controller : "ControlCtrl"
        })
        .when("/LiquidacionIndivDate/:idempleados/:fecha", {
            templateUrl : "plantillas/contenido/control/LiquidacionIndividualDate.php",
            controller : "ControlCtrl"
        })
        .when("/LiquidacionIndivDateGen/:idempleados/:fecha", {
            templateUrl : "plantillas/contenido/control/LiquidacionIndividualDateGen.php",
            controller : "ControlCtrl"
        })
        .when("/LiquidacionIndivAux/:idempleados", {
            templateUrl : "plantillas/contenido/control/LiquidacionIndividualAux.php",
            controller : "ControlCtrl"
        })
        .when("/Vales", {
            templateUrl : "plantillas/contenido/control/ListVales.php",
            controller : "ControlCtrl"
        })
        .when("/ControlVales", {
            templateUrl : "plantillas/contenido/control/ListControlVales.php",
            controller : "ControlCtrl"
        })
        .when("/NewVale", {
            templateUrl : "plantillas/contenido/control/NewVale.php",
            controller : "ControlCtrl"
        })
        .when("/EditVale/:idvales", {
            templateUrl : "plantillas/contenido/control/EditVale.php",
            controller : "ControlCtrl"
        })
        .when("/Prestamos", {
            templateUrl : "plantillas/contenido/control/ListPrestamos.php",
            controller : "ControlCtrl"
        })
        .when("/ControlPrestamo", {
            templateUrl : "plantillas/contenido/control/ListControlPrestamos.php",
            controller : "ControlCtrl"
        })
        .when("/EditPrestamo/:idprestamos", {
            templateUrl : "plantillas/contenido/control/EditPrestamo.php",
            controller : "ControlCtrl"
        })
        .when("/NewPrestamo", {
            templateUrl : "plantillas/contenido/control/NewPrestamo.php",
            controller : "ControlCtrl"
        })
        .when("/ListAbonos", {
            templateUrl : "plantillas/contenido/control/ListAbonosDia.php",
            controller : "ControlCtrl"
        })
        .when("/ControlAbonos", {
            templateUrl : "plantillas/contenido/control/ListControlAbonos.php",
            controller : "ControlCtrl"
        })
        .when("/CajaInterna", {
            templateUrl : "plantillas/contenido/control/CajaInterna.php",
            controller : "ControlCtrl"
        })
        .when("/NewEntradaCajaInt", {
            templateUrl : "plantillas/contenido/control/NewEntradaCaja.php",
            controller : "ControlCtrl"
        })
        .when("/NewSalidaCajaInt", {
            templateUrl : "plantillas/contenido/control/NewSalidaCaja.php",
            controller : "ControlCtrl"
        })
        .when("/AbonoPrestamo/:idprestamos", {
            templateUrl : "plantillas/contenido/control/AbonoPrestamo.php",
            controller : "ControlCtrl"
        })
        .when("/RegisterCaja", {
            templateUrl : "plantillas/contenido/control/ListRegistroCaja.php",
            controller : "ControlCtrl"
        })
        .when("/ControlRegisterCaja", {
            templateUrl : "plantillas/contenido/control/ListControlRegistroCaja.php",
            controller : "ControlCtrl"
        })
        .when("/ControlRegisterCajaGen", {
            templateUrl : "plantillas/contenido/control/ListControlRegistroCajaGen.php",
            controller : "ControlCtrl"
        })
        .when("/ControlIngDay", {
            templateUrl : "plantillas/contenido/control/IngresosDay.php",
            controller : "ControlCtrl"
        })


        .when("/ControlIngDayValor", {
            templateUrl : "plantillas/contenido/control/IngresosValores/IngresosDayValores.php",
            controller : "ControlCtrl"
        })
        .when("/ControlIngDateValor", {
            templateUrl : "plantillas/contenido/control/IngresosValores/IngresosDateValores.php",
            controller : "ControlCtrl"
        })

        .when("/ControlIngDate", {
            templateUrl : "plantillas/contenido/control/IngresosDate.php",
            controller : "ControlCtrl"
        })
        .when("/OtrosIngresos", {
            templateUrl : "plantillas/contenido/control/ListOtrosIngresos.php",
            controller : "ControlCtrl"
        })
        .when("/ControlOtroIngerso", {
            templateUrl : "plantillas/contenido/control/ListControlOtrosIngresos.php",
            controller : "ControlCtrl"
        })
        .when("/NewOtroIngreso", {
            templateUrl : "plantillas/contenido/control/NewOtroIngreso.php",
            controller : "ControlCtrl"
        })
        .when("/EditIngreso/:idotros_ingresos", {
            templateUrl : "plantillas/contenido/control/EditOtroIngreso.php",
            controller : "ControlCtrl"
        })
        .when("/SalidasDay", {
            templateUrl : "plantillas/contenido/control/ListSalidas.php",
            controller : "ControlCtrl"
        })
        .when("/EditSalida/:idsalidas_internas", {
            templateUrl : "plantillas/contenido/control/EditSalidaDia.php",
            controller : "ControlCtrl"
        })
        .when("/ControlSalidasdia", {
            templateUrl : "plantillas/contenido/control/ListControlSalidas.php",
            controller : "ControlCtrl"
        })
        .when("/NewSalida", {
            templateUrl : "plantillas/contenido/control/NewSalida.php",
            controller : "ControlCtrl"
        })

// Otros Registros **********************************************************************

// Notificaciones
        
        .when("/Control/OtrosReg/Notificaciones", {
            templateUrl : "plantillas/contenido/control/ListNotificacionesDia.php",
            controller : "ControlCtrl"
        })
        .when("/Control/OtrosReg/NewNotificacion", {
            templateUrl : "plantillas/contenido/control/NewNotificacion.php",
            controller : "ControlCtrl"
        })
        .when("/Control/OtrosReg/EditNotif/:idnotificaciones", {
            templateUrl : "plantillas/contenido/control/EditNotificacion.php",
            controller : "ControlCtrl"
        })
        .when("/Control/OtrosReg/ControlNotificacion", {
            templateUrl : "plantillas/contenido/control/ListControlNotificaciones.php",
            controller : "ControlCtrl"
        })

        // Pagos y préstamos Mensuales
        
        .when("/Control/OtrosReg/Pagos&Prestamos", {
            templateUrl : "plantillas/contenido/control/PagosPrestamos/ListPagosPrestamos.php",
            controller : "ControlCtrl"
        })

        .when("/Control/OtrosReg/ControlPagos&Prestamos", {
            templateUrl : "plantillas/contenido/control/PagosPrestamos/ListControlPagosPrestamos.php",
            controller : "ControlCtrl"
        })

        .when("/Control/OtrosReg/NewPrestamoQuincena", {
            templateUrl : "plantillas/contenido/control/PagosPrestamos/NewPrestamoQuincena.php",
            controller : "ControlCtrl"
        })

        .when("/Control/OtrosReg/ListPagosQuincenales", {
            templateUrl : "plantillas/contenido/control/PagosPrestamos/ListPagosQuincenales.php",
            controller : "ControlCtrl"
        })

        .when("/Control/OtrosReg/ControlPagosQuincenales", {
            templateUrl : "plantillas/contenido/control/PagosPrestamos/ListControlPagosQuincenales.php",
            controller : "ControlCtrl"
        })

        .when("/Control/OtrosReg/PagosQuincenales", {
            templateUrl : "plantillas/contenido/control/PagosPrestamos/RegistroPagoQuincenal.php",
            controller : "ControlCtrl"
        })

        // USUARIOS

        .when("/Control/OtrosReg/GestionUsers", {
            templateUrl : "plantillas/contenido/control/Usuarios/ListUsuarios.php",
            controller : "ControlCtrl"
        })
        // NEW USUARIO
        .when("/Control/OtrosReg/GestionUsers/NewUsuario", {
            templateUrl : "plantillas/contenido/control/Usuarios/NewUsuario.php",
            controller : "ControlCtrl"
        })

        // GESTION SERVICIOS
        .when("/Control/OtrosReg/GestionServicios", {
            templateUrl : "plantillas/contenido/control/GestionServicios/ListGestionServicios.php",
            controller : "ControlCtrl"
        })


        .otherwise({
            templateUrl : "../login/login.html"
        });

    });


// SERVICIOS DE PRODUCTOS

appGeneral.factory('Productos',['$http', function($http){

    return {

        ListProductos: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Productos/Listado_Productos_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{idtipo:idtipo}
            })
        },

        ListProductosEdi: function(idproductos){
            return $http({
                method:'POST',
                url:'includes/sql/Productos/List_Editar_Productos_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idproductos:idproductos}
            })
        },

        ListProductosInventario: function(idtipo_producto){
            return $http({
                method:'POST',
                url:'includes/sql/Productos/Listado_Productos_Inventario_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idtipo_producto:idtipo_producto}
            })
        },

        ListProductosInventarioMen: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Productos/Inventario/Listado_Productos_InventarioMenor_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        ExistenciaProduct: function(idproductos){
            // console.log('LLEGO AL FACTORY: '+idproductos);
            return $http({
                method:'POST',
                url:'includes/sql/Productos/Inventario/Existencia_Producto_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idproductos:idproductos}
            })
        },  

        ListEntradasProd: function(idtipo_producto){
            // console.log('llego factory: '+idtipo_producto);
            return $http({
                method:'POST',
                url:'includes/sql/Productos/Listado_Entrada_Productos_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idtipo_producto:idtipo_producto}
            })
        },

        ListEntradasProdEd: function(idingreso_productos, idproductos){
            return $http({
                method:'POST',
                url:'includes/sql/Productos/Listado_Entrada_Edit_Productos_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idingreso_productos:idingreso_productos, idproductos:idproductos}
            })
        },

        ListVentasProd: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Productos/Listado_Venta_Productos_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{id_proyecto:id_proyecto}
            })
        },

        ListVentasMes: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Productos/Listado_Venta_ProductosMes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        ListVentasDate: function(fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Productos/Listado_Venta_ProductosDate_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },

        // PORCIONES GENERAL
        ListPorciones: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Productos/Listado_Productos_Porcion_Gen_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },
        // PORCIONES POR MES
        ListPorcionesMes: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Productos/Listado_Productos_Porcion_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        // PORCIONES POR DATE
        ListPorcionesDate: function(fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Productos/Listado_Productos_Porcion_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },

        ListDetalleSalidaProd: function(idproductos){
            return $http({
                method:'POST',
                url:'includes/sql/Productos/Inventario/Detalle_Salida_Product_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idproductos:idproductos}
            })
        },

        ListDetalleEntradaProd: function(idproductos){
            return $http({
                method:'POST',
                url:'includes/sql/Productos/Inventario/Detalle_Entrada_Product_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idproductos:idproductos}
            })
        },

    } // End Return

}]);


// SERVICIOS DE EMPLEADOS

appGeneral.factory('Empleados',['$http', function($http){

    return {

        ListEmpleados: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Empleados/Listado_Empleados_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{id_proyecto:id_proyecto}
            })
        },
        ListEmpleadosEd: function(idempleados){
            return $http({
                method:'POST',
                url:'includes/sql/Empleados/Listado_Empleados_edit_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados}
            })
        },
        CambEstadoEmpleado: function(idempleados, estado){
            return $http({
                method:'POST',
                url:'includes/sql/Empleados/Cambiar_Estado_Empleado_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados, estado:estado}
            })
        },

    } // End Return

}]);


// SERVICIOS DE CLIENTES

appGeneral.factory('Clientes',['$http', function($http){
    
    return {

        ListClientes: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Clientes/Listado_Clientes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{id_proyecto:id_proyecto}
            })
        },
        ServClientes: function(idclientes){
            return $http({
                method:'POST',
                url:'includes/sql/Clientes/Listado_servicios_clientes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idclientes:idclientes}
            })
        },

    } // End Return

}]);

// SERVICIOS DE SERVICIOS

appGeneral.factory('Servicios', ['$http', function($http){
    
    return {
        
        ListServicios: function(idserv){
            // console.log('llego a factory '+idserv);
            return $http({
                method:'POST',
                url:'includes/sql/Servicios/Listado_Servicios_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idserv:idserv}
            })
        },

        // MOSTRAR SERVICIO PARA EDITAR 
         ListEditServicio: function(idservicio){

            return $http({
                method:'POST',
                url:'includes/sql/Servicios/Listado_Servicio_MostrarEditar_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idservicio:idservicio}
            })
        },

        // MOSTRAR ELEMENTOS USADOS EN EL SERVICIO PARA EDITAR 
         ListElementoServicio: function(idservicio){

            return $http({
                method:'POST',
                url:'includes/sql/Servicios/Listado_Elementos_Servicio_MostrarEditar_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idservicio:idservicio}
            })
        },

        ListDetalleInd: function(idempleados, idservicio){
            return $http({
                method:'POST',
                url:'includes/sql/Servicios/Listado_DetalleServicioInd_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados, idservicio:idservicio}
            })
        },

        ListOtrosServiciosDia: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Servicios/Listado_OtrosServicios_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        ListPropinasdia: function(){
            
            return $http({
                method:'POST',
                url:'includes/sql/Servicios/Listado_PropinasDia_sql.php',
                headers:{
                    'Content-Type': undefined
                },

            })
        },

        // LISTADO PRODUCTOS NO USADOS EN EL SERVICIO

        ProductServiceNoUso: function(tiposerv, idservicio){
            return $http({
                method:'POST',
                url:'includes/sql/Servicios/Listado_ProductoServicioNoUsados_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{tiposerv:tiposerv, idservicio:idservicio}
            })
        },

        // PREUBA

        ProductServiceTodos: function(tiposerv, idservicio){
            return $http({
                method:'POST',
                url:'includes/sql/Servicios/Listado_ProductoServicioTodos_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{tiposerv:tiposerv, idservicio:idservicio}
            })
        },

    } // End Return

}]);

// SERVICIOS DE CONTROL

appGeneral.factory('Control',['$http', function($http){

    return {

        DetalleLiquidac: function(idempleados){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Liquidacion_Diaria_empleado_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados}
            })
        },

        DetalleLiquidacDate: function(idempleados, fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Liquidacion_Date_empleado_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados, fecha:fecha}
            })
        },

        DetalleVales: function(idempleados){
            return $http({
                method:'POST',
                url:'includes/sql/Control/ListadoValesLiquidacion_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados}
            })
        },
        DetalleValesDate: function(idempleados, fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/ListadoValesLiquidacionDate_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados, fecha:fecha}
            })
        },
        DetalleVentas: function(idempleados){
            return $http({
                method:'POST',
                url:'includes/sql/Control/ListadoVentasLiquidacion_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados}
            })
        },
        DetalleVentasDate: function(idempleados, fecha){
            // console.log(idempleados, fecha) 
            return $http({
                method:'POST',
                url:'includes/sql/Control/ListadoVentasLiquidacionDate_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados, fecha:fecha}
            })
        },
        DetalleLavacabDia: function(idempleados){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_LavaCabezas_Liquidacion_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados}
            })
        },
        DetalleLavacabDate: function(idempleados, fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_LavaCabezas_Date_Liquidacion_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados, fecha:fecha}
            })
        },
        DetallePropinas: function(idempleados){
            return $http({
                method:'POST',
                url:'includes/sql/Control/ListadoPropinasLiquidacion_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados}
            })
        },
        DetallePropinasDate: function(idempleados, fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/ListadoPropinasLiquidacionDate_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados, fecha:fecha}
            })
        },

        // Listado Vales dia
        ListVales: function(idvale){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Vales_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idvale:idvale}
            })
        },

        // Listado Vales Mes
        ListValesMes: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Vales_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        // Listado Vales DATE
        ListValesDate: function(fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Vales_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },

        ListSalidasDia: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Salidas_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },
        ListSalidaEdi: function(idsalidas_internas){
            // console.log('llego al factory' +idsalidas_internas)
            return $http({
                method:'POST',
                url:'includes/sql/Control/List_Editar_Salidas_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idsalidas_internas:idsalidas_internas}
            })
        },

        // Listado Salidas mes seleccionado
        ListSalidasMes: function(mes, year){
            // console.log('llego al factory' +idsalida)
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Salidas_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        // Listado Salidas DATE seleccionado
        ListSalidasDate: function(fecha){
            
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Salidas_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },

        ListSalidas: function(mes){
            // console.log('llego al factory' +idsalida)
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_SalidasMonth_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes}
            })
        },
        ListValesEdi: function(idvales){
            // console.log('llego al factory ' +idvales)
            return $http({
                method:'POST',
                url:'includes/sql/Control/List_Editar_Vales_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idvales:idvales}
            })
        },

        // Listado prestamos dia
        ListPrestamos: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Prestamos_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        // Listado prestamos MES
        ListPrestamosMes: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Prestamos_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        // Listado prestamos DATE
        ListPrestamosDate: function(fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Prestamos_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },

        // Listado prsetamos quincenales dia
        ListPrestamosQuincDia: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/PagosPrestamos/Listado_PrestamosDia_Quincena_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        // Listado prsetamos quincenales MES
        ListPrestamosQuincMes: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/PagosPrestamos/Listado_PrestamosMes_Quincena_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        // Listado prsetamos quincenales DATE
        ListPrestamosQuincDate: function(fecha_con){
            return $http({
                method:'POST',
                url:'includes/sql/Control/PagosPrestamos/Listado_PrestamosDate_Quincena_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha_con:fecha_con}
            })
        },

        // Consultar salario para pagar
        ConsultaSal: function(idempleados){
            return $http({
                method:'POST',
                url:'includes/sql/Control/PagosPrestamos/Consulta_Salario_Empleado_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados}
            })
        },

        // Calcular prestamos para no suuperar los 100mil pesos
        calcprestamosind: function(idempleados){
            return $http({
                method:'POST',
                url:'includes/sql/Control/calcular_prestamo_empleado_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados}
            })
        },

        // Consultar pagos quincenales dia
        ListPagosQuincDia: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/PagosPrestamos/Consulta_Pagos_Quincena_dia_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        // Consultar pagos quincenales MES
        ListPagosQuincMes: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/PagosPrestamos/Consulta_Pagos_Quincena_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        // Consultar pagos quincenales DATE
        ListPagosQuincDate: function(fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/PagosPrestamos/Consulta_Pagos_Quincena_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },

        ListValidaPagos: function(idempleado, mes, periodo){
            return $http({
                method:'POST',
                url:'includes/sql/Control/PagosPrestamos/Listado_Validar_PagosQuincena_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleado:idempleado, mes:mes, periodo:periodo}
            })
        },

        ListValidaPagosInv: function(idempleado, mes, periodo, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/PagosPrestamos/Listado_Validar_PagosQuincenaInd_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleado:idempleado, mes:mes, periodo:periodo, year:year}
            })
        },

        ListDeudores: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Deudores_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{idsalida:idsalida}
            })
        },
        ListPrestamosInd: function(idprestamos){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_PrestamosInd_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idprestamos:idprestamos}
            })
        },
        ListPrestamosIndPago: function(idempleados){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_PrestamosIndPago_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idempleados:idempleados}
            })
        },
        ListPrestamosMonth: function(prestamomes){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_PrestamosMonth_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:prestamomes
            })
        },
        ListPrestamosEdi: function(idprestamos){
            // console.log('llego al factory ' +idprestamos)
            return $http({
                method:'POST',
                url:'includes/sql/Control/List_Editar_Prestamos_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idprestamos:idprestamos}
            })
        },
        ListAbonos: function(idprestamos){

            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Abonos_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idprestamos:idprestamos}
            })
        },
        ListAbonosDia: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Abonos_Dia_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },
        ListAbonosMes: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Abonos_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },
        ListAbonosDate: function(fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Abonos_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },
        ListLiquidaciones: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_SalidasMonth_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{mes:mes}
            })
        },
        ListEmpleadosLiquidar: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/ListadoEmpleadosLiquidar_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },
        ListEmpleadosAuxLiquidar: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/ListadoEmpleadosAuxLiquidar_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },
        ListLiquidacionesReg: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Liquidaciones_registradas_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },
        ListEntradaCaja: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Entradas_Caja_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{idempleados:idempleados}
            })
        },
        ListSalidaCaja: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Salidas_Caja_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{idempleados:idempleados}
            })
        },
        ListRegistroLiquid: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Registro_liquidacionesDia_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{idempleados:idempleados}
            })
        },
        ListLiquidacionMes: function(mes, year, idempleados){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Registro_liquidacioneMes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year, idempleados:idempleados}
            })
        },
        ListLiquidacionDate: function(year, mes, dia, idempleados){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Registro_liquidacioneDate_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{year:year, mes:mes, dia:dia, idempleados:idempleados}
            })
        },

        ListLiquidacionMesGen: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Registro_liquidacionMesGen_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        ListLiquidacionDateGen: function(fecha_con, idempleados){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Registro_liquidacionesDateGen_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha_con:fecha_con, idempleados:idempleados}
            })
        },

        ListIngresosDiarios: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Ingresos_Diarios_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{idempleados:idempleados}
            })
        },

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VALORES EN INGRESOS DEL DIA - NUEVA PANTALLA 

// ENTRADAS 

        // Servicios por dia $ valores 
        ListServiciosDiaVal: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Dia/List_ValServicios_Dia_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        // Servicios por MES $ valores 
        ListServiciosMesVal: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Mes/List_ValServicios_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        // Servicios por FECHA $ valores 
        ListServiciosDateVal: function(fecha_con){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Date/List_ValServicios_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha_con:fecha_con}
            })
        },

        // Productos por dia $ valores 
        ListProductosDiaVal: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Dia/List_ValProductos_Dia_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        // Productos por MES $ valores 
        ListProductosMesVal: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Mes/List_ValProductos_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        // Productos por DATE $ valores 
        ListProductosDateVal: function(fecha_con){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Date/List_ValProductos_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha_con:fecha_con}
            })
        },

        // OtrosIngersos por dia $ valores 
        ListOtrosIngresosDiaVal: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Dia/List_OtrosIngresos_Dia_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        // OtrosIngersos por MES $ valores 
        ListOtrosIngresosMesVal: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Mes/List_OtrosIngresos_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        // OtrosIngersos por MES $ valores 
        ListOtrosIngresosDateVal: function(fecha_con){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Date/List_OtrosIngresos_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha_con:fecha_con}
            })
        },

        // Abonos por dia $ valores 
        ListAbonosDiaVal: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Dia/List_Abonos_Dia_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        // Abonos por MES $ valores 
        ListAbonosMesVal: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Mes/List_Abonos_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        // Abonos por DATE $ valores 
        ListAbonosMesVal: function(fecha_con){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Date/List_Abonos_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha_con:fecha_con}
            })
        },

        // Propinas por dia $ valores 
        ListPropinasDiaVal: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Dia/List_Propinas_Dia_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        // Propinas por MES $ valores  
        ListPropinasMesVal: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Mes/List_Propinas_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        // Propinas por DATE $ valores  
        ListPropinasDateVal: function(fecha_con){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Date/List_Propinas_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha_con:fecha_con}
            })
        },

// SALIDAS

        // Salidas internas por dia $ valores 
        ListSalidasIntDiaVal: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Dia/List_SalidasInternas_Dia_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        // Salidas internas por MES $ valores 
        ListSalidasIntMesVal: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Mes/List_SalidasInternas_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        // Salidas internas por DATE $ valores 
        ListSalidasIntDateVal: function(fecha_con){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Date/List_SalidasInternas_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha_con:fecha_con}
            })
        },

        // Prestamos por dia $ valores 
        ListPrestamosDiaVal: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Dia/List_Prestamos_Dia_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        // Prestamos por MES $ valores 
        ListPrestamosMesVal: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Mes/List_Prestamos_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        // Prestamos por DATE $ valores 
        ListPrestamosDateVal: function(fecha_con){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Date/List_Prestamos_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha_con:fecha_con}
            })
        },

        // Liquidaciones por dia $ valores 
        ListLiquidacionDiaVal: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Dia/List_Liquidaciones_Dia_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        // Liquidaciones por MES $ valores 
        ListLiquidacionMesVal: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Mes/List_Liquidaciones_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        // Liquidaciones por DATE $ valores 
        ListLiquidacionDateVal: function(fecha_con){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Date/List_Liquidaciones_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha_con:fecha_con}
            })
        },

        // lavacabezas por dia $ valores 
        ListLavacabezaDiaVal: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Dia/List_Lavacabezas_Dia_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        // lavacabezas por MES $ valores 
        ListLavacabezaMesVal: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Mes/List_Lavacabezas_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        // lavacabezas por DATE $ valores
        ListLavacabezaDateVal: function(fecha_con){
            return $http({
                method:'POST',
                url:'includes/sql/Control/IngresosValores/Date/List_Lavacabezas_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha_con:fecha_con}
            })
        },

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//END VALORES EN INGRESOS DEL DIA - NUEVA PANTALLA 

        ListIngresosMes: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Ingresos_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },
        ListIngresosDate: function(fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Ingresos_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },
        ListOtrosIngDia: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Otros_Ingresos_Dia_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },
        ListOtrosIngMes: function(mes, year){

            // console.log('llego factyry: '+mes+year)
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Otros_Ingresos_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },
        ListOtrosIngDate: function(fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Otros_Ingresos_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },
        ListOtrosIngEdi: function(idotros_ingresos){
            // console.log('llego al factory ' +idotros_ingresos)
            return $http({
                method:'POST',
                url:'includes/sql/Control/List_Editar_OtrosIngresos_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idotros_ingresos:idotros_ingresos}
            })
        },
        ListTipoServDia: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_TipoServicios_Diarios_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{idempleados:idempleados}
            })
        },
        ListTipoServMes: function(mes, year){
             // console.log('mes: '+mes+' año: '+year )
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_TipoServicios_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },
        ListTipoServDate: function(fecha){
             // console.log('factory fecha: '+fecha)
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_TipoServicios_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },
        ListVentaProdVitrina: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Venta_ProductosVitrina_Diarios_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{idempleados:idempleados}
            })
        },
        ListVentaProdVitrinaMes: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Venta_ProductosVitrina_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },
        ListVentaProdVitrinaDate: function(fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Venta_ProductosVitrina_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha}
            })
        },
        ListVentaProdServ: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Venta_ProductosServicio_Diarios_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{idempleados:idempleados}
            })
        },
        ListVentaProdServMes: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Venta_ProductosServicio_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },
        ListVentaProdServDate: function(fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Venta_ProductosServicio_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },
        ListAbonosDiaIng: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Abonos_Diarios_Ing_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{idempleados:idempleados}
            })
        },
        ListAbonosMesIng: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Abonos_Mes_Ing_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes,year:year}
            })
        },
        ListAbonosDateIng: function(fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Abonos_Date_Ing_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },
        ListSalidasInt: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Salidas_Internas_Diarias_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{idempleados:idempleados}
            })
        },
        ListSalidasIntMes: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Salidas_Internas_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },
        ListSalidasIntDate: function(fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Salidas_Internas_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },
        ListPrestdia: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Prestamos_Diarios_Ing_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{idempleados:idempleados}
            })
        },
        ListPrestMes: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Prestamos_Mes_Ing_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },
        ListPrestDate: function(fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Prestamos_Date_Ing_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },
        ListLiquidaciondia: function(){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Liquidaciones_Diarias_Ing_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{idempleados:idempleados}
            })
        },
        ListLiquidacionMesIng: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Liquidaciones_Mes_Ing_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },
        ListLiquidacionDateIng: function(fecha){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Liquidaciones_Date_Ing_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },
        ListNotifDia: function(notif){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Notificaciones_Diarias_Ing_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{notif:notif}
            })
        },
        ListNotifEdit: function(idnotificaciones){
            // console.log('llego al factory ' +idprestamos)
            return $http({
                method:'POST',
                url:'includes/sql/Control/List_Editar_Notificaciones_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idnotificaciones:idnotificaciones}
            })
        },
        ListNotificacionMes: function(mes, year){
            return $http({
                method:'POST',
                url:'includes/sql/Control/Listado_Notificaciones_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },



        // CONSULTA DE SERVICIOS 

        ListServiciosMes: function(mes, year){
            // console.log('llego a factory mes: '+mes+' año: '+year);
            return $http({
                method:'POST',
                url:'includes/sql/Servicios/Listado_ServiciosMes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        ListServiciosDate: function(fecha){
            // console.log('llego a factory año: '+year+' mes: '+mes+' dia: '+dia);
            return $http({
                method:'POST',
                url:'includes/sql/Servicios/Listado_ServiciosDate_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },

        // CONSULTA OTROS SERVICIOS 

        ListOtrosServiciosMes: function(mes, year){

            return $http({
                method:'POST',
                url:'includes/sql/Servicios/Listado_OtrosServiciosMes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        ListOtrosServiciosDate: function(fecha){
            // console.log('llego a factory año: '+year+' mes: '+mes+' dia: '+dia);
            return $http({
                method:'POST',
                url:'includes/sql/Servicios/Listado_OtrosServiciosDate_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },

        // CONSULTA PROPINAS 

        ListPropinasMes: function(mes, year){

            return $http({
                method:'POST',
                url:'includes/sql/Servicios/Listado_Propinas_Mes_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{mes:mes, year:year}
            })
        },

        ListPropinasDate: function(fecha){

            return $http({
                method:'POST',
                url:'includes/sql/Servicios/Listado_Propinas_Date_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{fecha:fecha}
            })
        },

        // USUARIOS

        ListUsuarios: function(){

            return $http({
                method:'POST',
                url:'includes/sql/Control/Usuarios/Listado_Usuarios_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },
        // LISTAR USUARIO PARA EDITAR
        ListditUser: function(idlogin){

            return $http({
                method:'POST',
                url:'includes/sql/Control/Usuarios/Listar_Usuario_editar_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idlogin:idlogin}
            })
        },
        // CAMBIAR ESTADO USUARIO
        CambEstadoUser: function(idlogin, estado){

            return $http({
                method:'POST',
                url:'includes/sql/Control/Usuarios/Cambiar_Estado_Usuario_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idlogin:idlogin, estado:estado}
            })
        },


    } // End Return

}]);

// LISTADOS GENERALES PARA LOS SELECT

appGeneral.factory('ListGenerales',['$http', function($http){

    return {
        ListProductosSelect: function(idproducto){
            return $http({
                method:'POST',
                url:'includes/sql/ListadosGenerales/Listado_Productos_select_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idproducto:idproducto}
            })
        },
        ListEmpleadosSelect: function(){
            return $http({
                method:'POST',
                url:'includes/sql/ListadosGenerales/Listado_Empleados_select_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{id_proyecto:id_proyecto}
            })
        },
        ListEmpleadosGeneral: function(){
            return $http({
                method:'POST',
                url:'includes/sql/ListadosGenerales/Listado_Empleados_SelectGeneral_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{id_proyecto:id_proyecto}
            })
        },
        ListClientesSelect: function(){
            return $http({
                method:'POST',
                url:'includes/sql/ListadosGenerales/Listado_Clientes_select_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{id_proyecto:id_proyecto}
            })

        },
        ValorProduct: function(idproductos){
            // console.log('factory: '+idproductos)
            return $http({
                method:'POST',
                url:'includes/sql/ListadosGenerales/Valor_Producto_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{idproductos:idproductos}
            })
        },

        ListTipoService: function(){
            return $http({
                method:'POST',
                url:'includes/sql/ListadosGenerales/Listado_Tipo_Servicio_select_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        ListTipoServiceGest: function(){
            return $http({
                method:'POST',
                url:'includes/sql/ListadosGenerales/Listado_Tipo_Servicio_select_Gest_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        ListProductService: function(){
            return $http({
                method:'POST',
                url:'includes/sql/ListadosGenerales/Listado_Productos_Servicio_select_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        // PRODUCTOS AL SELECCIONAR EL SERVICIO
        ProductService: function(tiposerv){
            return $http({
                method:'POST',
                url:'includes/sql/ListadosGenerales/Listado_ProductoServicio_option_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{tiposerv:tiposerv}
            })
        },
        ListEmpleados: function(){
            return $http({
                method:'POST',
                url:'includes/sql/ListadosGenerales/Listado_Empleados_sql.php',
                headers:{
                    'Content-Type': undefined
                },
                // data:{id_proyecto:id_proyecto}
            })
        },
        FechaLiquidado: function(){
            return $http({
                method:'POST',
                url:'includes/sql/ListadosGenerales/Estado_Fecha_Liquidaciones_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },
        ListAnos: function(){
            return $http({
                method:'POST',
                url:'includes/sql/ListadosGenerales/Listado_Anos_Select_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },
        ListMeses: function(){
            return $http({
                method:'POST',
                url:'includes/sql/ListadosGenerales/Listado_Meses_Select_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },
        ListPeriodo: function(){
            return $http({
                method:'POST',
                url:'includes/sql/ListadosGenerales/Listado_PeriodosPago_Select_sql.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },
    } // End Return

}]);