// CONTENIDO INICIAL ******************************************************************************************************************************************************************************************************************************************************************
// ********************************************************************************************************************************************************************************************************************************************************************************

// CREAR PAGO
appGeneral.controller('PagosCrearCtrl',['$http','$scope' ,function($http,$scope){


}]); // End Controller PagosCrearCtrl

// VALIDA Y DIRECCIONA A OTRO - PAGO CORRECTO
appGeneral.controller('PagosCtrl',['$http','$scope' ,function($http,$scope){

	location.reload()
	var url = "http://juanmartinpeluqueria.com/servicios/web/#/pagos/perfecto";
	$(location).attr('href',url);
	
}]); // End Controller PagosCtrl

// VENTANA DE PAGO CORRECTO
appGeneral.controller('PagosCorrectCtrl',['$http','$scope' ,function($http,$scope){

	// console.log('Llego al pago perfecto OK OK OK JHON')


}]); // End Controller PagosCorrectCtrl



appGeneral.controller('ContPrincipalCtrl',['$http','$scope','Control', function($http,$scope,Control){

// Controller PRINCIPAL //*********************************************************************************************

	$scope.CargaAnimated = function(){

		$('.cargar').velocity('transition.slideDownIn', {
	        stagger: 250,
	        drag: true,
	        duration: 2000,
	            complete: function () {
		            return $('.value').each(function (index, item) {
		                var val, value;
		                value = $(item).data('value');
		                val = parseInt(value, 10);
		                return $({ someValue: 0 }).animate({ someValue: val }, {
		                    duration: 1000,
		                    easing: 'swing',
		                    step: function () {
		                        return $(item).text(Math.round(this.someValue));
		                    }
		                });
		            });
		        }
	    });
	}

	$scope.CargaAnimated();



// FUNCION PARA CONSULTAR LAS NOTIFICACIONES DEL DIA - PARA PANTALLA INICIAL ************************************************************
	
	$scope.ListNotificaionesDia = function(notif){

		Control.ListNotifDia(notif).success(function(data){
			
			$scope.notificaciones = data;

			if($scope.notificaciones == ''){
				// console.log('No hay notif para hoy');
			}else{
				alert('<span class="fa fa-bell-o fs40 mb10 animated shake"></span><br>¡Existen notificaciones<br>para el día de hoy!');
			}

			// console.log(data);

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListNotificaionesDia();



}]); // End Controller ContPrincipalCtrl

// Controller Header //*********************************************************************************************

appGeneral.controller('HeaderCtrl',['$http','$scope',function($http, $scope){

		$scope.today = new Date();

		// Funcion para actividad del mouse	

		$scope.Inactividad = function(){

			var nom_user = $('#nom_user').text();
			var tiempo = 0;

			setInterval(function(){ 
				
				// console.log('Tiempo es: ' + tiempo);
		      	tiempo++;

				if(tiempo == 360){

					// alert('¡'+ nom_user + '! <br><br>El sistema detectó inactividad<br> por más de 5 minutos.<br><br> Se cerrará la sesión automaticamente.');
					// swal("El sistema detectó inactividad");
					// swal("Usuario " + nom_user, "...and here's the text!");


					 $.sweetModal.confirm('¡'+ nom_user + '! <br><br>El sistema detectó inactividad<br> por más de 5 minutos.<br><br> Se cerrará la sesión automaticamente. Pulsar clic en continuar', function() {
						// console.log('Se cerrro');
						window.location = "../web/includes/sql/Logout.php";
					});
					 $('.redB').hide();
				}

			}, 1000);

			$(document).mousemove(function(event){
				tiempo = 0; 
			});

		}

		$scope.Inactividad();

}]); // End Controller HeaderCtrl

// Controller Menu Izquierda //*********************************************************************************************

appGeneral.controller('MenuIzqCtrl',['$http','$scope', 'ListGenerales', 'Productos', 'Control', function($http, $scope, ListGenerales, Productos, Control){

	$scope.today = new Date();

	$scope.estadoLiquidado = function(){

		ListGenerales.FechaLiquidado().success(function(data){
			$scope.fecha_liquidado = data;
		// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
		
	}

	$scope.estadoLiquidado();


// PRODUCTOS MENORES DE 2

	$scope.ListadoProductosInventarioMenor = function(){
		
		Productos.ListProductosInventarioMen().success(function(data){

			$scope.productinventariomenor = data;
			// console.log($scope.productinventariomenor);		

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListadoProductosInventarioMenor();

// FUNCION PARA CONSULTAR LAS NOTIFICACIONES DEL DIA - PARA PANTALLA INICIAL ************************************************************
	
	$scope.ListNotificaionesDia = function(notif){

		Control.ListNotifDia(notif).success(function(data){
			$scope.notificaciones = data;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListNotificaionesDia();

	
	// mover al home

	$scope.moverahome = function(){
		window.location = "#/";
	}

// Controller Menu Izquierda //*********************************************************************************************

		$('.menuBtn').click(function(){
			$('.menuBtn').removeClass('active');
			$(this).addClass('active');
		});

}]); // End Controller MenuIzqCtrl

// PRODUCTOS

// Controller PRODUCTOS //*********************************************************************************************

appGeneral.controller('ProductosCtrl', ['$http', '$scope', 'Productos', 'ListGenerales', 'Servicios', '$routeParams', function($http,$scope, Productos, ListGenerales, Servicios, $routeParams){	

	$scope.CargaAnimated = function(){

		$('.cargar').velocity('transition.slideDownIn', {
	        stagger: 250,
	        drag: true,
	        duration: 2000,
	            complete: function () {
		            return $('.value').each(function (index, item) {
		                var val, value;
		                value = $(item).data('value');
		                val = parseInt(value, 10);
		                return $({ someValue: 0 }).animate({ someValue: val }, {
		                    duration: 1000,
		                    easing: 'swing',
		                    step: function () {
		                        return $(item).text(Math.round(this.someValue));
		                    }
		                });
		            });
		        }
	    });
	}

	$scope.CargaAnimated();

	$scope.today = new Date();

// FUNCION PARA REGISTRAR UN NUEVO PRODUCTO ************************************************************

	$scope.RegistrarProducto = function(idtipo){

		alertify.confirm("¿Deseas registrar la información?", function (info) {

			if (info) {
				var RegistrarProducto = {
		            tipo:$scope.tipo,
		            nom_producto:$scope.nom_producto,
		            marca:$scope.marca,
		            mililitros:$scope.mililitros,
		            valor_unit:$scope.valor_unit,
		            descuento:$scope.descuento,
		            idtipo:idtipo
		        };
		        // console.log(RegistrarProducto);

				$http({
					method:'POST',
					url:'../web/includes/sql/Productos/Registrar_Productos_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:RegistrarProducto

					}).success(function(respuesta){
						$scope.productos = respuesta;
						// console.log(respuesta);
						if(respuesta == 1){
							alertify.error("<b>Info: </b>¡Ya existe un producto con esta misma información!");
						} else{
							alertify.success("<b>Info: </b>Información registrada con éxito");
							window.location = "#/productos";
						}
					}).error(function(err){
						console.log("Error: "+err);
					});
			}
				else{
					alertify.error("<b>Info: </b>¡Proceso cancelado!");
				}
		});
	}

// FUNCION PARA LISTAR LOS PRODUCTOS REGISTRADOS ************************************************************

	$scope.ListadoProductos = function(){
			// console.log(idtipo);
		Productos.ListProductos().success(function(data){

			$scope.productosV = data;
			$scope.productosS = data;
			$scope.tipo_vitrina = [];
			$scope.tipo_servicio = [];
			
			angular.forEach(data,function(valor){
				if(valor.idtipo_producto == 1){
					$scope.tipo_vitrina.push(valor);
					// console.log($scope.tipo_vitrina);
				} else if(valor.idtipo_producto == 2){
					$scope.tipo_servicio.push(valor);
					// console.log(valor);
				}

			});
			// console.log($scope.unos);

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListadoProductos();


// FUNCION PARA CONSULTAR PROUDCTOS Y EDITARLAOS ************************************************************

	$scope.ListadoProductosEdit = function(){

		Productos.ListProductosEdi($routeParams.idproductos).success(function(data){

			$scope.productos = data;
			$scope.idproductos = data[0].idproductos;
			$scope.tipo = data[0].tipo;
			$scope.nom_producto = data[0].nom_producto;
			$scope.marca = data[0].marca;
			$scope.mililitros = data[0].mililitros;
			$scope.valor_unit = data[0].valor_unit;
			$scope.descuento = data[0].descuento;
			$scope.idtipo_producto = data[0].idtipo_producto;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoProductosEdit();

// FUNCION PARA EDITAR UN PRODUCTO ************************************************************

	$('.fa-pulse, #update, .fa-check').hide();

	$scope.EditProducto = function(idproductos, idtipo_producto){
		
		var mililitros = $scope.mililitros;
		var valor_unit = $scope.valor_unit;
		var descuento = $scope.descuento;

		var descuentoVal = parseInt(descuento);
		var valor_unitVal = parseInt(valor_unit);

		if(mililitros <= 0 || mililitros == undefined || valor_unit <= 0 || valor_unit == undefined || descuento <= 0 || descuento == undefined){
  			alertify.error("<b>Info: </b>¡Campos invalidos!");
  		} else if(
  				!/^([0-9])*$/.test(mililitros) 
  				|| !/^([0-9])*$/.test(valor_unit)
  				|| !/^([0-9])*$/.test(descuento)
  				){
				alertify.error("<b>¡Los valores deben ser un número!");
			}

			else if(descuentoVal > valor_unitVal){
				alertify.error("<b>¡El descuento no puede ser mayor al valor del producto!");
			}

  		else{

			$('.fa-pulse, #update').show();
			$('.fa-check').hide();

			datos = {
				idproductos:$scope.idproductos,
				tipo:$scope.tipo,
				nom_producto:$scope.nom_producto,
				marca:$scope.marca,
				mililitros:mililitros,
				valor_unit:valor_unit,
				descuento:descuento,
				idtipo_producto:idtipo_producto
			}
			// console.log(datos);
			$http({
				method:'POST',
				url:'../web/includes/sql/Productos/Editar_Producto_sql.php',
				header:{
				'Content-Type': undefined
			},
				data:datos
			}).success(function(respuesta){
				$scope.employee = respuesta;
					// console.log(respuesta)
					setTimeout(function(){
						$('.fa-pulse, #update').hide();
						$('.fa-check').show();
						$('#cantidad, #total').val('');
						alertify.success("Información actualizada con éxito");
					 }, 3000);
			}).error(function(err){
					console.log("Error: "+err);
				});
  		}
	}

// FUNCION PARA REGISTRAR UN NUEVO INGRESO DE PRODUCTO AL INVENTARIO ************************************************************

	$scope.NewEntradaProducto = function(entrada){
		// console.log(producto)
		alertify.confirm("¿Deseas registrar la información?", function (info) {

			if(info){
				$http({
					method:'POST',
					url:'../web/includes/sql/Productos/New_Entrada_Producto_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:producto

					}).success(function(respuesta){
						$scope.productos = respuesta;
						// console.log(respuesta)
						alertify.success("<b>Info: </b>Información registrada con éxito");
						window.location = "#/productos";

					}).error(function(err){

						console.log("Error: "+err);
					});	
				}
				else{
					alertify.error("<b>Info: </b>¡Proceso cancelado!");
				}
		});
	}

// FUNCION PARA CONSULTAR TODOS LOS PRODUCTOS REGISTRADOS PARA SELECCION ************************************************************

	$scope.ListadoProductosSelect = function(idproducto){

		ListGenerales.ListProductosSelect(idproducto).success(function(data){
			$scope.productos_select = data;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoProductosSelect();


// FUNCION PARA CONSULTAR TODOS LOS EMPLEADOS REGISTRADOS PARA SELECCION ************************************************************

	$scope.ListadoEmpleadosSelect = function(){

		ListGenerales.ListEmpleadosSelect().success(function(data){

			$scope.empleados_select = data;
			// console.log(data)

		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoEmpleadosSelect();


// FUNCION PARA REGISTRAR UN NUEVA ENTRADA PRODUCTO A INVENTARIO ************************************************************

	$scope.RegistrarNewEntrada = function(){

  		var valor_real = $('#valor_real').val();
  		var usuario = $('#usuario').val();
  		var cantidad = $scope.cantidad;

  		if(cantidad < 0 || cantidad == 0){
  			alertify.error("<b>Info: </b>¡Cantidad no valida!");
  		} else{

	     	var parametrosentrada = {
	            idproductos:$scope.idproductos,
	            idtipo_producto:$scope.idtipo_producto,
	            cantidad:cantidad,
	            valor_real:$scope.valor_real,
	            usuario:usuario,
	        };
			// console.log(parametrosentrada)

			alertify.confirm("¿Deseas registrar la información?", function (info) { 

				if(info){

					$http({
						method:'POST',
						url:'../web/includes/sql/Productos/Registrar_Entrada_Prod_sql.php',
						header:{
						'Content-Type': undefined
					},
						data:parametrosentrada
						}).success(function(respuesta){
							$scope.entradas = respuesta;
							alertify.success("<b>Info: </b>Información registrada con éxito");
							// console.log(respuesta)
							$scope.ListadoProductosInventario(2)
							if(respuesta == 1){
								window.location = "#/entradas";
							}else{
								window.location = "#/EntradaServ";
							}
						}).error(function(err){
							console.log("Error: "+err);
						});
				}
				else{
					alertify.error("<b>Info: </b>¡Proceso cancelado!");
				}
			}); // End alerty registrar
  		}
	}

// FUNCION PARA CONSULTAR LAS ENTRADAS DE PRODUCTOS ************************************************************

	$scope.ListadoEntradasProductos = function(idtipo_producto){

		Productos.ListEntradasProd(idtipo_producto).success(function(data){
			$scope.entradas = data;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

// FUNCION PARA CONSULTAR LAS ENTRADAS DE PROUDCTOS Y EDITARLAS ************************************************************

	$scope.ListadoEntradasProductosEdit = function(){

		Productos.ListEntradasProdEd($routeParams.idingreso_productos, $routeParams.idproductos).success(function(data){
			$scope.entradas = data;
			$scope.idingreso_productos = data[0].idingreso_productos;
			$scope.idproductos = data[0].idproductos;
			$scope.nom_producto = data[0].nom_producto;
			$scope.marca = data[0].marca;
			$scope.tipo = data[0].tipo;
			$scope.cantidad = data[0].cantidad;
			$scope.valor_unit = data[0].valor_unit;
			$scope.valor_total = data[0].valor_total;
			$scope.fecha_ingreso = data[0].fecha_ingreso;
			$scope.entradas;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoEntradasProductosEdit();

// FUNCION PARA EDITAR UNA ENTRADA DEL INVENTARIO ************************************************************

	$('.fa-pulse, #update, .fa-check').hide();

	$scope.EditEntrada = function(idingreso_productos){
		
		var cantidad = $scope.newcantidad;

		if(cantidad <= 0 || cantidad == undefined){
  			alertify.error("<b>Info: </b>¡Cantidad no valida!");
  		} else{
			$('.fa-pulse, #update').show();
			$('.fa-check').hide();
			datos = {
				idingreso_productos:$scope.idingreso_productos,
				cantidad:cantidad,
				valor_unit:$scope.valor_unit,
			}
				// console.log(datos);
				$http({
					method:'POST',
					url:'../web/includes/sql/Productos/Editar_Entrada_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:datos
				}).success(function(respuesta){
					$scope.employee = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$scope.ListadoEntradasProductosEdit();
							$('.fa-pulse, #update').hide();
							$('.fa-check').show();
							$('#cantidad, #total').val('');
							alertify.success("Información actualizada con éxito");
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
  		}
	}

// FUNCION PARA ELIMINAR UNA ENTRADA DE PRODUCTO AL INVENTARIO ************************************************************

	$scope.DeleteEntradaProduct = function(idingreso_productos, idtipo_producto){

		// console.log(idingreso_productos+idtipo_producto);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Productos/Delete_EntradaProduct_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idingreso_productos:idingreso_productos}
				}).success(function(respuesta){
					$scope.deleteentrada = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ListadoEntradasProductos(idtipo_producto);
							// $scope.ListadoEntradasProductos(1);
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

	// End funcion $scope.DeleteEntradaProduct

// FUNCION PARA SABER EL VALOR DE CADA PRODUCTO

	$scope.ValorProducto = function(idproductos){
		// console.log('Primera funcion: '+idproductos)
		ListGenerales.ValorProduct(idproductos).success(function(data){

			$scope.valorproducto = data;
			$scope.valor_real = data[0].valor_unit;
			$scope.idtipo_producto = data[0].idtipo_producto;
			// console.log(data);

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ValorProducto();

// FUNCION PARA SABER LA EXISTENCIA DE UN PRODUCTO

	$scope.ExistenciaProducto = function(idproductos){
		// console.log('Existencia de: '+idproductos)
		$('#existenciaok').addClass('animated bounce')
		Productos.ExistenciaProduct(idproductos).success(function(data){
		$scope.existencias = data;
		// console.log(data);
		$scope.total_ingresos = data[0].total_ingresos
		$scope.total_salidas = data[0].total_salidas
		$scope.existencia = data[0].existencia
		$scope.resta = $scope.existencia - $scope.total_salidas;

			if($scope.existencia == 0){
				$('#guardar').hide();
				alertify.error("<b>Info: </b>¡Este producto no existe en el inventario!<br>No se puede realizar la venta");
			}
			else{
				$('#guardar').show();
			}

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ExistenciaProducto();


// FUNCION PARA REGISTRAR UN NUEVA VENTA DE PRODUCTO POR EMPLEADO ************************************************************

	$scope.RegistrarNewVenta = function(){

		var usuario = $('#usuario').val();
		// console.log(usuario);

		var valor_real = $('#valor_real').val();
		var cantidad = $scope.cantidad
		var existencia = $scope.existencia
		var diferencia = cantidad - existencia

		if(cantidad < 0 || cantidad == 0){
			alertify.error("<b>Info: </b>¡Cantidad no valida!");
		}else{
			if(cantidad > existencia){
				alertify.error("<b>Info: </b>¡No existe la cantidad solicitada!");
			}else{

				var VariablesVenta = {
		            idproductos:$scope.idproductos,
		            idempleados:$scope.idempleados,
		            cantidad:cantidad,
		            valor_real:$scope.valor_real,
		            tipo_pago:$scope.tipo_pago,
		            existencia:existencia,
		            diferencia:diferencia,
		            usuario:usuario,
		        };
				// console.log(VariablesVenta);

				alertify.confirm("¿Deseas registrar la información?", function (info) {

					if(info){
						$http({
							method:'POST',
							url:'../web/includes/sql/Productos/Registrar_Venta_Prod_sql.php',
							header:{
							'Content-Type': undefined
						},
							data:VariablesVenta
							}).success(function(respuesta){
								$scope.entradas = respuesta;
								alertify.success("Información registrada con éxito");
								// console.log(respuesta)
								window.location = "#/VentaProduct";
							}).error(function(err){
								console.log("Error: "+err);
							});	
					}else{
						alertify.error("<b>Info: </b>¡Proceso cancelado!");
					}
				}); // End alertity guardar
			} // End cantidad mayor
		} // End cantidad erronea
	}

// FUNCION PARA CONSULTAR LAS VENTAS DE PRODUCTOS ************************************************************

	$scope.ListadoVentasProductos = function(){

		$scope.today = new Date();

		Productos.ListVentasProd().success(function(data){

			$scope.ventas = data;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoVentasProductos();


// ***************************************************************************************************************


// FUNCION PARA ELIMINAR UN VENTA DE PRODUCTO DE VITRINA ************************************************************

	$scope.DeleteVentaProduct = function(idsalida_productos){

	// console.log(idsalida_productos);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Productos/Delete_VentaProduct_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idsalida_productos:idsalida_productos}
				}).success(function(respuesta){
					$scope.deleteventa = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ListadoVentasProductos();
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

	// End funcion $scope.DeleteVentaProduct


// FUNCION PARA LISTAR LOS PRODUCTOS DEL INVENTARIO 2************************************************************

	$scope.ListadoProductosInventario = function(idtipo_producto){

		Productos.ListProductosInventario(idtipo_producto).success(function(data){

			$scope.productinventarios = data;
			// console.log(data);

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListadoProductosInventario();


// FUNCION PARA CONSULTAR DETALLE DE ENTRADA DE PRODUCTOS - VENTAS ************************************************************

	$scope.DetalleEntradaProduct = function(){

		Productos.ListDetalleEntradaProd($routeParams.idproductos).success(function(data){
			$scope.entradaproductos = data;
			// console.log(data)

			if($scope.entradaproductos == ''){
				// console.log('no hay entradas')
			}
			else{
				$scope.nom_producto = data[0].nom_producto
				$scope.marca = data[0].marca
				$scope.mililitros = data[0].mililitros
			}

		}).error(function(error){
			console.log("Error."+error);
		});
	}
		// $scope.DetalleEntradaProduct();

// FUNCION PARA CONSULTAR DETALLE DE SALIDA DE PRODUCTOS - VENTAS ************************************************************

	$scope.DetalleSalidaProduct = function(){

		Productos.ListDetalleSalidaProd($routeParams.idproductos).success(function(data){
			$scope.salidaproductos = data;
			// console.log(data)
			if($scope.salidaproductos == '' || $scope.salidaproductos == null){
				// console.log('no hay salidas')
			}
			else{
				$scope.nom_producto = data[0].nom_producto
				$scope.marca = data[0].marca
				$scope.mililitros = data[0].mililitros
				// console.log($scope.nom_producto)
			}
		}).error(function(error){
			console.log("Error."+error);
		});
	}
		// $scope.DetalleSalidaProduct()

// FUNCION PARA MOVER PRODUCTOS DE SERVICIO A VITRIANA O VITRINA A SERVICIO ************************************************************

	$scope.MoverProductos = function(idproductos, nom_producto, tipo, marca, mililitros, valor_unit, existencia, idtipo_producto){

		var usuario = $('#usuario').val();

		alertify.prompt("Digite la cantidad que desea mover", function (info1, cantidad) {
			var mover = parseInt(cantidad);
			if(info1){
				// console.log(mover)
				if(cantidad == '' || cantidad == undefined || cantidad == NaN || cantidad == 0){
					alertify.error("<b>¡La cantidad está vacia!");

					
				}else if(!/^([0-9])*$/.test(mover)){
						alertify.error("<b>¡La cantidad debe ser un número!");
				 }
				 else if(mover > existencia){
						alertify.error("<b>¡No existe la cantidad de este producto!");
				 }
				else{
					alertify.confirm("<span class='warning'>PRODUCTO A MOVER</span><br><br><span class='ml20 f-l'>NOMBRE:  "+nom_producto+ "</span><br><span class='ml20 f-l'>TIPO:  "+tipo+"</span><br><span class='ml20 f-l'>MARCA:  "+marca+"</span><br><span class='cantidad ml20 f-l'>CANTIDAD:</span> <span class='ml10 f-l'>"+mover+"</span><br><br>¿Esta seguro?<br><br>", function (info2) {
						
						if(info2){

							alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está Moviendo el producto<br><i class="fa fa-exchange warning fs50 perfect mt10" aria-hidden="true"></i><br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
							$('.alertuse').css('display','none')

							$http({
								method:'POST',
								url:'includes/sql/Productos/Inventario/Mover_Productos_sql.php',
								header:{
								'Content-Type': undefined
							},
								data:{idproductos:idproductos, nom_producto:nom_producto, tipo:tipo, marca:marca, mililitros:mililitros, valor_unit:valor_unit, existencia:existencia, idtipo_producto:idtipo_producto, mover:mover, usuario:usuario}
							}).success(function(respuesta){
									$scope.mover = respuesta;
									// console.log(respuesta)

									if(respuesta == 1){
										setTimeout(function(){
											$('.alertuse').css('display','block')
											$('.textoalert').html('<p class="textoalert"><i class="fa fa-exchange warning fs50 perfect" aria-hidden="true"></i><br>¡Producto movido del inventario de vitrina!<br><i class="fa fa-exclamation-triangle mt30 warning fs50" aria-hidden="true"></i><br>Debe registrarle la nueva entrada en servicios</p>')
											window.location = "#/NewEntradaServ";
										 }, 4000);
									}else{
										setTimeout(function(){
											$('.alertuse').css('display','block')
											$('.textoalert').html('<p class="textoalert"><i class="fa fa-exchange warning fs50 perfect" aria-hidden="true"></i><br>¡Producto movido del inventario de servicios!<br><i class="fa fa-exclamation-triangle mt30 warning fs50" aria-hidden="true"></i><br>Debe registrarle la nueva entrada en vitrina</p>')
											window.location = "#/NewEntrada";
										 }, 4000);
									}

							}).error(function(err){
									console.log("Error: "+err);
								});
						}else{
							alertify.error("<b>Info: </b>¡Proceso cancelado!");
						}
					});
				}
			}else{
					alertify.error("<b>Info: </b>¡Proceso cancelado!");
				}
		});
	}
		// $scope.MoverProductos()


}]); // End Controller ProductosCtrl


// EMPLEADOS

// Controller EMPLEADOS //*********************************************************************************************

appGeneral.controller('EmpleadosCtrl', ['$http', '$scope', 'Empleados', '$routeParams', function($http, $scope, Empleados, $routeParams){

	$scope.CargaAnimated = function(){
		$('.cargar').velocity('transition.slideDownIn', {
	        stagger: 250,
	        drag: true,
	        duration: 2000,
	            complete: function () {
		            return $('.value').each(function (index, item) {
		                var val, value;
		                value = $(item).data('value');
		                val = parseInt(value, 10);
		                return $({ someValue: 0 }).animate({ someValue: val }, {
		                    duration: 1000,
		                    easing: 'swing',
		                    step: function () {
		                        return $(item).text(Math.round(this.someValue));
		                    }
		                });
		            });
		        }
	    });
	}
	$scope.CargaAnimated();

// FUNCION PARA REGISTRAR UN NUEVO EMPLEADO ************************************************************

	$scope.RegistrarEmpleado = function(empleado){

		alertify.confirm("¿Deseas registrar la información?", function (info) {

			if(info){
				// console.log(empleado)
				$http({
					method:'POST',
					url:'../web/includes/sql/Empleados/Registrar_Empleado_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:empleado
				}).success(function(respuesta){
					$scope.employee = respuesta;
						// console.log(respuesta)
						alertify.success("Información registrada con éxito");
						window.location = "#/employee";
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

// FUNCION PARA LISTAR LOS EMPLEADOS REGISTRADOS ************************************************************

	$scope.ListadoEmpleados = function(){

		Empleados.ListEmpleados().success(function(data){
			$scope.empleados = data;
			$scope.cant_empleados = data[0].cant_empleados
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoEmpleados();

// FUNCION PARA LISTAR LOS EMPLEADOS PARA EDITAR ************************************************************

	$scope.ListadoEmpleadosEdit = function(){

		Empleados.ListEmpleadosEd($routeParams.idempleados).success(function(data){
			$scope.empleados = data;
			$scope.idempleados = data[0].idempleados;
			$scope.documento = data[0].documento;
			$scope.nom_empleado = data[0].nom_empleado;
			$scope.ape_empleado = data[0].ape_empleado;
			$scope.telefono = data[0].telefono;
			$scope.cargo = data[0].cargo;
			$scope.fecha_nac = data[0].fecha_nac;
			$scope.salario = data[0].salario;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoEmpleadosEdit();

// FUNCION PARA EDITAR UN EMPLEADO ************************************************************

	$('.fa-pulse, #update, .fa-check').hide();

	$scope.EditEmpleado = function(idempleados){
		
		$('.fa-pulse, #update').show();
		$('.fa-check').hide();
		var salario = $('#salario').val();

		var newsalario = salario;
		var res_salario = newsalario.replace(',', "");
		salario = res_salario;

		datos = {
			idempleados:$scope.idempleados,
			nom_empleado:$scope.nom_empleado,
			ape_empleado:$scope.ape_empleado,
			telefono:$scope.telefono,
			cargo:$scope.cargo,
			fecha_nac:$scope.fecha_nac,
			salario:salario
		}
		// console.log(datos);
			$http({
				method:'POST',
				url:'../web/includes/sql/Empleados/Editar_Empleado_sql.php',
				header:{
				'Content-Type': undefined
			},
				data:datos
			}).success(function(respuesta){
				$scope.employee = respuesta;
					// console.log(respuesta)
					setTimeout(function(){
						$('.fa-pulse, #update').hide();
						$('.fa-check').show();
						alertify.success("Información actualizada con éxito");
					 }, 3000);
			}).error(function(err){
					console.log("Error: "+err);
				});
	}

// CAMBIAR ESTADO DEL EMPLEADO / ACTIVAR O DESACTIVAR ________________________________________________________
        
    $scope.CambiarEstadoEmpleado = function(idempleados, estado){
  	
    	// console.log('Estado es: ' + estado + ' ID EMPLEADO: ' + idempleados)

    	if(estado == 0){
    		estado = 1;
    	}else{
    		estado = 0;
    	}
    	// console.log('ESTADO A CAMBIAR: ' + estado + ' USER: ' + idempleados)

    	Empleados.CambEstadoEmpleado(idempleados, estado).success(function(data){
    		console.log(data);
    		$scope.ListadoEmpleados();
		}).error(function(error){
			console.log("Error."+error);
		});
    };



}]); // End Controller EmpleadosCtrl

// CLIENTES

// Controller CLIENTES //*********************************************************************************************

appGeneral.controller('ClientesCtrl', ['$http', '$scope', 'Clientes', '$routeParams', function($http, $scope, Clientes, $routeParams){

	$scope.CargaAnimated = function(){
		$('.cargar').velocity('transition.slideDownIn', {
	        stagger: 250,
	        drag: true,
	        duration: 2000,
	            complete: function () {
		            return $('.value').each(function (index, item) {
		                var val, value;
		                value = $(item).data('value');
		                val = parseInt(value, 10);
		                return $({ someValue: 0 }).animate({ someValue: val }, {
		                    duration: 1000,
		                    easing: 'swing',
		                    step: function () {
		                        return $(item).text(Math.round(this.someValue));
		                    }
		                });
		            });
		        }
	    });
	}
	$scope.CargaAnimated();

// FUNCION PARA REGISTRAR UN NUEVO EMPLEADO ************************************************************

	
	// VALIDAR EL MES
	$scope.validMes = function(){

		var mes = $scope.mes;

		if(mes == 0 || mes > 12){

			alertify.error("<b>Error: </b>¡Mes no valido!");
			$('#mes').val('');
			$('#mes').focus();
			// $('#guardaCliente').attr("disabled", "true");
			
		}else{

			var mes_pos = mes.charAt(0);

			if(mes_pos == 0){
				alertify.error("<b>Error: </b>¡Mes no valido!");
				$('#mes').val('');
				$('#mes').focus();
				// $('#guardaCliente').attr("disabled", "true");
			}else{
				// $('#guardaCliente').attr("disabled", "false");
			}
			// $('#guardaCliente').attr("disabled", "false");
		}

	}

	// VALIDAR EL MES
	$scope.validDia = function(){

		var mes = $scope.mes;
		var dia = $scope.dia;

		if(mes == 0){
			alertify.error("<b>Error: </b>¡Mes no valido!");
			$('#mes').val('');
			$('#mes').focus();
			// $('#guardaCliente').attr("disabled", "true");
		}else{
			// $('#guardaCliente').attr("disabled", "false");
		}

		if(dia == 0 || dia > 31){
			
			alertify.error("<b>Error: </b>¡Dia no valido!");
			$('#dia').val('');
			$('#dia').focus();
			// $('#guardaCliente').attr("disabled", "true");
			
		}else{

			var dia_pos = dia.charAt(0);

			if(dia_pos == 0){
				alertify.error("<b>Error: </b>¡dia no valido!");
				$('#dia').val('');
				$('#dia').focus();
				// $('#guardaCliente').attr("disabled", "true");
			}else{
				// $('#guardaCliente').attr("disabled", "false");
			}
			// $('#guardaCliente').attr("disabled", "false");
		}

	}

	$scope.RegistrarCliente = function(){

		var usuario = $('#usuario').val();

		var cliente = {
			documento:$scope.documento,
			nom_cliente:$scope.nom_cliente,
			ape_cliente:$scope.ape_cliente,
			telefono:$scope.telefono,
			correo:$scope.correo,
			mes:$scope.mes,
			dia:$scope.dia,
			usuario:usuario
		}

		// console.log(cliente);

		alertify.confirm("¿Deseas registrar la información?", function (info) {

			if (info) {
				$http({
					method:'POST',
					url:'../web/includes/sql/Clientes/Registrar_Cliente_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:cliente
				}).success(function(respuesta){
					$scope.clientes = respuesta;
					$scope.documento = respuesta;
					// console.log(respuesta);
					if($scope.documento == 1){
						$('#documento').addClass('error_input');
						$('#documento').focus();
						alertify.error("¡ERROR! Ya existe un cliente con el mismo N° de documento.");
						// alert('<i class="fa fa-times-circle-o fs50 error" aria-hidden="true"></i><br>Ya existe un cliente con el mismo N° de documento.<br>¡Cliente no Registrado!');
					}
					else if($scope.documento == 2){
						// alert('<i class="fa fa-check fs40 perfect" aria-hidden="true"></i> <br> Cliete registrado con éxito');
						alertify.success("<b>Info: </b>Información registrada con éxito");
						window.location = "#/clientes";
					}
				}).error(function(err){
					console.log("Error: "+err);
				   });	
			}
			else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

// FUNCION PARA LISTAR LOS CLIENTES REGISTRADOS ************************************************************

	$scope.ListadoClientes = function(){

		Clientes.ListClientes().success(function(data){

			$scope.clientes = data;
			// $scope.cant_clientes = data[0].cant_clientes
			// console.log($scope.cant_clientes)
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListadoClientes();

// FUNCION PARA LISTAR LOS SERVICIOS POR CLIENTE ************************************************************

	$scope.ServiciosClientes = function(){

		Clientes.ServClientes($routeParams.idclientes).success(function(data){

			$scope.serviciosclientes = data;
			// console.log(data)

			if($scope.serviciosclientes == ''){
				// console.log('no existe nada para este cliente')
			}else{			
				$scope.nom_cliente = data[0].nom_cliente
				$scope.ape_cliente = data[0].ape_cliente
				$scope.documento = data[0].documento
				$scope.cant_servicios = data[0].cant_servicios
			}
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListadoClientes();



// FUNCION PARA MOSTRAR NOMBRE Y COD DE TIPO DE SERVICIOS EN MODAL - GESTION DE SERVICIOS ************************************************************

	$scope.MostrarInfoClienteEdit = function(idcliente, documento, nom_cliente, ape_cliente, telefono, correo, fecha_nac){

		$scope.idclienteC = idcliente;
		$scope.documentoC = documento;
		$scope.nom_clienteC = nom_cliente;
		$scope.ape_clienteC = ape_cliente;
		$scope.telefonoC = telefono;
		$scope.correoC = correo;
		$scope.fecha_nacC = fecha_nac;

		var fecha_array = $scope.fecha_nacC.split('-');
		$scope.mes = fecha_array[1]
		$scope.dia = fecha_array[2]

		$('.fa-check').hide();

		// console.log('idcliente: ' + idcliente +' nom_cliente::::: ' + $scope.nom_cliente + ' ape_cliente: ' + ape_cliente + ' documento: ' + documento);

	}

	// End funcion $scope.MostrarInfoClienteEdit


// FUNCION PARA EDITAR INFORMACIÓN DE UN CLIENTE ************************************************************

	$('.fa-pulse, #update, .fa-check, .fa-spinner').hide();

	$scope.EditCliente = function(){
		
		$('.fa-pulse, #update').show();
		$('.fa-check').hide();

		datos = {
			idcliente:$scope.idclienteC,
			documento:$scope.documentoC,
			nom_cliente:$scope.nom_clienteC,
			ape_cliente:$scope.ape_clienteC,
			telefono:$scope.telefonoC,
			correo:$scope.correoC,
			mes:$scope.mes,
			dia:$scope.dia
		}

		// console.log(datos);

		$http({
			method:'POST',
			url:'../web/includes/sql/Clientes/Editar_Cliente_sql.php',
			header:{
			'Content-Type': undefined
		},
			data:datos
		}).success(function(respuesta){

			$scope.editcliente = respuesta;
			// console.log(respuesta);

			if($scope.editcliente == 1){
				$('#documentoC').addClass('error_input').focus();
				alertify.error("¡ERROR! Ya existe un cliente con el mismo N° de documento.");
				alert('<i class="fa fa-times-circle-o fs50 error animated wobble" aria-hidden="true"></i><br>¡Ya existe un cliente con el mismo N° de documento!<br>Información no actualizada');
			}
			if($scope.editcliente == 2){

				setTimeout(function(){
					$('.fa-pulse, #update').hide();
					$('.fa-check').show();
					alertify.success("Información actualizada con éxito");
					$scope.ListadoClientes();
				 }, 2000);
							
			}

		}).error(function(err){
				console.log("Error: "+err);
			});
	}	


}]); // End Controller ClientesCtrl


// VENTA DE SERVICIOS

// Controller VENTA DE SERVICIOS //*********************************************************************************************

appGeneral.controller('ServiciosCtrl', ['$http', '$scope', 'Servicios', 'ListGenerales', 'Productos', '$routeParams', function($http, $scope, Servicios, ListGenerales, Productos, $routeParams){

	// $scope.CurrentDate = new Date();

	$scope.CargaAnimated = function(){

		$('.cargar').velocity('transition.slideDownIn', {
	        stagger: 250,
	        drag: true,
	        duration: 2000,
	            complete: function () {
		            return $('.value').each(function (index, item) {
		                var val, value;
		                value = $(item).data('value');
		                val = parseInt(value, 10);
		                return $({ someValue: 0 }).animate({ someValue: val }, {
		                    duration: 1000,
		                    easing: 'swing',
		                    step: function () {
		                        return $(item).text(Math.round(this.someValue));
		                    }
		                });
		            });
		        }
	    });

	}

	$scope.CargaAnimated();
	    
	 //    var fechaok = new Date();
		// var anook = fechaok.getFullYear();
		// alert('El año actual es: '+anook);

		// var anook = (new Date).getFullYear();
 
		// $(document).ready(function() {
		//   // alert('999_ ' + anook);
		//   $("#anoactual").val(anook);
		// });


// FUNCION PARA REGISTRAR UN NUEVO SERVICIO **********************************************************************
// ***************************************************************************************************************

// FUNCION PARA ACTUALIZAR PRODUCTOS DEL INVENTARIO 1**************************************************************

	$scope.ListadoProductosInventario = function(idtipo_producto){

		Productos.ListProductosInventario(idtipo_producto).success(function(data){

			$scope.productinventarios = data;
			// console.log('Inventario actualizado');	

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListadoProductosInventario();

		$scope.cantidad = '';

		$('.seleccionar').hide();

	$scope.ValidarSolicitud = function(){

	// Productos Seleccionados ********************************

		$('.seleccionar').show();

		if($(".elemento").is(':checked')) {
	            // console.log("Activo");
	        } else {  
	            // console.log("No está activado"); 
	            $('.seleccionar').hide();
	        }

		var html='';
	    
	    $('.elemento:checked').each(function(index,valor) {
	        var texto = $(this).attr('data-nombre');
	        html +='<li class="text-azul"><span class="text-negro">'+texto+'</span></li>';
	        // html += '';
	        $('.seleccionado').html(html);
	        var cant = $('.elemento:checked').length;     
	        $('.prodseleccionado').html(cant);
	    });

	// Productos Seleccionados ********************************

		$('.cantidad').each(function(index, val){
			var existencia = parseInt($(this).attr('existencia'));
			var cantidad = parseInt($(this).val());
			var resta = existencia - cantidad;
			if(cantidad > existencia){
				$("input").prop('checked', false);
				alertify.error("<b>Info: </b>No existe la cantidad solicitada para este producto");
			}else{
				$('#guardar').show();
			}
		})

	}

// FUNCION PARA REGISTRAR UN NUEVO SERVICIO 
	
	$scope.RegistrarServicio = function(){

		var usuario = $('#usuario').val();
		// console.log(usuario);
		var valor = $scope.valor;
		var res_valor = valor.replace(',', "");
		valor = res_valor;
		// console.log('valor es: '+valor);
		var descuento = 0;

		var observacion = $scope.observacion;

		if(observacion == "" || observacion == " " || observacion == null || observacion == undefined){
			observacion = "Sin observación";
		}

		$('.elemento:checked').each(function() {
			descuento = descuento + parseFloat($(this).attr('desctotal'));
			// console.log(descuento)
		});

		var valor_final = valor-descuento;
		// console.log('desc final: ' + descuento);
		// console.log(observacion);

		alertify.confirm("¿Deseas registrar la información?", function (info) {
				
			var servicio = {
				idempleados:$scope.idempleados,
				idclientes:$scope.idclientes,
				idtipo_servicio:$scope.idtipo_servicio,
				tipo_pago:$scope.tipo_pago,
				valor:valor,
				descuento:descuento,
				valor_final:valor_final,
				observacion:observacion,
				usuario:usuario
			};

			// console.log(servicio);

				if(info){

					// console.log(servicio)

					alert('¡Espere un momento! <br>Se está guardando la información. <br><i class=" mt10 fa fa-spinner fa-spin fa-3x fa-fw"></i>')
					$('.alertuse').css('display','none')

					$http({
						method:'POST',
						url:'../web/includes/sql/Servicios/Registrar_NewServicio_sql.php',
						header:{
						'Content-Type': undefined
					},
						data:servicio
						}).success(function(respuesta){
							// console.log(respuesta);
							$scope.servicios = respuesta;
							$scope.idserv = respuesta;

							$('.elemento:checked').each(function() {

								var elementos = {
									idelemento:$(this).val(),
									descuento:$(this).attr('descuento'),
									cantidad:$(this).attr('cantidad'),
									idempleados:$(this).attr('empleado'),
									idtipo_producto:$(this).attr('idtipo_producto'),
									idservicio:$scope.idserv,
									tipo_pago:$scope.tipo_pago,
									usuario:usuario
								}
								// console.log(elementos);
								$http({
									method:'POST',
									url:'../web/includes/sql/Servicios/Registrar_ElmentoServicio_sql.php',
									header:{
									'Content-Type': undefined
								},
								data:elementos
								}).success(function(respuesta){
									$scope.idelementos = respuesta;
									$scope.ListadoProductosInventario(2);
									// console.log(respuesta);

								}).error(function(err){
									console.log("Error: "+err);
								});	

							});

							setTimeout(function(){
								window.location = "#/services";
								this._alertOverlay.remove();
            					this._alertWindow.remove();
								alertify.success("<b>Info: </b>Información registrada con éxito");
							 }, 3000);
						}).error(function(err){
							console.log("Error: "+err);
					});	
				}else{
					alertify.error("<b>Info: </b>¡Proceso cancelado!");
				}
		});
	}

// ***************************************************************************************************************

// FUNCION PARA CONSULTAR Y MOSTRAR SERVICIO PARA EDITARLO ************************************************************

	$scope.MostrarEditServicio = function(idservicio){
		
		Servicios.ListEditServicio(idservicio).success(function(data){

			$scope.editservicio = data;

			$scope.idservicio = data[0].idservicio;
			$scope.valor = data[0].valor;
			$scope.tipo_pago = data[0].tipo_pago;
			$scope.observacion = data[0].observacion;

			$scope.idempleados = data[0].idempleados;
			$scope.nom_empleado = data[0].nom_empleado;
			$scope.ape_empleado = data[0].ape_empleado;

			$scope.idclientes = data[0].idclientes;
			$scope.nom_cliente = data[0].nom_cliente + ' ' + data[0].ape_cliente;
			$scope.ape_cliente = data[0].ape_cliente;

			$scope.idtipo_servicio = data[0].idtipo_servicio;
			$scope.nom_tipo = data[0].nom_tipo;

			// console.log($scope.nom_cliente)

			$scope.ProductoServicioTodos($scope.idtipo_servicio);
			$scope.MostrarElemetosServicioEdit(idservicio);
			$scope.ProductoServicioNoUsados($scope.idtipo_servicio, idservicio);

		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.MostrarEditServicio();

// PRUEBAAAAAAAAAAAAAAAAA *********************************************************

	$scope.ProductoServicioTodos = function(tiposerv, idservicio){	

		Servicios.ProductServiceTodos(tiposerv, idservicio).success(function(data){

			$scope.elementostodos = data;
			// console.log(data)

		}).error(function(error){
			console.log("Error."+error);
		});


	}			


// FUNCION PARA CONSULTAR Y MOSTRAR ELEMENTOS PRODUCTOS USADOS EN EL SERVICIO PARA EDITARLO ************************************************************
	
	$scope.MostrarElemetosServicioEdit = function(idservicio){

		var arrayprod=[];

		Servicios.ListElementoServicio(idservicio).success(function(data){

			$scope.elementoseditservicio = data;
			$scope.usados = 1;

			// console.log(data);

			angular.forEach(data, function(element) {

				var producto_usado = element.idproductos;
				arrayprod.push(producto_usado);
				var producto_cantusada = element.cantidad_usada;
				console.log(producto_cantusada);

				setTimeout(function(){
					$('#cant_usada_'+producto_usado).text('Usados: ' + producto_cantusada);
					$('#cant_usada_'+producto_usado).attr('productousado', producto_cantusada);
					$('#product_'+producto_usado).attr('prueba', producto_cantusada);
					$('#product_'+producto_usado).attr('cantidad', producto_cantusada);
					console.log('MOSTRO')
				 }, 3000);

			});
				// console.log(arrayprod)
				arrayprod.forEach( function(elemento, index) {
					setTimeout(function(){
								$('#product_'+elemento).attr("checked", true);
								console.log('CHECKED')
							 }, 3000);
				});

		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.MostrarElemetosServicioEdit();

// *****************************************************************************************************************************

// FUNCION PARA LISTAR LOS PRODUCTOS QUE NOOO SE USARON EN EL SERVICIO *********************************************************

	$scope.ProductoServicioNoUsados = function(tiposerv, idservicio){

		Servicios.ProductServiceNoUso(tiposerv, idservicio).success(function(data){

			$scope.elementosnousados = data;
			// console.log(data)

		}).error(function(error){
			console.log("Error."+error);
		});
	}

// FUNCION PARA CONSULTAR LOS PRODUCTOS DE SERVICIO SEGUN TIPO DE SERVICIO SELECCIONADO *****************************************

	$('.fa-spinner').hide()

	$scope.ProductoServicio = function(tiposerv){

		if(tiposerv != 0){
			$('.fa-spinner').show()
		}

		ListGenerales.ProductService(tiposerv).success(function(data){

			$scope.elementos = data;
			// console.log(data)

		}).error(function(error){
			console.log("Error."+error);
		});
	}

// FUNCION PARA CONSULTAR TODOS SERVICIOS REGISTRADOS **************************************************************************

	$scope.ListadoServicios = function(idserv){

		$('.principal ').hide();

		swal({
          title: 'Consultando...',
          text: 'Espere un momento',
          timer: 2000,
          onOpen: () => {
            swal.showLoading()
          }
        }).then((result) => {
          if (result.dismiss === 'timer') {
          }
			$scope.today = new Date();
			
			Servicios.ListServicios(idserv).success(function(data){

				$scope.servicios = data;
				// console.log(data);

				if($scope.servicios == ''){
					// console.log('No hay servicios para hoy')
				}else{
					$scope.total_serviciosdia = data[0].total_serviciosdia;
					$scope.cant_serviciosdia = data[0].cant_serviciosdia;
				}
				
				$('.principal').show();
				// console.log(data);

			}).error(function(error){
				console.log("Error."+error);
			});
        })
        // End Swall
		
	}

	// $scope.ListadoServicios();


// FUNCION PARA ACTUALIZAR REGISTRAR UN NUEVO SERVICIO 
	
	$scope.ActualizarServicio = function(){

		var usuario = $('#usuario').val();
		// console.log('Usuario: ' + usuario);
		var valor = $scope.valor;
		var res_valor = valor.replace(',', "");
		valor = res_valor;
		console.log('valor es: '+ valor);
		var descuento = 0;

		$('.elemento:checked').each(function() {

			var cantidad = parseFloat($(this).attr('cantidad'));
			var productousado = parseFloat($(this).attr('prueba'));
			$scope.prueba = parseInt(productousado);
			console.log('nuevoooo---> : ' + productousado);

			descuento = descuento + parseFloat($(this).attr('desctotal'));
		});

			var valor_final = valor-descuento;
				// console.log(descuento);
			console.log('Descuento total: ' + descuento)

		// alertify.confirm("¿Deseas registrar la información?", function (info) {
				
		// 	var servicio = {
		// 		idempleados:$scope.idempleados,
		// 		idclientes:$scope.idclientes,
		// 		idtipo_servicio:$scope.idtipo_servicio,
		// 		tipo_pago:$scope.tipo_pago,
		// 		valor:valor,
		// 		descuento:descuento,
		// 		valor_final:valor_final,
		// 		observacion:$scope.observacion,
		// 		usuario:usuario
		// 	};
		// 	// console.log(servicio);

		// 		if(info){

		// 			// console.log(servicio)

		// 			alert('¡Espere un momento! <br>Se está guardando la información. <br><i class=" mt10 fa fa-spinner fa-spin fa-3x fa-fw"></i>')
		// 			$('.alertuse').css('display','none')

		// 			$http({
		// 				method:'POST',
		// 				url:'../web/includes/sql/Servicios/Registrar_NewServicio_sql.php',
		// 				header:{
		// 				'Content-Type': undefined
		// 			},
		// 				data:servicio
		// 				}).success(function(respuesta){
		// 					// console.log(respuesta);
		// 					$scope.servicios = respuesta;
		// 					$scope.idserv = respuesta;

		// 					$('.elemento:checked').each(function() {

		// 						var elementos = {
		// 							idelemento:$(this).val(),
		// 							descuento:$(this).attr('descuento'),
		// 							cantidad:$(this).attr('cantidad'),
		// 							idempleados:$(this).attr('empleado'),
		// 							idtipo_producto:$(this).attr('idtipo_producto'),
		// 							idservicio:$scope.idserv,
		// 							tipo_pago:$scope.tipo_pago,
		// 							usuario:usuario
		// 						}
		// 						// console.log(elementos);
		// 						$http({
		// 							method:'POST',
		// 							url:'../web/includes/sql/Servicios/Registrar_ElmentoServicio_sql.php',
		// 							header:{
		// 							'Content-Type': undefined
		// 						},
		// 						data:elementos
		// 						}).success(function(respuesta){
		// 							$scope.idelementos = respuesta;
		// 							$scope.ListadoProductosInventario(2);
		// 							// console.log(respuesta);

		// 						}).error(function(err){
		// 							console.log("Error: "+err);
		// 						});	

		// 					});

		// 					setTimeout(function(){
		// 						window.location = "#/services";
		// 						this._alertOverlay.remove();
  //           					this._alertWindow.remove();
		// 						alertify.success("<b>Info: </b>Información registrada con éxito");
		// 					 }, 3000);
		// 				}).error(function(err){
		// 					console.log("Error: "+err);
		// 			});	
		// 		}else{
		// 			alertify.error("<b>Info: </b>¡Proceso cancelado!");
		// 		}
		// });
	}


// *****************************************************************************************************************************


// // END BUSQUEDA SERVICIOS POR FECHA *****************************************************************************************
// // **************************************************************************************************************************

// FUNCION PARA REGISTRAR UN NUEVO SERVICIO (LAVA CABEZAS)

	$scope.RegistrarOtroServicio = function(){

		var usuario = $('#usuario').val();

		alertify.confirm("¿Deseas registrar la información?", function (info) {
				
			// console.log(servicio);
				if(info){
					// console.log(servicio)

					alert('¡Espere un momento! <br>Se está guardando la información. <br><i class=" mt10 fa fa-spinner fa-spin fa-3x fa-fw"></i>')
					$('.alertuse').css('display','none')

							$('.elemento:checked').each(function() {

								var elementos = {
									idproductos:$(this).val(),
									idempleados:$(this).attr('empleado'),
									cantidad:$(this).attr('cantidad'),
									tipo_pago:$scope.tipo_pago,
									valor_unit:$(this).attr('valor_unit'),
									descuento:$(this).attr('descuento'),
									idtipo_producto:$(this).attr('idtipo_producto'),
									usuario:usuario,
								}
								// console.log(elementos);

									$http({
										method:'POST',
										url:'../web/includes/sql/Servicios/Registrar_OtroServicio_sql.php',
										header:{
										'Content-Type': undefined
									},
									data:elementos
									}).success(function(respuesta){

										$scope.idproductos = respuesta;
										$scope.ListadoProductosInventario(2);
										// console.log(respuesta);
									
									}).error(function(err){
										console.log("Error: "+err);
									});	

							}); // end each

							setTimeout(function(){
								window.location = "#/OtrosServices";
								this._alertOverlay.remove();
            					this._alertWindow.remove();
								alertify.success("<b>Info: </b>Información registrada con éxito");
							 }, 3000);

				}else{
					alertify.error("<b>Info: </b>¡Proceso cancelado!");
				}
		});
	}

// *****************************************************************************************************************************

// FUNCION PARA CONSULTAR TODOS SERVICIOS REGISTRADOS LAVACABEZAS **************************************************************

	$scope.ListadoOtrosServiciosDia = function(){
		$scope.today = new Date();
		Servicios.ListOtrosServiciosDia().success(function(data){
			$scope.otrosservicios = data;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoOtrosServiciosDia();

// EMD FUNCION PARA CONSULTAR TODOS SERVICIOS REGISTRADOS LAVACABEZAS *********************************************************
// ****************************************************************************************************************************

	$scope.ListadoAnosSelect = function(){

		ListGenerales.ListAnos().success(function(data){
			$scope.anos = data;
			// console.log('1. ' + data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	$scope.ListadoAnosSelect();	

// END BUSQUEDA POR FECHA *****************************************************************************************************
// ****************************************************************************************************************************


// FUNCION PARA REGISTRAR UNA PROPINA *****************************************************************************************

	$scope.RegistrarNewPropina = function(propina){

		var usuario = $('#usuario').val();

		var propina = {
			idempleados:$scope.idempleados,
			valor:$scope.valor,
			descripcion:$scope.descripcion,
			usuario:usuario
		}
		// console.log(propina)

		alertify.confirm("¿Deseas registrar la información?", function (info) {

			if(info){
				$http({
					method:'POST',
					url:'../web/includes/sql/Servicios/Registrar_Propina_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:propina
				}).success(function(respuesta){
					$scope.propinas = respuesta;
						// console.log(respuesta)
						alertify.success("Información registrada con éxito");
						window.location = "#/Propinas";
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

// FUNCION PARA CONSULTAR LAS PROPINAS DEL DIA ************************************************************

	$scope.ListadoPropinasDia = function(){

		$scope.today = new Date();

		Servicios.ListPropinasdia().success(function(data){
			$scope.propinas = data;

			if($scope.propinas == ''){
				// console.log('no hay propinas para hoy')
			}else{
				$scope.suma_propinas = data[0].suma_propinas;
			}

			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoPropinasDia();

// FUNCION PARA ELIMINAR UN LAVA CABEZAS - FUNCION DE VENTA DE PRODUCTOS ************************************************************

	$scope.DeletePropina = function(idpropinas){

	// console.log(idpropinas);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Servicios/Delete_Propina_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idpropinas:idpropinas}
				}).success(function(respuesta){
					$scope.deleteventa = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ListadoPropinasDia();
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

	// End funcion $scope.DeletePropina

// FUNCION PARA ELIMINAR UN LAVA CABEZAS - FUNCION DE VENTA DE PRODUCTOS ************************************************************

	$scope.DeleteVentaProduct = function(idsalida_productos){

	// console.log(idsalida_productos);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Productos/Delete_VentaProduct_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idsalida_productos:idsalida_productos}
				}).success(function(respuesta){
					$scope.deleteventa = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ListadoOtrosServiciosDia();
							$scope.ListadoProductosInventario(2);
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

	// End funcion $scope.DeleteVentaProduct

// EMD FUNCION PARA CONSULTAR TODOS SERVICIOS DE LAVA CABEZAS REGISTRADOS *********************************************************



// FUNCION PARA CONSULTAR DETALLE DE SERVICIO INDIVIDUAL ************************************************************

	$scope.DetalleServicioInd = function(idempleados, idservicio){

		$('.modal-content').hide();

		swal({
          title: 'Cargando',
          text: 'Espere un momento...',
          timer: 1000,
          onOpen: () => {
            swal.showLoading()
          }
        }).then((result) => {
          if (result.dismiss === 'timer') {
			$('.modal-content').show();
          }
        })
        // End Swall

		Servicios.ListDetalleInd(idempleados, idservicio).success(function(data){
			
			$scope.detalles = data;
			// console.log($scope.detalles);
			$scope.descuento = data[0].descuento;
			$scope.nom_producto = data[0].nom_producto;
			$scope.nom_empleado = data[0].nom_empleado;
			$scope.ape_empleado = data[0].ape_empleado;
			$scope.nom_tipo = data[0].nom_tipo;
			$scope.fecha_servicio = data[0].fecha_servicio;
			$scope.nom_cliente = data[0].nom_cliente;
			$scope.ape_cliente = data[0].ape_cliente;
			$scope.valor = data[0].valor;
			$scope.valor_final = data[0].valor_final;
			$scope.valor_estilista = data[0].valor_estilista;
			$scope.valor_salon = data[0].valor_salon;
			$scope.tipo_pago = data[0].tipo_pago;
			$scope.descuento_total = data[0].descuento_total;
			$scope.total_salon = data[0].total_salon;
			$scope.tipo_pago = data[0].tipo_pago;
			$scope.observacion = data[0].observacion;
			// console.log(data);

			if($scope.observacion == null){
				$scope.observacion = 'Sin observaciones';
			}

		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.DetalleServicioInd();


// FUNCION PARA ELIMINAR UN SERVICIO ************************************************************

	$scope.DeleteService = function(idservicio){

	// console.log(idservicio);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Servicios/Delete_servicio_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idservicio:idservicio}
				}).success(function(respuesta){
					$scope.deleteservicio = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ListadoServicios(1)
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

	// End funcion $scope.DeleteService

// FUNCION PARA CONSULTAR TODOS LOS EMPLEADOS REGISTRADOS PARA SELECCION ************************************************************
	
	$scope.ListadoEmpleadosSelect = function(){

		ListGenerales.ListEmpleadosSelect().success(function(data){
			$scope.empleados_select = data;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoEmpleadosSelect();

// FUNCION PARA CONSULTAR TODOS TIPOS DE SERVICIO PARA SELECCION ************************************************************
	
	$scope.ListadoTipoServicioSelect = function(){

		ListGenerales.ListTipoService().success(function(data){
			$scope.tipo_servicios = data;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoTipoServicioSelect();


	$scope.ejemplo = function(id){
		// console.log('id es: ' +id)
	}

// FUNCION PARA CONSULTAR TODOS LOS PRODUCTOS REGISTRADOS PARA SELECCION ************************************************************

	$scope.ListadoProductosSelect = function(){

		ListGenerales.ListProductosSelect().success(function(data){
			$scope.productos_select = data;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListadoProductosSelect();


// FUNCION PARA CONSULTAR TODOS LOS CLIENTES REGISTRADOS PARA SELECCION ************************************************************

	$scope.ListadoClientesSelect = function(){

		ListGenerales.ListClientesSelect().success(function(data){
			$scope.clientes_select = data;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListadoClientesSelect();



}]); // End Controller ServiciosCtrl


// CONTROL INTERNO ******************************************************************************************************************************************************************************************************************************************************************
// ********************************************************************************************************************************************************************************************************************************************************************************

appGeneral.controller('ControlCtrl', ['$http', '$scope', 'ListGenerales', 'Control', 'Productos', 'Servicios', '$routeParams', function($http, $scope, ListGenerales, Control, Productos, Servicios, $routeParams){

// Controller CONTROL //*********************************************************************************************

	$scope.CargaAnimated = function(){

		$('.cargar').velocity('transition.slideDownIn', {
	        stagger: 250,
	        drag: true,
	        duration: 2000,
	            complete: function () {
		            return $('.value').each(function (index, item) {
		                var val, value;
		                value = $(item).data('value');
		                val = parseInt(value, 10);
		                return $({ someValue: 0 }).animate({ someValue: val }, {
		                    duration: 1000,
		                    easing: 'swing',
		                    step: function () {
		                        return $(item).text(Math.round(this.someValue));
		                    }
		                });
		            });
		        }
	    });
	}

	$scope.CargaAnimated();

	$scope.today = new Date();


// FUNCIÓN ACORDEÓN DE PANTALLA PRINCIPAL DE CONTROL ************************************************************

	var anchor = document.querySelectorAll('.controlMain');

	anchor.forEach(function(item, index){

		$('#controlMain' + (index + 1)).click(function(){
			if(!$('#collapse' + (index + 1)).is(':visible')){
				$('.collapse').slideUp();
				$('#collapse' + (index + 1)).slideDown();
			} else {
				$('#collapse' + (index + 1)).slideUp();
			}

		});		

	});

// Funcion para visualizar cantidad de liquidaciones por realizar y realiadas - enviar a correo


	$scope.ListadoRegistroLiquidaciones = function(){

		Control.ListLiquidacionesReg().success(function(data){

			$scope.liquidacionesreg = data;

			$scope.para_liquidar = data[0].para_liquidar;
			$scope.liquidados = data[0].liquidados;
			$scope.falta_liquidar = data[0].falta_liquidar;

			// console.log(data);

		}).error(function(error){
			console.log("Error."+error);
		});
	}


// FUNCION PARA LISTAR LOS EMPLEADOS PARA RELIZAR LIQUIDACIÓN ************************************************************

	$scope.ListadoEmpleadosLiquidar = function(){
		
		$scope.today = new Date();
		
		Control.ListEmpleadosLiquidar().success(function(data){
			$scope.empleadosliquidar = data;
			// console.log(data)
			$scope.ListadoRegistroLiquidaciones();
		}).error(function(error){
			console.log("Error."+error);
		});

	}

	$scope.ListadoEmpleadosAuxLiquidar = function(){
		$scope.today = new Date();
		Control.ListEmpleadosAuxLiquidar().success(function(data){
			$scope.empleadosAuxliquidar = data;
			// console.log(data)
			// $scope.ListadoRegistroLiquidaciones();
		}).error(function(error){
			console.log("Error."+error);
		});
	}




// FUNCION PARA REALIZAR LIQUIDACIÓN INDIVIDUAL DIARIA EMPLEADO ************************************************************

	$scope.LiquidacionIndividual = function(){

		$('.principal ').hide();

		swal({
          title: 'Consultando...',
          text: 'Espere un momento',
          timer: 2000,
          onOpen: () => {
            swal.showLoading()
          }
        }).then((result) => {
          if (result.dismiss === 'timer') {
			$('.principal').show();
          }
        })
        // End Swall

		Control.DetalleLiquidac($routeParams.idempleados).success(function(data){
			
			$scope.liquidadiones = data;

		if($scope.liquidadiones == ''){
			// console.log('no hay liquidacion individual')
		}else{
			// console.log(data);
				$scope.idservicio = data[0].idservicio;
				$scope.cant_servicios = data[0].cant_servicios;
				$scope.desc_individual = data[0].desc_individual;
				$scope.nom_product = data[0].nom_product;
				$scope.idempleados = data[0].idempleados;
				$scope.nom_empleado = data[0].nom_empleado;
				$scope.ape_empleado = data[0].ape_empleado;
				$scope.nom_tipo = data[0].nom_tipo;
				$scope.fecha_servicio = data[0].fecha_servicio;
				$scope.nom_cliente = data[0].nom_cliente;
				$scope.ape_cliente = data[0].ape_cliente;
				$scope.valor = data[0].valor;
				$scope.valor_final = data[0].valor_final;
				$scope.valor_estilista = data[0].valor_estilista;
				$scope.valor_salon = data[0].valor_salon;
				$scope.tipo_pago = data[0].tipo_pago;
				$scope.descuento_total = data[0].descuento_total;
				$scope.total_salon = data[0].total_salon;
				$scope.valor_vales = parseInt(data[0].valor_vales);
				$scope.tipo_pago = data[0].tipo_pago;
				$scope.total_servicios = parseInt(data[0].total_servicios);
				$scope.total_estilista = parseInt(data[0].total_estilista);
				$scope.total_ventas = parseInt(data[0].total_ventas);
				$scope.estado_liquidado = data[0].estado_liquidado;
				
				$scope.total_gen_servicios = data[0].total_gen_servicios;
				$scope.total_desc_servicios = data[0].total_desc_servicios;
				$scope.total_gen_servicios_desc = data[0].total_gen_servicios_desc;

				$scope.ListadoRegistroLiquidaciones();
		}

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.LiquidacionIndividual();

// FUNCION PARA REALIZAR LIQUIDACIÓN INDIVIDUAL DIARIA EMPLEADO ************************************************************

	$scope.LiquidacionIndividualDate = function(){

		Control.DetalleLiquidacDate($routeParams.idempleados, $routeParams.fecha).success(function(data){

			$scope.liquidaciondate = data;
			$scope.fecha_liquidacion = $routeParams.fecha;

		if($scope.liquidaciondate == ''){
			// console.log('no hay liquidacion individual')
		}else{
				// console.log(data);
				$scope.idservicio = data[0].idservicio;
				$scope.cant_servicios = data[0].cant_servicios;
				$scope.desc_individual = data[0].desc_individual;
				$scope.nom_product = data[0].nom_product;
				$scope.idempleados = data[0].idempleados;
				$scope.nom_empleado = data[0].nom_empleado;
				$scope.ape_empleado = data[0].ape_empleado;
				$scope.nom_tipo = data[0].nom_tipo;
				$scope.fecha_servicio = data[0].fecha_servicio;
				$scope.nom_cliente = data[0].nom_cliente;
				$scope.ape_cliente = data[0].ape_cliente;
				$scope.valor = data[0].valor;
				$scope.valor_final = data[0].valor_final;
				$scope.valor_estilista = data[0].valor_estilista;
				$scope.valor_salon = data[0].valor_salon;
				$scope.tipo_pago = data[0].tipo_pago;
				$scope.descuento_total = data[0].descuento_total;
				$scope.total_salon = data[0].total_salon;
				$scope.valor_vales = parseInt(data[0].valor_vales);
				$scope.tipo_pago = data[0].tipo_pago;
				$scope.total_servicios = parseInt(data[0].total_servicios);
				$scope.total_estilista = parseInt(data[0].total_estilista);
				$scope.total_ventas = parseInt(data[0].total_ventas);
				$scope.estado_liquidado = data[0].estado_liquidado;
				
				$scope.total_gen_servicios = data[0].total_gen_servicios;
				$scope.total_desc_servicios = data[0].total_desc_servicios;
				$scope.total_gen_servicios_desc = data[0].total_gen_servicios_desc;
		}

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.LiquidacionIndividualDate();


// FUNCION PARA REALIZAR LIQUIDACIÓN INDIVIDUAL DIARIA EMPLEADO VALES ************************************************************

	$scope.ListValesLiquidacion = function(){

		Control.DetalleVales($routeParams.idempleados).success(function(data){
			$scope.vales = data;
			if($scope.total_vales == '' || $scope.total_vales == null){
				// console.log('esta vacio')
			}
			else{
				$scope.total_vales = data[0].total_vales
				// console.log($scope.vales)
			}

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListValesLiquidacion();

// FUNCION PARA REALIZAR CONSULTA LIQUIDACION INDIVIDUAL POR FECHA EMPLEADO VALES ************************************************************

	$scope.ListValesLiquidacionDate = function(){

		Control.DetalleValesDate($routeParams.idempleados, $routeParams.fecha).success(function(data){
			$scope.vales = data;
			// console.log(data)
			if($scope.total_vales == '' || $scope.total_vales == null){
				// console.log('esta vacio')
			}
			else{
				$scope.total_vales = data[0].total_vales
				// console.log($scope.vales)
			}

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListValesLiquidacionDate();


// FUNCION PARA REALIZAR LIQUIDACIÓN INDIVIDUAL DIARIA EMPLEADO VENTAS ************************************************************

	$scope.ListVentasLiquidacion = function(){

		Control.DetalleVentas($routeParams.idempleados).success(function(data){
			
			$scope.ventas = data;

			if($scope.ventas == '' || $scope.ventas == null){
				// console.log('no hay ventas para el empleado')
			}
			else{
				$scope.total_venta = parseInt(data[0].total_venta);
			}
			// console.log($scope.total_venta)

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListVentasLiquidacion();

// FUNCION PARA BUSCAR LIQUIDACIÓN INDIVIDUAL DIARIA EMPLEADO VENTAS POR CUALQUIER FECHA ************************************************************

	$scope.ListVentasLiquidacionDate = function(){

		Control.DetalleVentasDate($routeParams.idempleados, $routeParams.fecha).success(function(data){
			
			$scope.ventas = data;
			// console.log(data);

			if($scope.ventas == '' || $scope.ventas == null){
				// console.log('no hay ventas para el empleado')
			}
			else{
				$scope.total_venta = parseInt(data[0].total_venta);
			}
			// console.log($scope.total_venta)

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListVentasLiquidacionDate();


// FUNCION PARA REALIZAR LIQUIDACIÓN INDIVIDUAL DIARIA EMPLEADO LAVACABEZAS ************************************************************

	$scope.ListLavaCabezasDia = function(){

		Control.DetalleLavacabDia($routeParams.idempleados).success(function(data){
			
			$scope.lavacabezas = data;

			if($scope.lavacabezas == '' || $scope.lavacabezas == null){
				// console.log('no hay lavacabezas para el empleado')
				if($scope.estado_liquidado == undefined){
					$scope.estado_liquidado = 0;
					$scope.idempleados = $routeParams.idempleados;
					// console.log($scope.estado_liquidado)
					// console.log('Empleado: '+$routeParams.idempleados)
				}
			}
			else{
				
				$scope.estilista_pservicios = parseInt(data[0].estilista_pservicios);
				$scope.cant_servicios = data[0].cant_servicios;
				$scope.idempleados = data[0].idempleados;
				$scope.nom_empleado = data[0].nom_empleado;
				$scope.ape_empleado = data[0].ape_empleado;
				$scope.estado_liquidado = data[0].estado_liquidado;

				$scope.ListadoRegistroLiquidaciones();

			}
			// console.log(data)

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListLavaCabezasDia();

// // FUNCION PARA REALIZAR LIQUIDACIÓN INDIVIDUAL POR FECHA EMPLEADO LAVACABEZAS ************************************************************

	$scope.ListLavaCabezasDate = function(){

		Control.DetalleLavacabDate($routeParams.idempleados, $routeParams.fecha).success(function(data){
			
			$scope.lavacabezas = data;

			if($scope.lavacabezas == '' || $scope.lavacabezas == null){
				// console.log('no hay lavacabezas para el empleado')
			}
			else{
				$scope.estilista_pservicios = parseInt(data[0].estilista_pservicios);
			}
			// console.log(data)

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListVentasLiquidacionDate();

// FUNCION PARA REALIZAR LIQUIDACIÓN INDIVIDUAL DIARIA EMPLEADO PROPINAS ************************************************************

	$scope.ListPropinasLiquidacion = function(){

		Control.DetallePropinas($routeParams.idempleados).success(function(data){

			// console.log(data)
			
			$scope.propinas = data;

			if($scope.propinas == '' || $scope.propinas == null){
				// console.log('no hay propinas para el empleado')
			}
			else{
				$scope.total_propinas = parseInt(data[0].total_propinas);
			}
			// console.log($scope.total_propinas)

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListPropinasLiquidacion();


// FUNCION PARA REALIZAR CONSULTA A LIQUIDACIÓN INDIVIDUAL POR FECHA EMPLEADO PROPINAS ************************************************************

	$scope.ListPropinasLiquidacionDate = function(){

		Control.DetallePropinasDate($routeParams.idempleados, $routeParams.fecha).success(function(data){
			
			// console.log(data)
			
			$scope.propinas = data;

			if($scope.propinas == '' || $scope.propinas == null){
				// console.log('no hay propinas para el empleado')
			}
			else{
				$scope.total_propinas = parseInt(data[0].total_propinas);
			}
			// console.log($scope.total_propinas)

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListPropinasLiquidacionDate();


// FUNCION CERRAR CAJA DIARIA A CADA TRABAJADOR ************************************************************

	$scope.CerrarCajaDiaria = function(){

		var usuario = $('#usuario').val();
		var valor_liquidacion = $('#valor_liquidacion').val();

		var liquidados = parseInt($scope.liquidados) + 1;

		var caja = {
			valor_liquidacion:valor_liquidacion,
			idempleados:$scope.idempleados,
			usuario:usuario,
			para_liquidar:$scope.para_liquidar,
			liquidados:liquidados,
			falta_liquidar:$scope.falta_liquidar
		};

		// console.log(caja)

		alertify.confirm("¿Deseas registrar la información?", function (info) {

			if(info){

				alert('¡Espere un momento! <br>Se está guardando la información. <br><i class=" mt10 fa fa-spinner fa-spin fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Registrar_Caja_Diaria_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:caja
				}).success(function(respuesta){
					$scope.cajadiaria = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							window.location = "#/ControlDay";
							this._alertOverlay.remove();
							this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información registrada con éxito");
						}, 3000);

						if(respuesta == 1){
							console.log('Correo enviado exitosamente');
						}else{
							console.log('No se envió en correo');
						}
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

// FUNCION PARA CONSULTAR TODOS LOS REGISTRO DE LIQUIDACION ************************************************************

	$scope.ListRegistroLiquidacionesDia = function(){

		$scope.ListadoRegistroLiquidaciones();

		$scope.today = new Date();

		Control.ListRegistroLiquid().success(function(data){
			
			$scope.registroliquidaciones = data;

			if($scope.registroliquidaciones == ''){
				// console.log('hoy no hay liquidaciones');
			}else{
				$scope.total_liquidacion = data[0].total_liquidacion;
				$scope.cant_liquidacion = data[0].cant_liquidacion;
			}
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListRegistroLiquidacionesDia()

// ***************************************************************************************************************

// FUNCION PARA CONSULTAR SERVICIOS REALIZADOS POR MES ************************************************************

	$scope.today = new Date();

	$scope.ListRegistroLiquidacionesMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
			// console.log($scope.nodata)
		}else{

			var mes = $scope.mes;
			var year = $scope.year;
			var idempleados = $scope.idempleados;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes o Día!");
				if(idempleados == undefined){
					alertify.log("<b>Info: </b>¡Debe Seleccionar un Estilista!");
				}
			}
			else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListLiquidacionMes(mes, year ,idempleados).success(function(data){

					$scope.liquidacionesdate = data;
					// console.log(data);

					if($scope.liquidacionesdate == ''){
						// console.log('No hay servicios en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.total_liquidacion = data[0].total_liquidacion;
						$scope.cant_liquidacion = data[0].cant_liquidacion;
						$scope.nom_empleado = data[0].nom_empleado;
						$scope.ape_empleado = data[0].ape_empleado;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 4000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.ListRegistroLiquidacionesMes();

// ***************************************************************************************************************
// ***************************************************************************************************************

// BUSQUEDA DE LIQUIDACIONES POR FECHA X EMPLEADO ***********************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.ListRegistroLiquidacionesDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			var idempleados = $scope.idempleados;

			if(idempleados == undefined){
				
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Estilista!");

				if(fecha_con == ''){
					alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				}

			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);
				var year = fecha_con.substr(0,[4]);
				var mes = fecha_con.substr(5,[2]);
				var dia = fecha_con.substr(8,[2]);

				// console.log('año: '+year+' mes: '+mes+' dia: '+dia)

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListLiquidacionDate(year, mes, dia, idempleados).success(function(data){
					
					// console.log(data);

					$scope.liquidacionesdate = data;

					if($scope.liquidacionesdate == ''){
						// console.log('No hay servicios en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.total_liquidacion = data[0].total_liquidacion;
						$scope.cant_liquidacion = data[0].cant_liquidacion;
						$scope.nom_empleado = data[0].nom_empleado;
						$scope.ape_empleado = data[0].ape_empleado;
						// console.log($scope.total_servicios)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 2000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ListRegistroLiquidacionesDate();

// END BUSQUEDA POR FECHA ***********************************************************************************
// ***************************************************************************************************************


// FUNCION PARA CONSULTAR LAS LIQUIDACIONES EN GENERAL *********************************************************************************************

	$scope.today = new Date();

	$scope.ListRegistroLiquidacionesMesGen = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
				$scope.nodata = 1;
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListLiquidacionMesGen(mes, year).success(function(data){

					$scope.liquidacionesdate = data;

					if($scope.liquidacionesdate == ''){
						// console.log('No hay PRESTAMOS MES en esta consulta');
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.total_liquidacion = data[0].total_liquidacion;
						$scope.cant_liquidacion = data[0].cant_liquidacion;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 3000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}
	// $scope.ListRegistroLiquidacionesMesGen();



// FUNCION PARA CONSULTAR LOS PRESTAMOS QUINCENALES POR FECHA *********************************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.ListRegistroLiquidacionesDateGen = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListLiquidacionDateGen(fecha_con).success(function(data){
					
					$scope.liquidacionesdate = data;

					if($scope.liquidacionesdate == ''){
						// console.log('No hay PRESTAMOS QUINCENA MES en esta consulta');
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.total_liquidacion = data[0].total_liquidacion;
						$scope.cant_liquidacion = data[0].cant_liquidacion;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 3000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ListRegistroLiquidacionesDateGen();	





// FUNCION PARA ELIMINAR UNA LIQUIDACIÓN ************************************************************

	$scope.DeleteLiquidacion = function(idliquidacion, idempleados, valor_liquidacion){

		var datos = {
			idempleados:idempleados,
			idliquidacion:idliquidacion,
			valor_liquidacion:valor_liquidacion,
			para_liquidar:$scope.para_liquidar,
			liquidados:$scope.liquidados,
			falta_liquidar:$scope.falta_liquidar
		};

		// console.log(datos);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Delete_registro_liquidacion_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:datos
				}).success(function(respuesta){
					$scope.deleteliquidacion = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ListRegistroLiquidacionesDia()
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}

		});
	}

// FUNCION PARA CONSULTAR TODOS LOS EMPLEADOS REGISTRADOS PARA SELECCION NO MUESRTRA LOS LIQUIDADOS ************************************************************
	
	$scope.ListadoEmpleadosSelect = function(){

		ListGenerales.ListEmpleadosSelect().success(function(data){
			$scope.empleados_select = data;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoEmpleadosSelect();

// FUNCION PARA CONSULTAR TODOS LOS EMPLEADOS REGISTRADOS PARA SELECCION OK GENERAL ************************************************************
	
	$scope.ListadoEmpleadosSelectGeneral = function(){

		ListGenerales.ListEmpleadosGeneral().success(function(data){
			$scope.empleados_select = data;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoEmpleadosSelectGeneral();

// FUNCION PARA CONSULTAR TODOS LOS EMPLEADOS REGISTRADOS PARA SELECCION OK GENERAL ************************************************************
	
	$scope.ListadoAnosSelect = function(){
		ListGenerales.ListAnos().success(function(data){
			$scope.anos = data;
			// console.log('2. ' + data)
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	$scope.ListadoAnosSelect();	

// FUNCION PARA REGISTRAR UN NUEVO EMPLEADO ************************************************************

	$scope.RegistrarNewVale = function(){

		var usuario = $('#usuario').val();

		var vale = {
			idempleados:$scope.idempleados,
			valor:$scope.valor,
			descripcion:$scope.descripcion,
			usuario:usuario
		}
		// console.log(vale)

		alertify.confirm("¿Deseas registrar la información?", function (info) {

			if(info){
				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Registrar_Vale_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:vale
				}).success(function(respuesta){
					$scope.vales = respuesta;
						// console.log(respuesta)
						alertify.success("Información registrada con éxito");
						window.location = "#/Vales";
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

// FUNCION PARA CONSULTAR TODOS VALES REGISTRADOS ************************************************************

	$scope.ListadoVales = function(idvale){

		$scope.today = new Date();

		Control.ListVales(idvale).success(function(data){

			$scope.valesgen = data;

			if($scope.valesgen == ''){
				// console.log('no hay vales para hoy')
			}else{
				$scope.suma_vales = data[0].suma_vales;
			}

			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoVales();

// FUNCION PARA CONSULTAR TODOS VALES REGISTRADOS POR MES SELECCIONADO ************************************************************

	$scope.today = new Date();

	$scope.ListadoValesMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
				$scope.nodata = 1;
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListValesMes(mes, year).success(function(data){

					$scope.valesgen = data;

					if($scope.valesgen == ''){
						// console.log('No hay vales MES en esta consulta');
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.suma_vales = data[0].suma_vales;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 5000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.ListadoValesMes();


// FUNCION PARA CONSULTAR TODOS VALES REGISTRADOS POR FECHA SELECCIONADA ************************************************************

	$("#fecha_consulta").datepicker();

    $scope.ListadoValesDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListValesDate(fecha_con).success(function(data){
					
					$scope.valesgen = data;

					if($scope.valesgen == ''){
						// console.log('No hay vales en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.suma_vales = data[0].suma_vales;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 4000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ListadoValesDate();	

// FUNCION PARA CONSULTAR VALES Y EDITARLAOS ************************************************************

	$scope.ListadoValesEdit = function(){

		Control.ListValesEdi($routeParams.idvales).success(function(data){
			
			$scope.vales = data;

			$scope.idvales = data[0].idvales;
			$scope.nom_empleado = data[0].nom_empleado;
			$scope.ape_empleado = data[0].ape_empleado;
			$scope.valor = parseInt(data[0].valor);
			$scope.descripcion = data[0].descripcion;
			$scope.fecha_vale = data[0].fecha_vale;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoValesEdit();

// FUNCION PARA EDITAR UN VALE ************************************************************

	$('.fa-pulse, #update, .fa-check').hide();

	$scope.EditVale = function(idvales){
		
		var valor = $scope.valor;

		if(valor <= 0 || valor == undefined){
  			alertify.error("<b>Info: </b>¡Campos invalidos!");
  		} 
  	// 	else if(!/^([0-9])*$/.test(valor) || !/^([0-9])*$/.test(valor)){
			// 	alertify.error("<b>¡Los valores deben ser un número!");
			// }
  		else{
			$('.fa-pulse, #update').show();
			$('.fa-check').hide();

			datos = {
				idvales:idvales,
				valor:valor,
				descripcion:$scope.descripcion,
			}
				// console.log(datos);
				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Editar_Vale_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:datos
				}).success(function(respuesta){
					$scope.vales = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.fa-pulse, #update').hide();
							$('.fa-check').show();
							$('#cantidad, #total').val('');
							alertify.success("Información actualizada con éxito");
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
  		}
	}

// FUNCION PARA ELIMINAR UNA ENTRADA DE PRODUCTO AL INVENTARIO ************************************************************

	$scope.DeleteVale = function(idvales){

		// console.log(idvales);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Delete_Vale_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idvales:idvales}
				}).success(function(respuesta){
					$scope.deleteentrada = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ListadoVales(1);
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

	// End funcion $scope.DeleteVale

// FUNCION PARA REGISTRAR UN NUEVO OTRO INGRESO ************************************************************

	$scope.RegistrarOtroIngreso = function(ingreso){

		var usuario = $('#usuario').val();

		var ingreso = {
			valor:$scope.valor,
			tipo_pago:$scope.tipo_pago,
			descripcion:$scope.descripcion,
			usuario:usuario
		}

		// console.log(ingreso)
		alertify.confirm("¿Deseas registrar la información?", function (info) {

			if(info){
				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Registrar_Otro_Ingreso_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:ingreso
				}).success(function(respuesta){
					$scope.ingreso = respuesta;
						// console.log(respuesta)
						alertify.success("Información registrada con éxito");
						window.location = "#/OtrosIngresos";
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

	// End funcion $scope.RegistrarOtroIngreso

// FUNCION PARA CONSULTAR OTROS INGRESOS REGISTRADOS ************************************************************

	$scope.ListadoOtrosIngresosDia = function(){

		$scope.today = new Date();

		Control.ListOtrosIngDia().success(function(data){
			$scope.otrosingresos = data;
			if($scope.otrosingresos == ''){
				// console.log('no hay otros ingresos');
			}else{
				$scope.valor_ingresos = parseInt(data[0].valor_total);
				$scope.total_tarjeta_ing = parseInt(data[0].total_tarjeta);
				$scope.total_efectivo_ing = parseInt(data[0].total_efectivo);
			}
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoOtrosIngresosDia();


// FUNCION PARA CONSULTAR OTROS INGRESOS REALIZADOS POR MES CONTROL GENERAL ************************************************************ CONTROL GENERAL

	$scope.today = new Date();

	$scope.CtrlListadoOtrosIngresosMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
				$scope.nodata = 1;
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListOtrosIngMes(mes, year).success(function(data){

					$scope.otrosingresos = data;

					if($scope.otrosingresos == ''){
						// console.log('No hay OOOTROS INGRESOS MES en esta consulta');
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.valor_ingresos = parseInt(data[0].valor_total);
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 3000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.CtrlListadoOtrosIngresosMes();


// ***************************************************************************************************************

// FUNCION PARA CONSULTAR OTROS INGRESOS POR CULQUIER FECHA ************************************************************************* CONTROL GENERAL

	$("#fecha_consulta").datepicker();

    $scope.CtrlListadoOtrosIngresosDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListOtrosIngDate(fecha_con).success(function(data){
					
					$scope.otrosingresos = data;

					if($scope.otrosingresos == ''){
						// console.log('No hay OTROS INGREOS en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.valor_ingresos = parseInt(data[0].valor_total);
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 4000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.CtrlListadoOtrosIngresosDate();



// END BUSQUEDA POR FECHA ***********************************************************************************
// ***************************************************************************************************************




// FUNCION PARA CONSULTAR OTROS INGRESOS DEL MES ************************************************************ INGRESOS DEL DIA

	$scope.today = new Date();

	$scope.ListadoOtrosIngresosMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListOtrosIngMes(mes, year).success(function(data){

					$scope.otrosingresos = data;

					if($scope.otrosingresos == ''){
						// console.log('No hay ingresos en esta consulta')

					}else{
						$scope.valor_ingresos = parseInt(data[0].valor_total);
						$scope.total_tarjeta_ing = parseInt(data[0].total_tarjeta);
						$scope.total_efectivo_ing = parseInt(data[0].total_efectivo);
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.ListadoOtrosIngresosMes();

// FUNCION PARA CONSULTAR LOS INGRESOS POR CULQUIER FECHA *********************************************************************************** INGRESOS DEL DIA

	$("#fecha_consulta").datepicker();

    $scope.ListadoOtrosIngresosDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListOtrosIngDate(fecha_con).success(function(data){
					
					$scope.otrosingresos = data;

					if($scope.otrosingresos == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{
						$scope.valor_ingresos = parseInt(data[0].valor_total);
						$scope.total_tarjeta_ing = parseInt(data[0].total_tarjeta);
						$scope.total_efectivo_ing = parseInt(data[0].total_efectivo);
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ListadoOtrosIngresosDate();

// FUNCION PARA CONSULTAR OTROS INGRESOS Y EDITARLAOS ************************************************************

	$scope.ListadoOtrosIngEdit = function(){

		Control.ListOtrosIngEdi($routeParams.idotros_ingresos).success(function(data){
			
			$scope.otros_ingresos = data;
			// console.log(data)
			$scope.idotros_ingresos = data[0].idotros_ingresos;
			$scope.valor = parseInt(data[0].valor);
			$scope.tipo_pago = data[0].tipo_pago;
			$scope.descripcion = data[0].descripcion;
			$scope.fecha_ingreso = data[0].fecha_ingreso;
			
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoOtrosIngEdit();

// FUNCION PARA EDITAR INGRESO (OTRO INGRESO) ************************************************************

	$('.fa-pulse, #update, .fa-check').hide();

	$scope.EditOtroIngreso = function(idotros_ingresos){
		
		var valor = $scope.valor;

		if(valor <= 0 || valor == undefined){
  			alertify.error("<b>Info: </b>¡Campos invalidos!");
  		} else if(!/^([0-9])*$/.test(valor) || !/^([0-9])*$/.test(valor)){
				alertify.error("<b>¡Los valores deben ser un número!");
			}
  		else{
			$('.fa-pulse, #update').show();
			$('.fa-check').hide();

			datos = {
				idotros_ingresos:idotros_ingresos,
				valor:valor,
				tipo_pago:$scope.tipo_pago,
				descripcion:$scope.descripcion,
			}
				// console.log(datos);
				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Editar_OtroIngreso_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:datos
				}).success(function(respuesta){
					$scope.vales = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.fa-pulse, #update').hide();
							$('.fa-check').show();
							$('#cantidad, #total').val('');
							alertify.success("Información actualizada con éxito");
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
  		}
	}

// FUNCION PARA ELIMINAR UNA INGRESO (OTROS INGRESOS) ************************************************************

	$scope.DeleteOtroIngreso = function(idotros_ingresos){

		// console.log(idotros_ingresos);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Delete_OtroIngreso_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idotros_ingresos:idotros_ingresos}
				}).success(function(respuesta){
					$scope.deleteentrada = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ListadoOtrosIngresosDia();
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

// End funcion $scope.DeleteOtroIngreso

// FUNCION PARA REGISTRAR UNA SALIDA DE DIA ************************************************************

	$scope.RegistrarNewSalida = function(){

		var usuario = $('#usuario').val();

		var salida = {
			nom_responsable:$scope.nom_responsable,
			valor:$scope.valor,
			descripcion:$scope.descripcion,
			usuario:usuario
		}

		// console.log(salida)

		alertify.confirm("¿Deseas registrar la información?", function (info) {

			if(info){
				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Registrar_Salida_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:salida
				}).success(function(respuesta){
					$scope.salidas = respuesta;
						// console.log(respuesta)
						alertify.success("Información registrada con éxito");
						window.location = "#/SalidasDay";
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

// FUNCION PARA CONSULTAR LAS SALIDAS DEL DIA DE HOY ************************************************************

	$scope.ListadoSalidas = function(){
		
		$scope.today = new Date();

		Control.ListSalidasDia().success(function(data){
			$scope.salidas = data;
			// console.log(data)
			if($scope.salidas == ''){
				// console.log('no hay salidas para hoy');
			}else{
				$scope.suma_dia = data[0].suma_dia;
			}
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoSalidas()

// FUNCION PARA CONSULTAR SALIDAS DEL DIA Y EDITARLAOS ************************************************************

	$scope.ListadoSalidasEdit = function(){

		Control.ListSalidaEdi($routeParams.idsalidas_internas).success(function(data){
			
			$scope.salidas = data;

			$scope.idsalidas_internas = data[0].idsalidas_internas;
			$scope.nom_responsable = data[0].nom_responsable;
			$scope.valor = parseInt(data[0].valor);
			$scope.descripcion = data[0].descripcion;
			$scope.fecha_salida = data[0].fecha_salida;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoSalidasEdit();

// FUNCION PARA EDITAR SALIDA INTERNA ************************************************************

	$('.fa-pulse, #update, .fa-check').hide();

	$scope.EditSalida = function(idsalidas_internas){
		
		var valor = $scope.valor;

		if(valor <= 0 || valor == undefined){
  			alertify.error("<b>Info: </b>¡Campos invalidos!");
  		} else if(!/^([0-9])*$/.test(valor) || !/^([0-9])*$/.test(valor)){
				alertify.error("<b>¡Los valores deben ser un número!");
			}
  		else{
			$('.fa-pulse, #update').show();
			$('.fa-check').hide();

			datos = {
				idsalidas_internas:idsalidas_internas,
				valor:valor,
				nom_responsable:$scope.nom_responsable,
				descripcion:$scope.descripcion,
			}
				// console.log(datos);
				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Editar_SalidaInterna_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:datos
				}).success(function(respuesta){
					$scope.vales = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.fa-pulse, #update').hide();
							$('.fa-check').show();
							$('#cantidad, #total').val('');
							alertify.success("Información actualizada con éxito");
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
  		}
	}

// FUNCION PARA ELIMINAR UNA SALIDA INTERNA ************************************************************

	$scope.DeleteSalida = function(idsalidas_internas){

		// console.log(idsalidas_internas);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Delete_SalidaInterna_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idsalidas_internas:idsalidas_internas}
				}).success(function(respuesta){
					$scope.deletesalida = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ListadoSalidas();
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}
// End funcion $scope.DeleteSalida


// SALIDAS MENSUALES

	$scope.SalidasMensuales = function(mes){
		// console.log(mes);
		$scope.nomes = 0;
			if(mes == 0){
				// console.log('mes no existe')
				$scope.nomes = 1;
				// console.log('no existe: '+$scope.nomes)
			}else{
				// $scope.nomes = 2;
			Control.SalidasMonth(mes).success(function(data){
				$scope.meses = data;
				if($scope.meses == ''){
					// console.log('no hay vales para este mes')
				}else{
					$scope.suma_mes = data[0].suma_mes;
				}
				// console.log(data)
				
			}).error(function(error){
				console.log("Error."+error);
			});
		}
	}


// FUNCION PARA CONSULTAR SALIDAS DEL DIA DEL MES SELECCIONADO *********************************************************************************************

	$scope.today = new Date();

	$scope.ListadoSalidasMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
				$scope.nodata = 1;
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListSalidasMes(mes, year).success(function(data){

					$scope.salidas = data;

					if($scope.salidas == ''){
						// console.log('No hay SALIDAS MES en esta consulta');
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.total_salidas = data[0].total_salidas;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 5000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.ListadoSalidasMes();


// FUNCION PARA CONSULTAR SALIDAS DEL DIA DE LA FECHA SELECCIONADA *********************************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.ListadoSalidasDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListSalidasDate(fecha_con).success(function(data){
					
					$scope.salidas = data;

					if($scope.salidas == ''){
						// console.log('No hay SALIDAS en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.total_salidas = data[0].total_salidas;
						// console.log(data);
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 4000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ListadoSalidasDate();	



// FUNCION PARA CONSULTAR TODOS LAS LIQUIDACIONES REGISTRADAS ************************************************************
	
	$scope.ListadoLiquidaciones = function(){

		ListGenerales.ListLiquidaciones().success(function(data){
			$scope.liquidaciones = data;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoLiquidaciones();

// FUNCION PARA REGISTRAR UN PRESTAMO ************************************************************
	
	$("#fecha_pago").datepicker();

	$scope.RegistrarNewPrestamo = function(){

		var usuario = $('#usuario').val();
		var valorpermitido = $scope.valorpermitido;
		console.log(valorpermitido);
		valorpermitido = parseInt($scope.valorpermitido.replace(",", ""));
		var valorsolicitado = parseInt($scope.valor.replace(",", ""))
		// console.log('PERMITIDO: ' + valorpermitido + ' SOLICITADO: ' + valorsolicitado);

		if(valorsolicitado > valorpermitido){
			alertify.error("<b>Info: </b>¡El valor es mayor al permitido!");
		}else{

			var prestamo = {
				idempleados:$scope.idempleados,
				valor:$scope.valor,
				caja_salida:$scope.caja_salida,
				motivo:$scope.motivo,
				fecha_pago:$scope.fecha_pago,
				usuario:usuario
			}
			// console.log(prestamo)

			alertify.confirm("¿Deseas registrar la información?", function (info) {

				if(info){
					$http({
						method:'POST',
						url:'../web/includes/sql/Control/Registrar_Prestamo_sql.php',
						header:{
						'Content-Type': undefined
					},
						data:prestamo
					}).success(function(respuesta){
						$scope.prestamos = respuesta;
							// console.log(respuesta)
							alertify.success("Información registrada con éxito");
							window.location = "#/Prestamos";
					}).error(function(err){
							console.log("Error: "+err);
						});
				}else{
					alertify.error("<b>Info: </b>¡Proceso cancelado!");
				}
			});
		}

	}

	// 06-Ago-2018
	// CALCULAR PRESTAMOS TOTAL DE EMPLEADOS - PARA RESTRINGIR VALOR DE PRESTAMOS POR $100.000
	$scope.calcularprestamosind = function(idempleados){

		// console.log('ID Empleado: ' + idempleados);

		var prestamoaut = 50000;
		var prestamoautorizado = parseInt(prestamoaut);
		var prestamoperm = 0;
		var prestamopermitido = parseInt(prestamoperm);

		$scope.spinercalculo = true;
		$scope.infoprestamo = false;

		var prestamo = {
			idempleados:$scope.idempleados
		}

		Control.calcprestamosind(idempleados).success(function(data){

			$scope.caculoprestamo = data;
			$scope.spinercalculo = false;
			$scope.infoprestamo = true;

			$scope.total_prestamos = data[0].total_prestamos;
			$scope.abonos = data[0].abonos;
			$scope.saldo = data[0].saldo;
			$scope.valorpermitido = data[0].valorpermitido;

			if($scope.valorpermitido <= 0){
				$scope.valorpermitido = 0;
				$scope.btnRegistrarPrest = false;
				// console.log('No se puede prestar..');
			}else{
				// console.log('SI SE PUEDE');
				$scope.btnRegistrarPrest = true;
			}

			if($scope.total_prestamos == 0){
				$scope.valorpermitido = '50,000';
				$scope.btnRegistrarPrest = true;
				// console.log('No hay prestamos')
				// $('.infoprestamos').hide();
				// $('#noprestamos').text('Esta persona no tiene préstamos registrados');
				// $('#noprestamos').show();
			}else{
				$('.infoprestamos').show();
				$('#noprestamos').hide();
			}

			console.log(data);
			
		}).error(function(error){
			console.log("Error."+error);
		});

	}

// PERMITIR EL PRESTAMO - INPUT DE VALOR

// $scope.prestamopermitido = function(){
// 	console.log('Es: ' + $scope.valor + ' PERMITIDO: ' + $scope.valorpermitido);
// }

// LISTADO PAGOS QUINCENALES DIA

	$scope.ListadoPagosQuincenaDia = function(){

		Control.ListPagosQuincDia().success(function(data){

			$scope.pagosquincenales = data;

			if($scope.pagosquincenales == ''){
				// console.log('no hay pagos quincenales')
			}else{
				$scope.suma_pagos = data[0].suma_pagos;
			}
			// console.log(data);
			
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoPagosQuincenaDia();


// FUNCION PARA CONSULTAR LOS PAGOS QUINCENALES DEL MES SELECCIONADO *********************************************************************************************

	$scope.today = new Date();

	$scope.ListadoPagosQuincenaMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
				$scope.nodata = 1;
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListPagosQuincMes(mes, year).success(function(data){

					$scope.pagosquincenales = data;

					if($scope.pagosquincenales == ''){
						// console.log('No hay PAGOS MES en esta consulta');
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.suma_pagos = data[0].suma_pagos;
						// console.log(data);
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 4000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.ListadoPagosQuincenaMes();


// FUNCION PARA CONSULTAR LOS SERVIVIOS DEL DIA INGRESOS DE CUALQUIER FECHA SELECCIONADO *********************************************************************************************	

	$("#fecha_consulta").datepicker();

    $scope.ListadoPagosQuincenaDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListPagosQuincDate(fecha_con).success(function(data){
					
					$scope.pagosquincenales = data;

					if($scope.pagosquincenales == ''){
						console.log('No hay PAGOS FECHA en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.suma_pagos = data[0].suma_pagos;
						// console.log(data);
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 4000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ListadoPagosQuincenaDate();	


// REGISTRAR PRESTAMO QUINCENAL

	$scope.RegistrarNewPrestamoQuincena = function(){

		var usuario = $('#usuario').val();

		var prestamo = {
			idempleados:$scope.idempleados,
			valor:$scope.valor,
			caja_salida:$scope.caja_salida,
			motivo:$scope.motivo,
			usuario:usuario
		}
		// console.log(prestamo)

		alertify.confirm("¿Deseas registrar la información?", function (info) {

			if(info){
				$http({
					method:'POST',
					url:'../web/includes/sql/Control/PagosPrestamos/Registrar_Prestamo_Quincena_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:prestamo
				}).success(function(respuesta){
					$scope.prestamos = respuesta;
						// console.log(respuesta)
						alertify.success("Información registrada con éxito");
						window.location = "#/Control/OtrosReg/Pagos&Prestamos";
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

// FUNCION PARA VALIDAR PAGOS QUINCENARES ************************************************************
	
	$('.detalleEmpleado, .cargaNomb').hide();

	$scope.ConsultaSalario = function(idempleados, nom_empleado){
		
		$('.cargaNomb').show();
		$('.detalleEmpleado, .tablaPrestamosQuin, .infoPagado, .btnPago').hide();
		$('.error').addClass('shake');
		$('.warning').addClass('fadeIn');
		$('#mes, #periodo').val('');

		var select = $('#periodo');
		select.val($('option:first', select).val());

			setTimeout(function(){
				$('.cargaNomb').hide();
				$('.detalleEmpleado').show();
			}, 1000);

		Control.ConsultaSal(idempleados).success(function(data){

			$scope.salario = data[0].salario;
			$scope.nom_empleado = data[0].nom_empleado;
			$scope.ape_empleado = data[0].ape_empleado;

			if($scope.salario == null){
				// console.log('Empleado no tiene salario');
				setTimeout(function(){
					alertify.error("<b>Info: </b>¡No tiene salario asignado!");
				}, 1000);
				$('.btnGuardar').hide()
			}else{
				$('.btnGuardar').show()
				$scope.salario = data[0].salario/2;
				// console.log(data)
				// $scope.ListadoMesesSelect();
				// $scope.ListadoPeriodoPagoSelect();
			}

		}).error(function(error){
			console.log("Error."+error);
		});
	}

	$scope.VaciarCamposPagos = function(idempleados){
		$('#periodo').val('');
		$('.tablaPrestamosQuin, .btnPago, .infoPagado').hide();
	}


// Validar pagos de empleados - Verifica si existe el pago - Trae el pago. ***************
	
	$('.tablaPrestamosQuin, .cargaQuincena, .infoPagado').hide();

	$scope.ValidarPagosQuincena = function(){
		
		$('.tablaPrestamosQuin').hide();

		if($scope.mes == 0){
			alertify.error("<b>Info: </b>¡Debe seleccionar un mes!");
			
		}else if($scope.idempleados == undefined){
			alertify.error("<b>Info: </b>¡Debe seleccionar un estilista!");				
		 }
		
		else{

			$('.cargaQuincena').show();
			
			var idempleado = $scope.idempleados
			var mes = $scope.mes
			var periodo = $scope.periodo
			var salario = $scope.salario

			// console.log('salario es: ' + salario)	

			Control.ListValidaPagos(idempleado, mes, periodo).success(function(data){

				$scope.prestamosquincenas = data;

				if($scope.prestamosquincenas == ''){
					// console.log('No hay prestamos registrados');
					setTimeout(function(){
							$('.cargaQuincena').hide();
							$('.tablaPrestamosQuin, .btnPago').hide();
							alertify.error("<b>Info: </b>¡No hay prestamos registrados!");
					}, 1000);
				}else{

					$scope.SumaPrestamos = data[0].SumaPrestamos;
					$scope.estado = data[0].estado;
					$scope.totalPago = salario - $scope.SumaPrestamos;
					// console.log('total es: ' + totalPago);
					// console.log('ESTADO DEL PRESTAMO: ' + $scope.estado);

					if($scope.estado == 0){
						$('.infoPagado').hide();
						$('.btnGuardar').show();
					}else{
						$('.btnGuardar').hide();
						$('.infoPagado').show();
					}

					setTimeout(function(){
						$('.cargaQuincena').hide();
						$('.tablaPrestamosQuin, .btnPago').show();
						$('.tablaPrestamosQuin').addClass('fadeIn');
						alertify.success("Información encontrada");
					}, 2000);

					// console.log(data);
				}

			}).error(function(error){
				console.log("Error."+error);
			});
		}
	}


// CONSULTAR PAGO INDIVIDUAL MODAL

	$scope.ValidarPagosQuincenaInd = function(idempleado, mes, periodo , year, nom_mes, periodo){
	
		// console.log('EMPLEADO es: ' + idempleado + ' mes: ' + mes + ' PERIODO: ' + periodo + ' AÑO: ' + year);

		Control.ListValidaPagosInv(idempleado, mes, periodo, year).success(function(data){

			$scope.prestamosquincenasind = data;

			if($scope.prestamosquincenasind == ''){
				// console.log('No hay prestamos registrados');
			}else{

				$scope.nom_mes = nom_mes;
				$scope.periodo = periodo;
				// console.log('mmmm ' + $scope.nom_mes);
				$scope.nom_empleado = data[0].nom_empleado;
				$scope.ape_empleado = data[0].ape_empleado;
				$scope.salariotot = parseInt(data[0].salario);
				$scope.salarioind = $scope.salariotot / 2;

				$scope.SumaPrestamosind = data[0].SumaPrestamos;
				$scope.estadoind = data[0].estado;	
				$scope.totalPagoind = $scope.salarioind - $scope.SumaPrestamosind;

				// console.log(data);
			}

		}).error(function(error){
			console.log("Error."+error);
		});

	}	

// REGISTRAR PRESTAMO QUINCENAL

	$scope.RegistrarNewPagoQuincena = function(){

		var usuario = $('#usuario').val();
		var nom_empleado = $('#empleado').val();

		var pagoquincena = {
			valor:$scope.totalPago,
			mes:$scope.mes,
			periodo:$scope.periodo,
			idempleados:$scope.idempleados,
			usuario:usuario,
			nom_empleado:nom_empleado
		}
		// console.log(pagoquincena)

		alertify.confirm("¿Deseas registrar la información?", function (info) {

			if(info){
				
				alert('¡Espere un momento! <br>Se está guardando la información. <br><i class=" mt10 fa fa-spinner fa-spin fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Control/PagosPrestamos/Registrar_Pago_Quincena_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:pagoquincena
					}).success(function(respuesta){

						$scope.pagosquiena = respuesta;
						// console.log(respuesta);

						setTimeout(function(){
							window.location = "#/Control/OtrosReg/ListPagosQuincenales";
							this._alertOverlay.remove();
        					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información registrada con éxito");
						 }, 2000);
					}).error(function(err){
						console.log("Error: "+err);
				});	
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}	

// FUNCION PARA CONSULTAR MESES - SELECT ************************************************************
	
	$scope.ListadoMesesSelect = function(){

		// console.log('Se ejecuto LISTADO MES')

		ListGenerales.ListMeses().success(function(data){
			$scope.meses = data;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoMesesSelect();

// FUNCION PARA CONSULTAR PERIODO DE MES - SELECT ************************************************************
	
	$scope.ListadoPeriodoPagoSelect = function(){

		// console.log('Se ejecuto LISTADO DE PERIODOSSS')

		ListGenerales.ListPeriodo().success(function(data){
			$scope.periodos = data;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoPeriodoPagoSelect();

// FUNCION PARA CONSULTAR PRESTAMOS DE DIA ACTUAL ************************************************************
	
	$scope.ListadoPrestamos = function(){

		Control.ListPrestamos().success(function(data){
			$scope.prestamos = data;
			$scope.suma_prestamos = data;
			// console.log(data)
				if($scope.suma_prestamos == ''){
					// console.log('no hay prestamos para hoy')
				}else{
					$scope.suma_prestamos = data[0].suma_prestamos;
				}

		}).error(function(error){
			console.log("Error."+error);
		});
	}

// FUNCION PARA CONSULTAR LOS PRESTAMOS QUINCENALES DEL MES *********************************************************************************************

	$scope.today = new Date();

	$scope.ListadoPrestamosMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
				$scope.nodata = 1;
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListPrestamosMes(mes, year).success(function(data){

					$scope.prestamos = data;
					$scope.suma_prestamos = data;

					if($scope.suma_prestamos == ''){
						// console.log('No hay PRESTAMOS MES en esta consulta');
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.suma_prestamos = data[0].suma_prestamos;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 3000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}
	// $scope.ListadoPrestamosMes();

// FUNCION PARA CONSULTAR LOS PRESTAMOS QUINCENALES POR FECHA *********************************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.ListadoPrestamosDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				$scope.nodata = 1;
			}else{
				console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListPrestamosDate(fecha_con).success(function(data){
					
					$scope.prestamos = data;
					$scope.suma_prestamos = data;

					if($scope.suma_prestamos == ''){
						// console.log('No hay PRESTAMOS QUINCENA MES en esta consulta');
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.suma_prestamos = data[0].suma_prestamos;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 3000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ListadoPrestamosDate();	




// LISTADO PRESTAMOS QUINCENALES DIA

	$scope.ListadoPrestamosQuincenaDia = function(){

		Control.ListPrestamosQuincDia().success(function(data){

			$scope.prestamos = data;
			$scope.suma_prestamos = data;
			// console.log(data)
			
			if($scope.suma_prestamos == ''){
				// console.log('no hay prestamos para hoy')
			}else{
				$scope.suma_prestamos = data[0].suma_prestamos;
			}

		}).error(function(error){
			console.log("Error."+error);
		});
	}

// FUNCION PARA CONSULTAR LOS PRESTAMOS QUINCENALES DEL MES *********************************************************************************************

	$scope.today = new Date();

	$scope.ListadoPrestamosQuincenaMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
				$scope.nodata = 1;
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListPrestamosQuincMes(mes, year).success(function(data){

					$scope.prestamos = data;
					$scope.suma_prestamos = data;

					if($scope.suma_prestamos == ''){
						// console.log('No hay PRESTAMOS QUINCENA MES en esta consulta');
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.suma_prestamos = data[0].suma_prestamos;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 3000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}
	// $scope.ListadoPrestamosQuincenaMes();

// FUNCION PARA CONSULTAR LOS PRESTAMOS QUINCENALES POR FECHA *********************************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.ListadoPrestamosQuincenaDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				$scope.nodata = 1;
			}else{
				console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListPrestamosQuincDate(fecha_con).success(function(data){
					
					$scope.prestamos = data;
					$scope.suma_prestamos = data;

					if($scope.suma_prestamos == ''){
						console.log('No hay PRESTAMOS QUINCENA MES en esta consulta');
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.suma_prestamos = data[0].suma_prestamos;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 3000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ListadoPrestamosQuincenaDate();


	$scope.ListadoDeudores = function(){

		Control.ListDeudores().success(function(data){
			
			$scope.deudores = data;
			// console.log(data);

			if($scope.suma_prestamos_deudores == ''){
				// console.log('no hay prestamos para hoy')
			}else{
				$scope.suma_prestamos_deudores = data[0].suma_prestamos_deudores;
			}

		}).error(function(error){
			console.log("Error."+error);
		});

	}

	// ---- ↓↓ Cerrar Modal en esta Página ↓↓ ----
	$scope.closeModal = function(){
		$('.modal').hide();
		$('.modal-backdrop').hide();
	}

	// ---- ↓↓ Cerrar Modal en esta Página ↓↓ ----
	$scope.openModal = function(){
		$('.modal').show();
		$('.modal-backdrop').show();
	}

// FUNCION PARA CONSULTAR PRESTAMOS POR MES ************************************************************
	
	$scope.year = '2018';
	$scope.mes = '0';

	$scope.PrestamosMensuales = function(){

		$scope.nomes = 0;

		var prestamomes = {
			mes:$scope.mes,
			year:$scope.year
		}
			if($scope.mes == 0){
				// console.log('mes no existe')
				$scope.nomes = 1;
			}else{
				Control.ListPrestamosMonth(prestamomes).success(function(data){
					
					$scope.prestamosmes = data;
					$scope.suma_prestamosmes = data;
					// console.log(data)
						if($scope.prestamosmes == ''){
							// console.log('no hay prestamos para este mes')
						}else{
							$scope.suma_prestamosmes = data[0].suma_prestamosmes;
						}

				}).error(function(error){
					console.log("Error."+error);
				});
		}
	}

// FUNCION PARA CONSULTAR PRESTAMO INDIVIDUAL ************************************************************
	
	$scope.ListadoPrestamoInd = function(idprestamos){

		Control.ListPrestamosInd(idprestamos).success(function(data){

			$scope.abonos = data;
			// console.log(data)
			$scope.idprestamos = data[0].idprestamos;
			$scope.nom_empleado = data[0].nom_empleado;
			$scope.ape_empleado = data[0].ape_empleado;
			$scope.valor = data[0].valor;
			$scope.motivo = data[0].motivo;
			$scope.fecha_prestamo = data[0].fecha_prestamo;
			$scope.fecha_pago = data[0].fecha_pago;
			$scope.caja_salida = data[0].caja_salida;
			$scope.idcaja_salida = data[0].caja_salida;
			$scope.abonos = data[0].abonos;
			$scope.saldo = data[0].saldo;
			$scope.estado_prestamo = data[0].estado_prestamo;

			if($scope.caja_salida == 1){
				$scope.caja_salida = 'Caja Principal'
			}else{
				$scope.caja_salida = 'Caja Interna'
			}

		}).error(function(error){
			console.log("Error."+error);
		});
	}

// FUNCION PARA CONSULTAR PRESTAMO INDIVIDUAL PARA PAGAR (FECHA DE PAGO) ************************************************************
	
	$scope.ListadoPrestamoIndPago = function(){
		Control.ListPrestamosIndPago($routeParams.idempleados).success(function(data){
			
			$scope.fechapagos = data;
			// console.log(data)

			if($scope.fechapagos == ''){
				// console.log('no tiene prestamos para pagara hoy')
			}else{
				nom_empleado = data[0].nom_empleado
				// console.log('si tiene prestamos')
				alert('¡'+nom_empleado+'!<br> Tiene prestamos que debe cancelara hoy')
			}

		}).error(function(error){
			console.log("Error."+error);
		});
	}

// FUNCION PARA CONSULTAR PRSETAMOS Y EDITARLAOS ************************************************************

	$scope.ListadoPrestamosEdit = function(idprestamos){

		Control.ListPrestamosEdi(idprestamos).success(function(data){
			
			$scope.prestamos_det = data;

			$scope.idprestamos = data[0].idprestamos;
			$scope.nom_empleado = data[0].nom_empleado;
			$scope.ape_empleado = data[0].ape_empleado;
			$scope.valor = parseInt(data[0].valor);
			$scope.motivo = data[0].motivo;
			$scope.caja_salida = data[0].caja_salida;
			$scope.fecha_prestamo = data[0].fecha_prestamo;
			$scope.fecha_pago = data[0].fecha_pago;
			$scope.estado_prestamo = data[0].estado_prestamo;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoPrestamosEdit();

// FUNCION PARA EDITAR UN PRESTAMO ************************************************************

	$('.fa-pulse, #update, .fa-check').hide();

	$scope.EditPrestamo = function(idprestamos){

		var valor = $scope.valor;

		if(valor <= 0 || valor == undefined){
  			alertify.error("<b>Info: </b>¡Campos invalidos!");
  		} else if(!/^([0-9])*$/.test(valor) || !/^([0-9])*$/.test(valor)){
				alertify.error("<b>¡Los valores deben ser un número!");
		}else{

			$('.fa-pulse, #update').show();
			$('.fa-check').hide();

			datos = {
				idprestamos:idprestamos,
				valor:valor,
				motivo:$scope.motivo,
				caja_salida:$scope.caja_salida,
				fecha_pago:$scope.fecha_pago,
			}
				// console.log(datos);
				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Editar_Prestamo_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:datos
				}).success(function(respuesta){
					$scope.vales = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$scope.ListadoPrestamos();
							$('.fa-pulse, #update').hide();
							$('.fa-check').show();
							$('#cantidad, #total').val('');
							alertify.success("Información actualizada con éxito");
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
  		}
	}


// FUNCION PARA ELIMINAR UN PRESTAMO ************************************************************

	$scope.DeletePrestamo = function(idprestamos){

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Delete_Prestamo_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idprestamos:idprestamos}
				}).success(function(respuesta){
					$scope.deleteentrada = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ListadoPrestamos();
							window.location = "#/Prestamos";
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

	// End funcion $scope.DeletePrestamo

// FUNCION PARA ELIMINAR UN PRESTAMO QUINCENAL ************************************************************

	$scope.DeletePrestamoQuincena = function(idprestamos){

		// console.log(idprestamos);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Control/PagosPrestamos/Delete_Prestamo_Quincena_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idprestamos:idprestamos}
				}).success(function(respuesta){
					$scope.deleteprestamo = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ListadoPrestamosQuincenaDia();
							window.location = "#/Control/OtrosReg/Pagos&Prestamos";
						 }, 2000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

	// End funcion $scope.DeletePrestamoQuincena

// FUNCION PARA ELIMINAR UN PAGO QUINCENAL ************************************************************

	$scope.DeletePagoQuincena = function(idpagos, idempleados, fecha, hora, valor, mes, periodo, year){

		// console.log(idprestamos);

		var deletepago = {
			idpagos:idpagos,
			idempleados:idempleados,
			fecha:fecha,
			hora:hora,
			valor:valor,
			mes:mes,
			periodo:periodo,
			year:year
		}
		// console.log(deletepago);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Control/PagosPrestamos/Delete_Pago_Quincena_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:deletepago
				}).success(function(respuesta){
					$scope.deletepagos = respuesta;
						// console.log(respuesta);
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ListadoPagosQuincenaDia();
							window.location = "#/Control/OtrosReg/ListPagosQuincenales";
						 }, 2000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

	// End funcion $scope.DeletePagoQuincena


// FUNCION PARA REGISTRAR ABONO A UN PRESTAMO ************************************************************

	$scope.AbonarPrestamo = function(idprestamos, saldo, idcaja_salida){
		// console.log(saldo)

		var usuario = $('#usuario').val();

		alertify.prompt("Digite el valor que desea abonar", function (info1, valor) {
			var abono = parseInt(valor);
			if(info1){
				// console.log(abono)
				if(abono == '' || abono == undefined || abono == NaN){
					alertify.error("<b>¡El valor está vacio!");
					
				}else if(!/^([0-9])*$/.test(abono)){
						alertify.error("<b>¡El valor debe ser un número!");
					}
				 else if(abono > saldo){
						alertify.error("<b>¡El valor a abonar es mayor al saldo!");
				 	}
				else{
					alertify.confirm("¿Esta seguro abonar "+abono+ "?", function (info2) {
						if(info2){

							$http({
								method:'POST',
								url:'../web/includes/sql/Control/Registrar_Abono_sql.php',
								header:{
								'Content-Type': undefined
							},
								data:{idprestamos:idprestamos, abono:abono, idcaja_salida:idcaja_salida, usuario:usuario}
							}).success(function(respuesta){
									$scope.ListadoAbonos(idprestamos);
									$scope.ListadoPrestamoInd(idprestamos);
									$scope.ListadoPrestamos();
									$scope.prestamos_abono = respuesta;
									// console.log(respuesta)
									alertify.success("Información registrada con éxito");
							}).error(function(err){
									console.log("Error: "+err);
								});
						}else{
							alertify.error("<b>Info: </b>¡Proceso cancelado!");
						}
					});
				}
			}else{
					alertify.error("<b>Info: </b>¡Abono cancelado!");
				}
		});
	}

// FUNCION PARA CONSULTAR LOS ABONOS REALIZADOS // PANTALLA INDIVIDUAL DEL PRESTAMO ************************************************************
	
	$scope.ListadoAbonos = function(idprestamos){

		Control.ListAbonos(idprestamos).success(function(data){
			$scope.listadoabonos = data;
			if($scope.listadoabonos == ''){
				// console.log('No hay abonos')
				$scope.suma_abonos = '$ 0';
				$('#abonos').text('$ 0');
				$('#suma_abonos').text('$ 0');
			}else{
				$scope.suma_abonos = data[0].suma_abonos;
			}
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListadoAbonos();

// FUNCION PARA CONSULTAR ABONOS A PRESTAMOS DE DIA PANTALLA DE BONOS ************************************************************
	
	$scope.ListadoAbonosDia = function(){

		Control.ListAbonosDia().success(function(data){
			$scope.abonos = data;
			// console.log(data)
				if($scope.abonos == ''){
					// console.log('no hay abono para hoy')
				}else{
					$scope.TotalAbonos = data[0].TotalAbonos;
				}

		}).error(function(error){
			console.log("Error."+error);
		});
	}

// FUNCION PARA CONSULTAR LOS ABONOS DEL MES SELECCIONADO *********************************************************************************************

	$scope.today = new Date();

	$scope.ListadoAbonosMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
				$scope.nodata = 1;
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListAbonosMes(mes, year).success(function(data){

					$scope.abonos = data;

					if($scope.abonos == ''){
						// console.log('No hay PRESTAMOS QUINCENA MES en esta consulta');
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.TotalAbonos = data[0].TotalAbonos;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 3000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}
	// $scope.ListadoAbonosMes();


// BUSQUEDA DE ABONOS A PRSTAMOS POR FECHA PANTALLA DE CONTROL DE PRESTAMOS ***********************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.ListadoAbonosDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListAbonosDate(fecha_con).success(function(data){
					
					$scope.abonos = data;

					if($scope.abonos == ''){
						console.log('No hay abonos en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.TotalAbonos = data[0].TotalAbonos;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 3000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ListadoAbonosDate();


// END BUSQUEDA POR FECHA ***********************************************************************************
// ***************************************************************************************************************

// FUNCION PARA ELIMINAR UN PRESTAMO QUINCENAL ************************************************************

	$scope.DeleteAbono = function(idabono, valor, idprestamo, idpos){

		// console.log('ID ABONO: ' + idabono + ' VALOR: ' + valor + ' IDPRESTAMO: ' + idprestamo);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Delete_Abono_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idabono:idabono, valor:valor, idprestamo:idprestamo}
				}).success(function(respuesta){
					$scope.deleteabono = respuesta;
						// console.log(respuesta);
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							if(idpos == 1){
								window.location = "#/ListAbonos";
								$scope.ListadoAbonosDia();
							}else{
								window.location = "#/ListAbonos";
							}
						 }, 2000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

	// End funcion $scope.DeleteAbono

// FUNCION PARA CONSULTAR ENTRADAS CAJA INTERNA ************************************************************
	
	$scope.ListadoEntradaCajaInterna = function(){
		Control.ListEntradaCaja().success(function(data){
			$scope.entradascaja = data;
			if($scope.entradascaja == ''){
				// console.log('No hay entradas a la Caja Interna');
			}else{
				$scope.valor_entrada = data[0].valor_entrada;
				$scope.total_entradas = data[0].total_entradas;
			}
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListadoEntradaCajaInterna();

// FUNCION PARA CONSULTAR SALIDAS CAJA INTERNA ************************************************************
	
	$scope.ListadoSalidaCajaInterna = function(){

		Control.ListSalidaCaja().success(function(data){

			$scope.salidascaja = data;
			if($scope.salidascaja == ''){
				// console.log('No hay Salidas a la Caja Interna');
			}else{
				$scope.valor_salida = data[0].valor_salida;
				$scope.total_salidas = data[0].total_salidas;
			}
			// console.log($scope.valor_salida)
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListadoSalidaCajaInterna();

// FUNCION PARA REGISTRAR UNA ENTRADA A LA CAJA INTERNA ************************************************************

	$scope.RegistrarNewEntradaCaja = function(){

		var usuario = $('#usuario').val();

		var entradacajaint = {
			valor:$scope.valor,
			descripcion:$scope.descripcion,
			usuario:usuario
		}

		alertify.confirm("¿Deseas registrar la información?", function (info) {

			if(info){
				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Registrar_EntradaCaja_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:entradacajaint
				}).success(function(respuesta){
					$scope.entradacaja = respuesta;
						// console.log(respuesta)
						alertify.success("Información registrada con éxito");
						window.location = "#/CajaInterna";
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

// FUNCION PARA ELIMINAR UNA ENTRADA CAJA INTERNA ************************************************************

	$scope.DeleteEntradaCaja = function(idcaja_interna_entradas){

		// console.log(idcaja_interna_entradas);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Delete_EntradaCajaInt_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idcaja_interna_entradas:idcaja_interna_entradas}
				}).success(function(respuesta){
					$scope.DeleteEntradaCaja = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ListadoEntradaCajaInterna();
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}
// End funcion $scope.DeleteEntradaCaja

// FUNCION PARA REGISTRAR UNA SALIDA DE LA CAJA INTERNA ************************************************************

	$scope.RegistrarNewSalidaCaja = function(){

		var usuario = $('#usuario').val();

		var salidacajaint = {
			valor:$scope.valor,
			descripcion:$scope.descripcion,
			usuario:usuario
		}

		alertify.confirm("¿Deseas registrar la información?", function (info) {

			if(info){
				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Registrar_SalidaCaja_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:salidacajaint
				}).success(function(respuesta){
					$scope.salidacaja = respuesta;
						// console.log(respuesta)
						alertify.success("Información registrada con éxito");
						window.location = "#/CajaInterna";
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

// FUNCION PARA ELIMINAR UNA SALIDA DE CAJA INTERNA ************************************************************

	$scope.DeleteSalidaCaja = function(idcaja_interna_salidas){

		// console.log(idcaja_interna_salidas);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Delete_SalidaCajaInt_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idcaja_interna_salidas:idcaja_interna_salidas}
				}).success(function(respuesta){
					$scope.DeleteSalidaCaja = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ListadoSalidaCajaInterna();
						 }, 3000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}
// End funcion $scope.DeleteSalidaCaja

// *********************************************************************************************************************************************************************
// *********************************************************************************************************************************************************************
// INGRESOS DEL DIA ****************************************************************************************************************************************************
// *********************************************************************************************************************************************************************
// *********************************************************************************************************************************************************************

// ENTRADAS *********************











// FUNCION PARA CONSULTAR LOS INGRESOS DEL DIA *********************************************************************************************
	
	$scope.IngresosDiarios = function(){

		Control.ListIngresosDiarios().success(function(data){
			$scope.ingresosdiarios = data;
			// console.log(data)
			if($scope.ingresosdiarios == ''){
				// console.log('no hay servicios para hoy');
			}else{
				$scope.cant_servicios = data[0].cant_servicios;
				$scope.suma_servicios = parseInt(data[0].suma_servicios);
				$scope.suma_descuento_productos = data[0].suma_descuento_productos;
				$scope.suma_valor_final_servicios = data[0].suma_valor_final_servicios;
				$scope.suma_valor_salon = data[0].suma_valor_salon;
				$scope.suma_valor_estilista = data[0].suma_valor_estilista;
				$scope.suma_valor_salon_mas_desc = parseInt(data[0].suma_valor_salon_mas_desc);
				$scope.pago_total_efectivo = data[0].pago_total_efectivo;
				$scope.pago_total_tarjeta = parseInt(data[0].pago_total_tarjeta);
				$scope.total_salon_efectivo = parseInt(data[0].total_salon_efectivo);
				$scope.total_salon_tarjeta = parseInt(data[0].total_salon_tarjeta);
			}
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.IngresosDiarios();

// FUNCION PARA CONSULTAR LOS INGRESOS DEL MES ************************************************************

	$scope.today = new Date();

	$scope.IngresosMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListIngresosMes(mes, year).success(function(data){

					$scope.ingresosdiarios = data;

					if($scope.ingresosdiarios == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{
						$scope.cant_servicios = data[0].cant_servicios;
						$scope.suma_servicios = parseInt(data[0].suma_servicios);
						$scope.suma_descuento_productos = data[0].suma_descuento_productos;
						$scope.suma_valor_final_servicios = data[0].suma_valor_final_servicios;
						$scope.suma_valor_salon = data[0].suma_valor_salon;
						$scope.suma_valor_estilista = data[0].suma_valor_estilista;
						$scope.suma_valor_salon_mas_desc = parseInt(data[0].suma_valor_salon_mas_desc);
						$scope.pago_total_efectivo = data[0].pago_total_efectivo;
						$scope.pago_total_tarjeta = parseInt(data[0].pago_total_tarjeta);
						$scope.total_salon_efectivo = parseInt(data[0].total_salon_efectivo);
						$scope.total_salon_tarjeta = parseInt(data[0].total_salon_tarjeta);
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.IngresosMes();

// FUNCION PARA CONSULTAR LOS INGRESOS POR CULQUIER FECHA ***********************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.IngresosDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListIngresosDate(fecha_con).success(function(data){
					
					$scope.ingresosdiarios = data;

					if($scope.ingresosdiarios == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{
						$scope.cant_servicios = data[0].cant_servicios;
						$scope.suma_servicios = parseInt(data[0].suma_servicios);
						$scope.suma_descuento_productos = data[0].suma_descuento_productos;
						$scope.suma_valor_final_servicios = data[0].suma_valor_final_servicios;
						$scope.suma_valor_salon = data[0].suma_valor_salon;
						$scope.suma_valor_estilista = data[0].suma_valor_estilista;
						$scope.suma_valor_salon_mas_desc = parseInt(data[0].suma_valor_salon_mas_desc);
						$scope.pago_total_efectivo = data[0].pago_total_efectivo;
						$scope.pago_total_tarjeta = parseInt(data[0].pago_total_tarjeta);
						$scope.total_salon_efectivo = parseInt(data[0].total_salon_efectivo);
						$scope.total_salon_tarjeta = parseInt(data[0].total_salon_tarjeta);
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.IngresosDate();

// ***************************************************************************************************************
// ***************************************************************************************************************

// FUNCION PARA CONSULTAR CANTIDAD DE TIPO DE SERVICIOS EN LOS INGRESOS DEL DIA ************************************************************
	
	$scope.TipoServIngresosDia = function(){

		Control.ListTipoServDia().success(function(data){

			$scope.tiposervicios = data;
			if($scope.tiposervicios == ''){
				// console.log('no hay servicios para hoy');
			}else{
				$scope.cant_tiposervicio = data[0].cant_tiposervicio;
			}
			// console.log(data);
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.TipoServIngresosDia();


// FUNCION PARA CONSULTAR TIPO SERVICIOS REALIZADOS POR MES ************************************************************

	$scope.today = new Date();

	$scope.TipoServIngresosMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
				$scope.nodata = 1;
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListTipoServMes(mes, year).success(function(data){

					$scope.tiposervicios = data;

					if($scope.tiposervicios == ''){
						// console.log('No hay ingresos en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.cant_tiposervicio = data[0].cant_tiposervicio;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 5000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.TipoServIngresosMes();

// ***************************************************************************************************************
// ***************************************************************************************************************

// FUNCION PARA CONSULTAR TIPO SERVICIOS REALIZADOS POR CULQUIER FECHA ***********************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.TipoServIngresosDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListTipoServDate(fecha_con).success(function(data){
					
					$scope.tiposervicios = data;

					if($scope.tiposervicios == ''){
						// console.log('No hay ingresos en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.cant_tiposervicio = data[0].cant_tiposervicio;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 4000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.TipoServIngresosDate();


// FUNCION PARA CONSULTAR LOS PRODUCTOS VENDIDOS DEL DIA PARA INGRESOS DEL DIA ************************************************************
	
	$scope.VentaProductosVitrinaDia = function(){

		Control.ListVentaProdVitrina().success(function(data){
			$scope.ventaproductos = data;
			if($scope.ventaproductos == ''){
				// console.log('No hay ventas de productos para hoy')
			}else{
				$scope.cant_vendida_prod = data[0].cant_vendida_prod;
				$scope.cantidad = data[0].cantidad;
				$scope.valor_total_ventas = data[0].valor_total_ventas;
				$scope.valor_salon_ventas = parseInt(data[0].valor_salon_ventas);
				$scope.valor_estilista_ventas = parseInt(data[0].valor_estilista_ventas);
				$scope.total_salon_ventas_efectivo = parseInt(data[0].total_salon_ventas_efectivo);
				$scope.total_salon_ventas_tarjeta = parseInt(data[0].total_salon_ventas_tarjeta);
				$scope.valor_salon_ventas_efectivo = parseInt(data[0].valor_salon_ventas_efectivo);
				$scope.valor_salon_ventas_tarjeta = parseInt(data[0].valor_salon_ventas_tarjeta);
			}
			// console.log(data)
			
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.VentaProductosVitrinaDia();

// FUNCION PARA CONSULTAR LOS PRODUCTOS VENDIDOS DEL MES PARA INGRESOS DEL DIA ************************************************************

	$scope.today = new Date();

	$scope.VentaProductosVitrinaMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListVentaProdVitrinaMes(mes, year).success(function(data){

					$scope.ventaproductos = data;

					if($scope.ventaproductos == ''){
						// console.log('No hay ingresos en esta consulta')

					}else{
						$scope.cant_vendida_prod = data[0].cant_vendida_prod;
						$scope.cantidad = data[0].cantidad;
						$scope.valor_total_ventas = data[0].valor_total_ventas;
						$scope.valor_salon_ventas = parseInt(data[0].valor_salon_ventas);
						$scope.valor_estilista_ventas = parseInt(data[0].valor_estilista_ventas);
						$scope.total_salon_ventas_efectivo = parseInt(data[0].total_salon_ventas_efectivo);
						$scope.total_salon_ventas_tarjeta = parseInt(data[0].total_salon_ventas_tarjeta);
						$scope.valor_salon_ventas_efectivo = parseInt(data[0].valor_salon_ventas_efectivo);
						$scope.valor_salon_ventas_tarjeta = parseInt(data[0].valor_salon_ventas_tarjeta);
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.VentaProductosVitrinaMes();

// FUNCION PARA CONSULTAR LAS VENTAS POR CULQUIER FECHA ***********************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.VentaProductosVitrinaDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListVentaProdVitrinaDate(fecha_con).success(function(data){
					
					$scope.ventaproductos = data;

					if($scope.ventaproductos == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{
						$scope.cant_vendida_prod = data[0].cant_vendida_prod;
						$scope.cantidad = data[0].cantidad;
						$scope.valor_total_ventas = data[0].valor_total_ventas;
						$scope.valor_salon_ventas = parseInt(data[0].valor_salon_ventas);
						$scope.valor_estilista_ventas = parseInt(data[0].valor_estilista_ventas);
						$scope.total_salon_ventas_efectivo = parseInt(data[0].total_salon_ventas_efectivo);
						$scope.total_salon_ventas_tarjeta = parseInt(data[0].total_salon_ventas_tarjeta);
						$scope.valor_salon_ventas_efectivo = parseInt(data[0].valor_salon_ventas_efectivo);
						$scope.valor_salon_ventas_tarjeta = parseInt(data[0].valor_salon_ventas_tarjeta);
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.VentaProductosVitrinaDate();

// ***************************************************************************************************************
// ***************************************************************************************************************

// FUNCION PARA CONSULTAR LOS PRODUCTOS VENDIDOS DEL DIA PARA INGRESOS DEL DIA ************************************************************
	
	$scope.VentaProductosServicioDia = function(){

		Control.ListVentaProdServ().success(function(data){

			$scope.ventaproductosServ = data;

			if($scope.ventaproductosServ == ''){
				// console.log('No hay ventas de productos SERVICIO para hoy')
			}else{

				$scope.cant_vendida_prod = data[0].cant_vendida_prod;
				$scope.cantidad = data[0].cantidad;

				$scope.total_pservicios_efectivo = parseInt(data[0].total_pservicios_efectivo);
				$scope.total_pservicios_tarjeta = parseInt(data[0].total_pservicios_tarjeta);
				$scope.total_pservicios = parseInt(data[0].total_pservicios);

				$scope.salon_pservicios = parseInt(data[0].salon_pservicios);
				$scope.estilista_pservicios = parseInt(data[0].estilista_pservicios);

				$scope.totalingresosServicios = $scope.salon_pservicios + $scope.estilista_pservicios;
				
				$scope.salon_pservicios_efectivo = parseInt(data[0].salon_pservicios_efectivo);
				$scope.salon_pservicios_tarjeta = parseInt(data[0].salon_pservicios_tarjeta);
			}

			// console.log($scope.totalingresosServicios)

		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.VentaProductosServicioDia();

// FUNCION PARA CONSULTAR LOS VENTAS LAVACABEZAS DEL MES ************************************************************

	$scope.today = new Date();

	$scope.VentaProductosServicioMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListVentaProdServMes(mes, year).success(function(data){

					$scope.ventaproductosServ = data;

					if($scope.ventaproductosServ == ''){
						// console.log('No hay ingresos en esta consulta')

					}else{
						$scope.cant_vendida_prod = data[0].cant_vendida_prod;
						$scope.cantidad = data[0].cantidad;

						$scope.total_pservicios_efectivo = parseInt(data[0].total_pservicios_efectivo);
						$scope.total_pservicios_tarjeta = parseInt(data[0].total_pservicios_tarjeta);
						$scope.total_pservicios = parseInt(data[0].total_pservicios);

						$scope.salon_pservicios = parseInt(data[0].salon_pservicios);
						$scope.estilista_pservicios = parseInt(data[0].estilista_pservicios);

						$scope.totalingresosServicios = $scope.salon_pservicios + $scope.estilista_pservicios;
						
						$scope.salon_pservicios_efectivo = parseInt(data[0].salon_pservicios_efectivo);
						$scope.salon_pservicios_tarjeta = parseInt(data[0].salon_pservicios_tarjeta);
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.VentaProductosServicioMes();

// FUNCION PARA CONSULTAR LOS INGRESOS POR CULQUIER FECHA ***********************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.VentaProductosServicioDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListVentaProdServDate(fecha_con).success(function(data){
					
					$scope.ventaproductosServ = data;

					if($scope.ventaproductosServ == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{
						$scope.cant_vendida_prod = data[0].cant_vendida_prod;
						$scope.cantidad = data[0].cantidad;

						$scope.total_pservicios_efectivo = parseInt(data[0].total_pservicios_efectivo);
						$scope.total_pservicios_tarjeta = parseInt(data[0].total_pservicios_tarjeta);
						$scope.total_pservicios = parseInt(data[0].total_pservicios);

						$scope.salon_pservicios = parseInt(data[0].salon_pservicios);
						$scope.estilista_pservicios = parseInt(data[0].estilista_pservicios);

						$scope.totalingresosServicios = $scope.salon_pservicios + $scope.estilista_pservicios;
						
						$scope.salon_pservicios_efectivo = parseInt(data[0].salon_pservicios_efectivo);
						$scope.salon_pservicios_tarjeta = parseInt(data[0].salon_pservicios_tarjeta);
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.VentaProductosServicioDate();

// ***************************************************************************************************************
// ***************************************************************************************************************

// FUNCION PARA CONSULTAR LOS ABONOS QUE SE REALIZARON DE LOS PRESTAMOS DE CAJA PRINCIPAL ************************************************************
	
	$scope.AbonosDia = function(){

		Control.ListAbonosDiaIng().success(function(data){
			$scope.abonos = data;
			if($scope.abonos == ''){
				// console.log('no hay abonos para hoy');
			}else{
				$scope.valor_abonos = parseInt(data[0].valor_abonos);
			}
			// console.log(data);
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.AbonosDia();

// FUNCION PARA CONSULTAR LOS ABONOS QUE SE REALIZARON POR MES DE LOS PRESTAMOS DE CAJA PRINCIPAL ************************************************************

	$scope.today = new Date();

	$scope.AbonosMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListAbonosMesIng(mes, year).success(function(data){

					$scope.abonos = data;

					if($scope.abonos == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{
						$scope.valor_abonos = parseInt(data[0].valor_abonos);
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.AbonosMes();

// FUNCION PARA CONSULTAR LOS ABONOS POR CULQUIER FECHA ***********************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.AbonosDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListAbonosDateIng(fecha_con).success(function(data){
					
					$scope.abonos = data;

					if($scope.abonos == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{
						$scope.valor_abonos = parseInt(data[0].valor_abonos);
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.AbonosDate();


// SALIDAS DEL DIA *******************************************************************************************************************************************************
// ***********************************************************************************************************************************************************************

// FUNCION PARA CONSULTAR LAS SALIDAS INTERNAS PARA INGRESOS DEL DIA ************************************************************
	
	$scope.SalidasInternasDia = function(){

		Control.ListSalidasInt().success(function(data){
			$scope.salidasinternas = data;
			if($scope.salidasinternas == ''){
				// console.log('No hay ventas de productos para hoy')
			}else{
				$scope.total_salidas = parseInt(data[0].total_salidas);
			}
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.SalidasInternasDia();

// FUNCION PARA CONSULTAR LAS SALIDAS INTERNAS PARA INGRESOS DEL MES ************************************************************

	$scope.today = new Date();

	$scope.SalidasInternasMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListSalidasIntMes(mes, year).success(function(data){

					$scope.salidasinternas = data;

					if($scope.salidasinternas == ''){
						// console.log('No hay ingresos en esta consulta')

					}else{
						$scope.total_salidas = parseInt(data[0].total_salidas);;
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.SalidasInternasMes();

// FUNCION PARA CONSULTAR LAS SALIDAS INTERNAS POR CULQUIER FECHA ***********************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.SalidasInternasDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListSalidasIntDate(fecha_con).success(function(data){
					
					$scope.salidasinternas = data;

					if($scope.salidasinternas == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{
						$scope.total_salidas = parseInt(data[0].total_salidas);;
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.SalidasInternasDate();

// FUNCION PARA CONSULTAR LOS PRSTAMOS DEL DIA PARA INGRESOS DEL DIA ************************************************************
	
	$scope.PrestamosDia = function(){

		Control.ListPrestdia().success(function(data){
			$scope.prestamos = data;
			// console.log(data)
			if($scope.prestamos == ''){
				// console.log('No hay prestamos para hoy')
			}else{
				$scope.valor_prestamos = parseInt(data[0].valor_prestamos);
			}
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.PrestamosDia();

// FUNCION PARA CONSULTAR LOS PRSTAMOS DEL MES PARA INGRESOS DEL DIA ************************************************************

	$scope.today = new Date();

	$scope.PrestamosMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListPrestMes(mes, year).success(function(data){

					$scope.prestamos = data;

					if($scope.prestamos == ''){
						// console.log('No hay ingresos en esta consulta')

					}else{
						$scope.valor_prestamos = parseInt(data[0].valor_prestamos);
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.PrestamosMes();

// FUNCION PARA CONSULTAR LOS PRSTAMOS DE FECHA SELECCIONADA PARA INGRESOS DEL DIA ***********************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.PrestamosDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListPrestDate(fecha_con).success(function(data){
					
					$scope.prestamos = data;

					if($scope.prestamos == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{
						$scope.valor_prestamos = parseInt(data[0].valor_prestamos);
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.PrestamosDate();

// ***************************************************************************************************************
// ***************************************************************************************************************

// FUNCION PARA CONSULTAR LOS PRSTAMOS DEL DIA PARA INGRESOS DEL DIA ************************************************************
	
	$scope.LiquidacionesDia = function(){

		Control.ListLiquidaciondia().success(function(data){
			$scope.liquidaciones = data;
			// console.log(data)
			if($scope.liquidaciones == ''){
				// console.log('No hay liquidaciones para hoy')
			}else{
				$scope.total_liquidacion = parseInt(data[0].total_liquidacion);
			}
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.LiquidacionesDia();

// FUNCION PARA CONSULTAR LOS INGRESOS DEL MES ************************************************************

	$scope.today = new Date();

	$scope.LiquidacionesMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListLiquidacionMesIng(mes, year).success(function(data){

					$scope.liquidaciones = data;

					if($scope.liquidaciones == ''){
						// console.log('No hay ingresos en esta consulta')

					}else{
						$scope.total_liquidacion = parseInt(data[0].total_liquidacion);
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.LiquidacionesMes();


// FUNCION PARA CONSULTAR LOS INGRESOS POR CULQUIER FECHA ***********************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.LiquidacionesDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListLiquidacionDateIng(fecha_con).success(function(data){
					
					$scope.liquidaciones = data;

					if($scope.liquidaciones == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{
						$scope.total_liquidacion = parseInt(data[0].total_liquidacion);
						// console.log(data)
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.LiquidacionesDate();

// FUNCION PARA REGISTRAR UNA NOTIFICACIÓN ************************************************************
	
	$("#fecha_notif").datepicker();

	$scope.RegistrarNewNotificacion = function(){

		var usuario = $('#usuario').val();

		var notificacion = {
			tipo:$scope.tipo,
			fecha_notif:$scope.fecha_notif,
			descripcion:$scope.descripcion,
			usuario:usuario
		}

		// console.log(notificacion)

		alertify.confirm("¿Deseas registrar la información?", function (info) {

			if(info){
				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Registrar_Notificacion_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:notificacion
				}).success(function(respuesta){
					$scope.notificaciones = respuesta;
						// console.log(respuesta);
						alertify.success("Información registrada con éxito");
						window.location = "#/Control/OtrosReg/Notificaciones";
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

// FUNCION PARA CONSULTAR LAS NOTIFICACIONES DEL DIA PARA LISTADO ************************************************************
	
	$scope.ListNotificaionesDia = function(notif){

		Control.ListNotifDia(notif).success(function(data){
			$scope.notificaciones = data;
			// console.log(data)
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ListNotificaionesDia();


// FUNCION PARA CONSULTAR PRSETAMOS Y EDITARLAOS ************************************************************

	$scope.ListadoNotificacionEdit = function(){

		Control.ListNotifEdit($routeParams.idnotificaciones).success(function(data){
			
			$scope.notificaciones = data;

			$scope.idnotificaciones = data[0].idnotificaciones;
			$scope.tipo = data[0].tipo;
			$scope.fecha_notif = data[0].fecha_notif;
			$scope.descripcion = data[0].descripcion;
			$scope.nombre = parseInt(data[0].nombre);
			
			// console.log(data)

		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoNotificacionEdit();

// FUNCION PARA EDITAR UNA NOTIFICACION 

	$('.fa-pulse, #update, .fa-check').hide();

	$scope.EditNotificacion = function(idnotificaciones){
		
		$('.fa-pulse, #update').show();
		$('.fa-check').hide();

		var usuario = $('#usuario').val();
		
		datos = {
			idnotificaciones:$scope.idnotificaciones,
			tipo:$scope.tipo,
			descripcion:$scope.descripcion,
			fecha_notif:$scope.fecha_notif,
			usuario:usuario,
		}
		// console.log(datos);
			$http({
				method:'POST',
				url:'../web/includes/sql/Control/Editar_Notificacion_sql.php',
				header:{
				'Content-Type': undefined
			},
				data:datos
			}).success(function(respuesta){
				$scope.notificaiones = respuesta;
					// console.log(respuesta)
					setTimeout(function(){
						$('.fa-pulse, #update').hide();
						$('.fa-check').show();
						alertify.success("Información actualizada con éxito");
					 }, 3000);
			}).error(function(err){
					console.log("Error: "+err);
				});
	}

// FUNCION PARA ELIMINAR UNA NOTIFICACION ************************************************************

	$scope.DeleteNotif = function(idnotificaciones){

		// console.log(idnotificaciones);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Control/Delete_Notificacion_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idnotificaciones:idnotificaciones}
				}).success(function(respuesta){
					$scope.deletenotif = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ListNotificaionesDia();
							window.location = "#/Control/OtrosReg/Notificaciones";
						 }, 2000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}

	// End funcion $scope.DeleteNotif

// FUNCION PARA CONSULTAR NOTIFICACIONES REGISTRADAS POR MES ************************************************************

	$scope.today = new Date();

	$scope.ListadoNotificaionesMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListNotificacionMes(mes, year).success(function(data){

					$scope.notificaciones = data;

					if($scope.notificaciones == ''){
						// console.log('No hay servicios en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 5000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.ListadoNotificaionesMes();

// FUNCION PARA CONSULTAR VENTAS REALIZADOS POR MES ************************************************************ PENDIENTE

	$scope.today = new Date();

	$scope.ListadoVentasMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
				$scope.nodata = 1;
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Productos.ListVentasMes(mes, year).success(function(data){

					$scope.ventas = data;

					if($scope.ventas == ''){
						// console.log('No hay vemtas en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.valorT = data[0].valorT;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 4000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.ListadoVentasMes();

// ***************************************************************************************************************

// BUSQUEDA DE VENTAS POR FECHA ***********************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.ListadoVentasDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Productos.ListVentasDate(fecha_con).success(function(data){
					
					$scope.ventas = data;

					if($scope.ventas == ''){
						console.log('No hay VENTAS en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.valorT = data[0].valorT;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 3000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ListadoVentasDate();	

// END BUSQUEDA POR FECHA ***********************************************************************************
// ***************************************************************************************************************


// CONSULTAS DE PRODUCTOS POR PORCIONES

// Ver Info en modal de producto editar

$scope.verproductoporciones = function(idproductos, nom_producto, valor_unit, descuento){

	$('.fa-pulse, #update, .fa-check').hide();
	$scope.idproductos = idproductos;
	$scope.nom_producto = nom_producto;
	$scope.valor_unit = valor_unit;
	$scope.descuento = descuento;

}

// Ediar producto - PORCIONES

$scope.editarproductoporciones = function(){

	$('.fa-pulse, #update').show();
	$('.fa-check').hide();

	var res_valor = $scope.valor_unit.replace(',', "");
	$scope.valor_unit = res_valor;
	var res_descuento = $scope.descuento.replace(',', "");
	$scope.descuento = res_descuento;

	datos = {
		idproductos:$scope.idproductos,
		nom_producto:$scope.nom_producto,
		valor_unit:$scope.valor_unit,
		descuento:$scope.descuento
	}

	console.log(datos);

	$http({
		method:'POST',
		url:'../web/includes/sql/Productos/editar_producto_porcion_sql.php',					
		header:{
		'Content-Type': undefined
	},
		data:datos
	}).success(function(respuesta){

		$scope.editproduct = respuesta;
		console.log(respuesta)

		if(respuesta == 1){
			alertify.success("Información actualizada con éxito");	
			$('.fa-pulse, #update').hide();
			$('.fa-check').show();
			$scope.ListadoPorcionesGeneral();				
		}

	}).error(function(err){
			console.log("Error: "+err);
		});

}	

// FUNCION PARA CONSULTAR PORCIONES EN GENERAL ************************************************************

	$scope.ListadoPorcionesGeneral = function(){

		Productos.ListPorciones().success(function(data){
			
			$scope.prodporciones = data;

			$scope.total_final_venta = data[0].total_final_venta;
	
			// console.log(data);

		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoPorcionesGeneral();


// FUNCION PARA CONSULTAR PORCIONES PRODUCTOS DEL MES SELECCIONADO *********************************************************************************************

	$scope.today = new Date();

	$scope.ListadoPorcionesMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
				$scope.nodata = 1;
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Productos.ListPorcionesMes(mes, year).success(function(data){

					$scope.prodporciones = data;

					if($scope.prodporciones == ''){
						// console.log('No hay VENTAS PORCIONES MES en esta consulta');
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.total_final_venta = parseInt(data[0].total_final_venta);

						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 2000);
					}
						// console.log(data);

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.ListadoPorcionesMes();

// FUNCION PARA CONSULTAR LOS PRODUCTOS PORCIONES DE CUALQUIER FECHA SELECCIONADO *********************************************************************************************	

	$("#fecha_consulta").datepicker();

    $scope.ListadoPorcionesDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Productos.ListPorcionesDate(fecha_con).success(function(data){
					
					$scope.prodporciones = data;

					if($scope.prodporciones == ''){
						console.log('No hay PORCIONES en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.total_final_venta = parseInt(data[0].total_final_venta);
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 2000);
					}
						// console.log(data)

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ListadoPorcionesDate();	













// *********************************************************************************************************************************************************************
// *********************************************************************************************************************************************************************
// INGRESOS DEL DIA  SOLO VALORES **************************************************************************************************************************************
// *********************************************************************************************************************************************************************
// *********************************************************************************************************************************************************************

// ENTRADAS *********************

// FUNCION PARA CONSULTAR LOS SERVIVIOS DEL DIA INGRESOS DEL DIA *********************************************************************************************
	
	$scope.ServiciosdelDiaIngresos = function(){

		Control.ListServiciosDiaVal().success(function(data){
			$scope.serviciosdiarios = data;
			// console.log(data)
			if($scope.serviciosdiarios == ''){
				// console.log('no hay servicios para hoy');
			}else{
				$scope.ValTotalServicios = parseInt(data[0].ValTotalServicios);
				$scope.ValTotalDescuentos = parseInt(data[0].ValTotalDescuentos);
				$scope.ValFinalServicio = parseInt(data[0].ValFinalServicio);
				$scope.ValTotalSalon = parseInt(data[0].ValTotalSalon);
				$scope.ValTotalEstilista = parseInt(data[0].ValTotalEstilista);
				$scope.PagoServiciosEfectivo = parseInt(data[0].PagoServiciosEfectivo);
				$scope.PagoServiciosTarjeta = parseInt(data[0].PagoServiciosTarjeta);
			}
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ServiciosdelDiaIngresos();

// FUNCION PARA CONSULTAR LOS SERVIVIOS DEL DIA INGRESOS DEL MES SELECCIONADO *********************************************************************************************

	$scope.today = new Date();

	$scope.ServiciosdelMesIngresos = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
				$scope.nodata = 1;
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListServiciosMesVal(mes, year).success(function(data){

					$scope.serviciosdiarios = data;

					if($scope.serviciosdiarios == ''){
						// console.log('No hay ingresos MES en esta consulta');
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.ValTotalServicios = parseInt(data[0].ValTotalServicios);
						$scope.ValTotalDescuentos = parseInt(data[0].ValTotalDescuentos);
						$scope.ValFinalServicio = parseInt(data[0].ValFinalServicio);
						$scope.ValTotalSalon = parseInt(data[0].ValTotalSalon);
						$scope.ValTotalEstilista = parseInt(data[0].ValTotalEstilista);
						$scope.PagoServiciosEfectivo = parseInt(data[0].PagoServiciosEfectivo);
						$scope.PagoServiciosTarjeta = parseInt(data[0].PagoServiciosTarjeta);
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 5000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.ServiciosdelMesIngresos();

// FUNCION PARA CONSULTAR LOS SERVIVIOS DEL DIA INGRESOS DE CUALQUIER FECHA SELECCIONADO *********************************************************************************************	

	$("#fecha_consulta").datepicker();

    $scope.ServiciosdelDateIngresos = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListServiciosDateVal(fecha_con).success(function(data){
					
					$scope.serviciosdiarios = data;

					if($scope.serviciosdiarios == ''){
						// console.log('No hay ingresos en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.ValTotalServicios = parseInt(data[0].ValTotalServicios);
						$scope.ValTotalDescuentos = parseInt(data[0].ValTotalDescuentos);
						$scope.ValFinalServicio = parseInt(data[0].ValFinalServicio);
						$scope.ValTotalSalon = parseInt(data[0].ValTotalSalon);
						$scope.ValTotalEstilista = parseInt(data[0].ValTotalEstilista);
						$scope.PagoServiciosEfectivo = parseInt(data[0].PagoServiciosEfectivo);
						$scope.PagoServiciosTarjeta = parseInt(data[0].PagoServiciosTarjeta);
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 4000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ServiciosdelDateIngresos();


// FUNCION PARA CONSULTAR LOS PRODUCTOS VENDIDOS DEL DIA INGRESOS DEL DIA *********************************************************************************************
// PRODUCTOS DE LAVACABEZAS - SERVICIOS Y A PARTE LOS SHAMPOO
	
	$scope.ProductosdelDiaIngresos = function(){

		Control.ListProductosDiaVal().success(function(data){
			$scope.productosdiarios = data;
			// console.log(data)
			if($scope.productosdiarios == ''){
				// console.log('no hay productos para hoy');
			}else{
				$scope.ValorVentasProductos = parseInt(data[0].ValorVentasProductos);
				$scope.ValorComisionEstilista = parseInt(data[0].ValorComisionEstilista);
				$scope.TotalVentasProductos = parseInt(data[0].TotalVentasProductos);
				$scope.VentaProductosEfectivo = parseInt(data[0].VentaProductosEfectivo);
				$scope.VentaProductosTarjeta = parseInt(data[0].VentaProductosTarjeta);
				$scope.VentaShampooEfectivo = parseInt(data[0].VentaShampooEfectivo);
				$scope.VentaEnjuagueEfectivo = parseInt(data[0].VentaEnjuagueEfectivo);
				$scope.VentaShampooTotal = parseInt(data[0].VentaShampooTotal);

				$scope.TotalProductosEfectivo = $scope.VentaProductosEfectivo + $scope.VentaShampooEfectivo + $scope.VentaEnjuagueEfectivo;
				// console.log($scope.VentaShampooTotal);
			}
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.ProductosdelDiaIngresos();


// FUNCION PARA CONSULTAR LOS PRODUCTOS VENDIDOS DEL MES INGRESOS DEL DIA *********************************************************************************************

	$scope.today = new Date();

	$scope.ProductosdelMesIngresos = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListProductosMesVal(mes, year).success(function(data){	

					$scope.productosdiarios = data;

					if($scope.productosdiarios == ''){
						// console.log('No hay ingresos MES en esta consulta');

					}else{

						$scope.ValorVentasProductos = parseInt(data[0].ValorVentasProductos);
						$scope.ValorComisionEstilista = parseInt(data[0].ValorComisionEstilista);
						$scope.TotalVentasProductos = parseInt(data[0].TotalVentasProductos);
						$scope.VentaProductosEfectivo = parseInt(data[0].VentaProductosEfectivo);
						$scope.VentaProductosTarjeta = parseInt(data[0].VentaProductosTarjeta);
						$scope.VentaShampooEfectivo = parseInt(data[0].VentaShampooEfectivo);
						$scope.VentaEnjuagueEfectivo = parseInt(data[0].VentaEnjuagueEfectivo);
						$scope.VentaShampooTotal = parseInt(data[0].VentaShampooTotal);

						$scope.TotalProductosEfectivo = $scope.VentaProductosEfectivo + $scope.VentaShampooEfectivo + $scope.VentaEnjuagueEfectivo;
						// console.log(data);

					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.ProductosdelMesIngresos();


// FUNCION PARA CONSULTAR LOS PRODUCTOS VENDIDOS DE LA FECHA SELECCIONADA INGRESOS DEL DIA *********************************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.ProductosdelDateIngresos = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListProductosDateVal(fecha_con).success(function(data){
					
					$scope.productosdiarios = data;

					if($scope.productosdiarios == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{

						$scope.ValorVentasProductos = parseInt(data[0].ValorVentasProductos);
						$scope.ValorComisionEstilista = parseInt(data[0].ValorComisionEstilista);
						$scope.TotalVentasProductos = parseInt(data[0].TotalVentasProductos);
						$scope.VentaProductosEfectivo = parseInt(data[0].VentaProductosEfectivo);
						$scope.VentaProductosTarjeta = parseInt(data[0].VentaProductosTarjeta);
						$scope.VentaShampooEfectivo = parseInt(data[0].VentaShampooEfectivo);
						$scope.VentaEnjuagueEfectivo = parseInt(data[0].VentaEnjuagueEfectivo);
						$scope.VentaShampooTotal = parseInt(data[0].VentaShampooTotal);

						$scope.TotalProductosEfectivo = $scope.VentaProductosEfectivo + $scope.VentaShampooEfectivo + $scope.VentaEnjuagueEfectivo;
						// console.log(data);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ProductosdelDateIngresos();

// ***************************************************************************************************************
// ***************************************************************************************************************	


// FUNCION PARA CONSULTAR OTROS INGRESOS DEL DIA INGRESOS DEL DIA *********************************************************************************************
	
	$scope.OtrosIngersosdelDiaIngresos = function(){

		Control.ListOtrosIngresosDiaVal().success(function(data){
			$scope.otrosingresos = data;
			// console.log(data)
			if($scope.otrosingresos == ''){
				// console.log('no hay otros ingresos para hoy');
			}else{
				$scope.TotalOtrosIngresos = parseInt(data[0].TotalOtrosIngresos);
				$scope.OtrosIngresosEfectivo = parseInt(data[0].OtrosIngresosEfectivo);
				$scope.OtrosIngresosTarjeta = parseInt(data[0].OtrosIngresosTarjeta);
			}
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.OtrosIngersosdelDiaIngresos();


// FUNCION PARA CONSULTAR OTROS INGRESOS MES INGRESOS DEL DIA *********************************************************************************************

	$scope.today = new Date();

	$scope.OtrosIngersosdelMesIngresos = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListOtrosIngresosMesVal(mes, year).success(function(data){	

					$scope.otrosingresos = data;

					if($scope.otrosingresos == ''){
						// console.log('No hay ingresos MES en esta consulta');

					}else{

						$scope.TotalOtrosIngresos = parseInt(data[0].TotalOtrosIngresos);
						$scope.OtrosIngresosEfectivo = parseInt(data[0].OtrosIngresosEfectivo);
						$scope.OtrosIngresosTarjeta = parseInt(data[0].OtrosIngresosTarjeta);
						// console.log(data);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.OtrosIngersosdelMesIngresos();

// FUNCION PARA CONSULTAR OTROS INGRESOS DE LA FECHA SELECCIONADA INGRESOS DEL DIA *********************************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.OtrosIngersosdelDateIngresos = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListOtrosIngresosDateVal(fecha_con).success(function(data){
					
					$scope.otrosingresos = data;

					if($scope.otrosingresos == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{

						$scope.TotalOtrosIngresos = parseInt(data[0].TotalOtrosIngresos);
						$scope.OtrosIngresosEfectivo = parseInt(data[0].OtrosIngresosEfectivo);
						$scope.OtrosIngresosTarjeta = parseInt(data[0].OtrosIngresosTarjeta);
						// console.log(data);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.OtrosIngersosdelDateIngresos();

// ***************************************************************************************************************
// ***************************************************************************************************************


// FUNCION PARA CONSULTAR LOS OTROS INGRESOS DEL DIA INGRESOS DEL DIA *********************************************************************************************
	
	$scope.AbonosdelDiaIngresos = function(){

		Control.ListAbonosDiaVal().success(function(data){
			$scope.abonos = data;
			// console.log(data)
			if($scope.abonos == ''){
				// console.log('no hay abonos para hoy');
			}else{
				$scope.TotalAbonos = parseInt(data[0].TotalAbonos);
			}
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.AbonosdelDiaIngresos();


// FUNCION PARA CONSULTAR OTROS INGRESOS MES INGRESOS DEL DIA *********************************************************************************************

	$scope.today = new Date();

	$scope.AbonosdelMesIngresos = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListAbonosMesVal(mes, year).success(function(data){

					$scope.abonos = data;

					if($scope.abonos == ''){
						// console.log('No hay ingresos MES en esta consulta');

					}else{

						$scope.TotalAbonos = parseInt(data[0].TotalAbonos);
						// console.log(data);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.AbonosdelMesIngresos();


// FUNCION PARA CONSULTAR ABONOS DE LA FECHA SELECCIONADA INGRESOS DEL DIA *********************************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.AbonosdelDateIngresos = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListAbonosMesVal(fecha_con).success(function(data){
					
					$scope.abonos = data;

					if($scope.abonos == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{

						$scope.TotalAbonos = parseInt(data[0].TotalAbonos);
						// console.log(data);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.AbonosdelDateIngresos();

// ***************************************************************************************************************
// ***************************************************************************************************************	


// FUNCION PARA CONSULTAR LAS PRODPINAS DEL DIA INGRESOS DEL DIA *********************************************************************************************
	
	$scope.PropinasdelDiaIngresos = function(){

		Control.ListPropinasDiaVal().success(function(data){
			$scope.propinas = data;
			// console.log(data)
			if($scope.propinas == ''){
				// console.log('no hay propinas para hoy');
			}else{
				$scope.TotalPropinas = parseInt(data[0].TotalPropinas);
			}
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.PropinasdelDiaIngresos();


// FUNCION PARA CONSULTAR OTROS INGRESOS MES INGRESOS DEL DIA *********************************************************************************************

	$scope.today = new Date();

	$scope.PropinasdelMesIngresos = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListPropinasMesVal(mes, year).success(function(data){

					$scope.propinas = data;

					if($scope.propinas == ''){
						// console.log('No hay ingresos MES en esta consulta');

					}else{

						$scope.TotalPropinas = parseInt(data[0].TotalPropinas);
						// console.log(data);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.PropinasdelMesIngresos();


// FUNCION PARA CONSULTAR PROPINAS DE LA FECHA SELECCIONADA INGRESOS DEL DIA *********************************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.PropinasdelDateIngresos = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListPropinasDateVal(fecha_con).success(function(data){
					
					$scope.propinas = data;

					if($scope.propinas == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{

						$scope.TotalPropinas = parseInt(data[0].TotalPropinas);
						// console.log(data);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.PropinasdelDateIngresos();

// ***************************************************************************************************************
// ***************************************************************************************************************		




// *******************************
// *******************************
// SALIDAS ***********************
// *******************************
// *******************************
// *******************************
// *******************************

// FUNCION PARA CONSULTAR LAS SALIDAS INTERDAS DEL DIA INGRESOS DEL DIA *********************************************************************************************
	
	$scope.SalidasInternasdelDiaIngresos = function(){

		Control.ListSalidasIntDiaVal().success(function(data){
			$scope.salidasinternas = data;
			// console.log(data)
			if($scope.salidasinternas == '' || $scope.salidasinternas == null){
				// console.log('no hay salidas para hoy');
			}else{
				$scope.TotalSalidasInternas = parseInt(data[0].TotalSalidasInternas);
			}
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.SalidasInternasdelDiaIngresos();


// FUNCION PARA CONSULTAR SALIDAS INTERNAS MES INGRESOS DEL DIA *********************************************************************************************

	$scope.today = new Date();

	$scope.SalidasInternasdelMesIngresos = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListSalidasIntMesVal(mes, year).success(function(data){

					$scope.salidasinternas = data;

					if($scope.salidasinternas == ''){
						// console.log('No hay ingresos MES en esta consulta');

					}else{
						$scope.TotalSalidasInternas = parseInt(data[0].TotalSalidasInternas);
						// console.log(data);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.SalidasInternasdelMesIngresos();

// FUNCION PARA CONSULTAR SALIDAS INTERNAS DE LA FECHA SELECCIONADA INGRESOS DEL DIA *********************************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.SalidasInternasdelDateIngresos = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListSalidasIntDateVal(fecha_con).success(function(data){
					
					$scope.salidasinternas = data;

					if($scope.salidasinternas == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{

						$scope.TotalSalidasInternas = parseInt(data[0].TotalSalidasInternas);
						// console.log(data);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.SalidasInternasdelDateIngresos();

// ***************************************************************************************************************
// ***************************************************************************************************************		


// FUNCION PARA CONSULTAR LAS PRESTAMOS DEL DIA INGRESOS DEL DIA *********************************************************************************************
	
	$scope.PrestamosdelDiaIngresos = function(){

		Control.ListPrestamosDiaVal().success(function(data){
			$scope.prestamos = data;
			// console.log(data)
			if($scope.prestamos == ''){
				// console.log('no hay prestamos para hoy');
			}else{
				$scope.TotalPrestamos = parseInt(data[0].TotalPrestamos);
			}
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.PrestamosdelDiaIngresos();


// FUNCION PARA CONSULTAR PRESTAMOS DEL MES INGRESOS DEL DIA *********************************************************************************************

	$scope.today = new Date();

	$scope.PrestamosdelMesIngresos = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListPrestamosMesVal(mes, year).success(function(data){

					$scope.prestamos = data;

					if($scope.prestamos == ''){
						// console.log('No hay ingresos MES en esta consulta');

					}else{
						$scope.TotalPrestamos = parseInt(data[0].TotalPrestamos);
						// console.log(data);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.PrestamosdelMesIngresos();


// FUNCION PARA CONSULTAR PRESTAMOS DE LA FECHA SELECCIONADA INGRESOS DEL DIA *********************************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.PrestamosdelDateIngresos = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListPrestamosDateVal(fecha_con).success(function(data){
					
					$scope.prestamos = data;

					if($scope.prestamos == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{

						$scope.TotalPrestamos = parseInt(data[0].TotalPrestamos);
						// console.log(data);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.PrestamosdelDateIngresos();

// ***************************************************************************************************************
// ***************************************************************************************************************	



// FUNCION PARA CONSULTAR LAS LIQUIDACIONES DEL DIA INGRESOS DEL DIA *********************************************************************************************
	
	$scope.LiquidacionesdelDiaIngresos = function(){

		Control.ListLiquidacionDiaVal().success(function(data){
			$scope.liquidaciones = data;
			// console.log(data)
			if($scope.liquidaciones == ''){
				// console.log('no hay prestamos para hoy');
			}else{
				$scope.TotalLiquidaciones = parseInt(data[0].TotalLiquidaciones);
			}
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.LiquidacionesdelDiaIngresos();


// FUNCION PARA CONSULTAR LIQUIDACIONES DEL MES INGRESOS DEL DIA *********************************************************************************************

	$scope.today = new Date();

	$scope.LiquidacionesdelMesIngresos = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListLiquidacionMesVal(mes, year).success(function(data){

					$scope.liquidaciones = data;

					if($scope.liquidaciones == ''){
						// console.log('No hay ingresos MES en esta consulta');

					}else{
						$scope.TotalLiquidaciones = parseInt(data[0].TotalLiquidaciones);
						// console.log(data);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.LiquidacionesdelMesIngresos();


// FUNCION PARA CONSULTAR LIQUIDACIONES DE LA FECHA SELECCIONADA INGRESOS DEL DIA *********************************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.LiquidacionesdelDateIngresos = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListLiquidacionDateVal(fecha_con).success(function(data){
					
					$scope.liquidaciones = data;

					if($scope.liquidaciones == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{

						$scope.TotalLiquidaciones = parseInt(data[0].TotalLiquidaciones);
						// console.log(data);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.LiquidacionesdelDateIngresos();

// ***************************************************************************************************************
// ***************************************************************************************************************	


// FUNCION PARA CONSULTAR LOS LABACABEZAS DEL DIA INGRESOS DEL DIA *********************************************************************************************
// SE RESTA A INGRESOS DEL DIA PORQUE LAVACABEZAS NO SE SUMA EN LIQUIDACIONES

	$scope.LavacabezasdelDiaIngresos = function(){

		Control.ListLavacabezaDiaVal().success(function(data){
			$scope.lavacabezas = data;
			// console.log(data)
			if($scope.lavacabezas == ''){
				// console.log('no hay lavacabezas para hoy para hoy');
			}else{
				$scope.LiquidacionLavacabezas = parseInt(data[0].LiquidacionLavacabezas);
			}
		}).error(function(error){
			console.log("Error."+error);
		});
	}
	// $scope.LavacabezasdelDiaIngresos();


// FUNCION PARA CONSULTAR LAVACABEZAS DEL MES INGRESOS DEL DIA *********************************************************************************************

	$scope.today = new Date();

	$scope.LavacabezasdelMesIngresos = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				$scope.nodata = 1;
			}else{

				Control.ListLavacabezaMesVal(mes, year).success(function(data){

					$scope.lavacabezas = data;

					if($scope.lavacabezas == ''){
						// console.log('No hay ingresos MES en esta consulta');

					}else{
						$scope.LiquidacionLavacabezas = parseInt(data[0].LiquidacionLavacabezas);
						// console.log(data);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.LavacabezasdelMesIngresos();

// FUNCION PARA CONSULTAR LIQUIDACIONES DE LA FECHA SELECCIONADA INGRESOS DEL DIA *********************************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.LavacabezasdelDateIngresos = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				Control.ListLavacabezaDateVal(fecha_con).success(function(data){
					
					$scope.lavacabezas = data;

					if($scope.lavacabezas == ''){
						// console.log('No hay ingresos en esta consulta')
					}else{

						$scope.LiquidacionLavacabezas = parseInt(data[0].LiquidacionLavacabezas);
						// console.log(data);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.LavacabezasdelDateIngresos();

// ***************************************************************************************************************
// ***************************************************************************************************************	


// FUNCION PARA CONSULTAR DETALLE DE SERVICIO INDIVIDUAL REPETIDA PARA PODER VER EL MODAL ************************************************************

	$scope.DetalleServicioInd = function(idempleados, idservicio){

		$('.modal-content').hide();

		swal({
          title: 'Cargando',
          text: 'Espere un momento...',
          timer: 2000,
          onOpen: () => {
            swal.showLoading()
          }
        }).then((result) => {
          if (result.dismiss === 'timer') {
			$('.modal-content').show();
          }
        })
        // End Swall

		Servicios.ListDetalleInd(idempleados, idservicio).success(function(data){
			
			$scope.detalles = data;
			// console.log($scope.detalles);
			$scope.descuento = data[0].descuento;
			$scope.nom_producto = data[0].nom_producto;
			$scope.nom_empleado = data[0].nom_empleado;
			$scope.ape_empleado = data[0].ape_empleado;
			$scope.nom_tipo = data[0].nom_tipo;
			$scope.fecha_servicio = data[0].fecha_servicio;
			$scope.nom_cliente = data[0].nom_cliente;
			$scope.ape_cliente = data[0].ape_cliente;
			$scope.valor = data[0].valor;
			$scope.valor_final = data[0].valor_final;
			$scope.valor_estilista = data[0].valor_estilista;
			$scope.valor_salon = data[0].valor_salon;
			$scope.tipo_pago = data[0].tipo_pago;
			$scope.descuento_total = data[0].descuento_total;
			$scope.total_salon = data[0].total_salon;
			$scope.tipo_pago = data[0].tipo_pago;
			$scope.observacion = data[0].observacion;
			// console.log(data);

			if($scope.observacion == null){
				$scope.observacion = 'Sin observaciones';
			}

		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.DetalleServicioInd();


// CONSULTAS DATE X SERVICIOS en consulta general.


// FUNCION PARA CONSULTAR SERVICIOS REALIZADOS POR MES ************************************************************

	$scope.today = new Date();

	$scope.ListadoServiciosMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
				$scope.nodata = 1;
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListServiciosMes(mes, year).success(function(data){

					$scope.serviciosdate = data;

					if($scope.serviciosdate == ''){
						// console.log('No hay servicios en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.total_servicios = data[0].total_servicios;
						$scope.cant_servicios = data[0].cant_servicios;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 5000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.ListadoServiciosMes();

// ***************************************************************************************************************
// ***************************************************************************************************************

// BUSQUEDA DE SERVICIOS POR FECHA ***********************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.ListadoServiciosDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListServiciosDate(fecha_con).success(function(data){
					
					$scope.serviciosdate = data;

					if($scope.serviciosdate == ''){
						// console.log('No hay servicios en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.total_servicios = data[0].total_servicios;
						$scope.cant_servicios = data[0].cant_servicios;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 4000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ListadoServiciosDate();	

// END BUSQUEDA SERVICIOS POR FECHA ***********************************************************************************
// ***************************************************************************************************************


// FUNCION PARA CONSULTAR SERVICIOS LAVACABEZAS REALIZADOS POR MES ************************************************************

	$scope.today = new Date();

	$scope.ListadoOtrosServiciosMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
				$scope.nodata = 1;
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListOtrosServiciosMes(mes, year).success(function(data){

					$scope.otrosservicios = data;

					if($scope.otrosservicios == ''){
						// console.log('No hay lavacabezas en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.total_servicios = data[0].total_servicios;
						$scope.cant_servicios = data[0].cant_servicios;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 3000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.ListadoOtrosServiciosMes();

// ***************************************************************************************************************
// ***************************************************************************************************************

// BUSQUEDA DE SERVICIOS POR FECHA ***********************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.ListadoOtrosServiciosDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListOtrosServiciosDate(fecha_con).success(function(data){
					
					$scope.otrosservicios = data;

					if($scope.otrosservicios == ''){
						// console.log('No hay lavacabezas en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 1000);
					}else{
						$scope.total_servicios = data[0].total_servicios;
						$scope.cant_servicios = data[0].cant_servicios;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 2000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ListadoOtrosServiciosDate();



// FUNCION PARA CONSULTAR PROPINAS REALIZADOS POR MES ************************************************************

	$scope.today = new Date();

	$scope.ListadoPropinasMes = function(info){

		$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var mes = $scope.mes;
			var year = $scope.year;

			if(mes == 0){
				alertify.log("<b>Info: </b>¡Debe Seleccionar un Mes!");
				$scope.nodata = 1;
			}else{

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListPropinasMes(mes, year).success(function(data){

					$scope.propinas = data;

					if($scope.propinas == ''){
						// console.log('No hay propinas en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 1000);
					}else{
						$scope.suma_propinas = data[0].suma_propinas;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 3000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		}
	}

	// $scope.ListadoPropinasMes();


// BUSQUEDA DE SERVICIOS POR FECHA ***********************************************************************************

	$("#fecha_consulta").datepicker();

    $scope.ListadoPropinasDate = function(info) {

    	$scope.nodata = 0;

		if(info == 0 || info == 'undefined'){
			$scope.nodata = 1;
		}else{

			var fecha_con = $('#fecha_consulta').val();

			if(fecha_con == ''){
				alertify.log("<b>Info: </b>¡Debe Seleccionar una Fecha!");
				$scope.nodata = 1;
			}else{
				// console.log(fecha_con);
				var capturadate = $('#capturafecha').val(fecha_con);

				alert('¡Espere un momento!<br><i class=" mt10 fa fa-circle-o-notch fa-spin fa-3x fa-fw mb10"></i><br>Se está consultando la información.')
				$('.alertuse').css('display','none')

				Control.ListPropinasDate(fecha_con).success(function(data){
					
					$scope.propinas = data;

					if($scope.propinas == ''){
						// console.log('No hay PROPINAS en esta consulta')
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.error("<b>Info: </b>Información no encontrada");
						 }, 2000);
					}else{
						$scope.suma_propinas = data[0].suma_propinas;
						// console.log(data)
						setTimeout(function(){
							this._alertOverlay.remove();
	    					this._alertWindow.remove();
							alertify.success("<b>Info: </b>Información encontrada");
						 }, 4000);
					}

				}).error(function(error){
					console.log("Error."+error);
				});
			}
		} 			
	}
	// $scope.ListadoPropinasDate();


// FUNCION PARA CONSULTAR TODOS LOS USUARIOS REGISTRADOS ************************************************************

	$scope.ListadoUsuarios = function(){

		Control.ListUsuarios().success(function(data){

			$scope.usuarios = data;
			// console.log(data)

		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListadoUsuarios();



// FUNCION PARA CONSULTAR USUARIO EDITARLO PARA PODER VER EL MODAL ************************************************************

	$scope.ListarEditUsuario = function(idlogin){

		Control.ListditUser(idlogin).success(function(data){
			
			$scope.usuarioedit = data;

			$scope.idlogin = data[0].idlogin;
			$scope.nombre = data[0].nombre;
			$scope.apellido = data[0].apellido;
			$scope.nom_user = data[0].nom_user;
			$scope.correo = data[0].correo;
			$scope.pass_user = data[0].pass_user;
			$scope.fecha_creacion = data[0].fecha_creacion;
			$scope.idrol = data[0].idrol;
			$scope.rol = data[0].rol;
			$scope.estado = data[0].estado;
			$scope.nom_estado = data[0].nom_estado;
			$scope.nom_creador = data[0].nom_creador;

			// console.log(data);

		}).error(function(error){
			console.log("Error."+error);
		});
	}

	// $scope.ListarEditUsuario();


// CAMBIAR ESTADO DE USUARIO / ACTIVAR O DESACTIVAR ________________________________________________________
        
    $scope.CambiarEstadoUser = function(idlogin, estado){
  	
    	// console.log('Estado es: ' + estado + ' USER: ' + idlogin)

    	if(estado == 0){
    		estado = 1;
    	}else{
    		estado = 0;
    	}
    	// console.log('ESTADO A CAMBIAR: ' + estado + ' USER: ' + idlogin)

    	Control.CambEstadoUser(idlogin, estado).success(function(data){
    		// console.log(data);
    		$scope.ListadoUsuarios();
		}).error(function(error){
			console.log("Error."+error);
		});
    };

// FUNCION PARA EDITAR INFORMACION DE UN USUARIO ************************************************************

	$('.fa-pulse, #update, .fa-check').hide();

	$scope.EditarUsuario = function(idlogin){

		var rol = $scope.rol;
		var correo = $scope.correo;
		var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		$('.fa-check').hide();

		if ( !expr.test(correo) ){
	        alert("Error de correo");
	        alertify.error("La dirección de correo <br>" + correo + "<br> es incorrecta.");
		}else{

			if(rol == 'Administrador'){
				rol = 3;
			}else{
				rol = 2;
			}
			// console.log('rol id es: ' + rol)

			$('.fa-pulse, #update').show();

			datos = {
				idlogin:idlogin,
				nombre:$scope.nombre,
				apellido:$scope.apellido,
				nom_user:$scope.nom_user,
				correo:$scope.correo,
				pass_user:$scope.pass_user,
				idrol:rol,
			}
			// console.log(datos);

			$http({
				method:'POST',
				url:'../web/includes/sql/Control/Usuarios/Update_Info_Usuario_sql.php',					
				header:{
				'Content-Type': undefined
			},
				data:datos
			}).success(function(respuesta){
				$scope.updateuser = respuesta;
					// console.log(respuesta)
					setTimeout(function(){
						$scope.ListadoUsuarios();
						$('.fa-pulse, #update').hide();
						$('.fa-check').show();
						$('#cantidad, #total').val('');
						alertify.success("Información actualizada con éxito");
						alert("La información del usuario se 'actualizó' y<br>se envió correctamente al siguiente correo: <br><br> <span class='correoest fs15'> <i class='fa fa-envelope'></i>: " + correo + "</span>");
					 }, 3000);
			}).error(function(err){
					console.log("Error: "+err);
				});
		}
  
	}


// REGISTRAR NUEVO USUARIO PARA LOGIN

	$scope.RegistrarUsuario = function(){

		var usuario = $('#usuario').val();
		var correo = $scope.correo.toLowerCase();
		var idrol = $scope.idrol;
		var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		if ( !expr.test(correo) ){
	        alertify.error("La dirección de correo <br>" + correo + "<br> es incorrecta.");
		}
			else if(idrol == undefined){
				alertify.error('Seleccione un tipo usuario');
				$('#tipouser').focus();
			}else{

				var datosusuario = {
					nombre:$scope.nombre,
					apellido:$scope.apellido,
					nom_user:$scope.nom_user,
					correo:correo,
					pass_user:$scope.pass_user,
					idrol:idrol,
					creador:usuario
				}
				// console.log(datosusuario)

				alertify.confirm("¿Deseas registrar la información?", function (info) {

					if(info){
						
						alert('¡Espere un momento! <br>Se está guardando la información. <br><i class=" mt10 fa fa-spinner fa-spin fa-3x fa-fw"></i>')
						$('.alertuse').css('display','none')

						$http({
							method:'POST',
							url:'../web/includes/sql/Control/Usuarios/Crear_New_Usuario_sql.php',
							header:{
							'Content-Type': undefined
						},
							data:datosusuario
							}).success(function(respuesta){

								$scope.createuser = respuesta;
								// console.log(respuesta);

								setTimeout(function(){
									this._alertOverlay.remove();
		        					this._alertWindow.remove();
		        					alert("La información del 'nuevo' usuario<br>se envió correctamente al siguiente correo: <br><br> <span class='correoest fs15'> <i class='fa fa-envelope'></i>: " + correo + "</span>");
									alertify.success("<b>Info: </b>Información registrada con éxito");
									window.location = "#/Control/OtrosReg/GestionUsers";
								 }, 2000);
							}).error(function(err){
								console.log("Error: "+err);
						});	
					}else{
						alertify.error("<b>Info: </b>¡Proceso cancelado!");
					}
				});
			}
	}


// FUNCION DE ACORDEON PARA GESTION DE SERVICIOS - TIPOS DE SERVICIOS	

	$scope.acordeon = function(){
		
		$('.pace-done').removeClass('modal-open');

		setTimeout(function(){

			(function () {
				var accordions, i;
			  
				// Make sure the browser supports what we are about to do.
				if (!document.querySelectorAll || !document.body.classList) return;

				// Using a function helps isolate each accordion from the others
				function makeAccordion(accordion) {
					var targets, currentTarget, i;

					targets = accordion.querySelectorAll('.accordion > * > h1');
					for(i = 0; i < targets.length; i++) {
						targets[i].addEventListener('click', function () {
							if (currentTarget)
								currentTarget.classList.remove('expanded');

							currentTarget = this.parentNode;
							currentTarget.classList.add('expanded');
						}, false);
					}

					accordion.classList.add('js');
				}

				// Find all the accordions to enable
				accordions = document.querySelectorAll('.accordion');

				// Array functions don't apply well to NodeLists
				for(i = 0; i < accordions.length; i++) {
					makeAccordion(accordions[i]);
				}
			  
			})();

			// console.log('Acordeon***');

		}, 1000);

	}

	$scope.acordeon();


// FUNCION PARA CONSULTAR TODOS TIPOS DE SERVICIO PARA SELECCION PARA GESTION DE SERVICIOS ************************************************************
	
	$scope.ListadoTipoServicioSelect = function(){

		ListGenerales.ListTipoServiceGest().success(function(data){
			$scope.tipo_servicios = data;
			// console.log(data);
		}).error(function(error){
			console.log("Error."+error);
		});

	}


// FUNCION PARA CONSULTAR TODOS LOS PRODUCTOS TIPO SERVICIO PARA AGREGARLO A UN SERVICIO ************************************************************
	
	$scope.ListadoProductoServicioSelect = function(){

		ListGenerales.ListProductService().success(function(data){
			$scope.productoservicio = data;
			// console.log(data);
		}).error(function(error){
			console.log("Error."+error);
		});
		
	}

	$scope.ListadoProductoServicioSelect();


// FUNCION PARA CREAR UN NUEVO TIPO DE SERVICIO *****************************************************************************************

	$scope.CrearTipoServicio = function(){

		var tiposervicio = {
			nom_tipo:$scope.nom_tipo
		}

		// console.log(tiposervicio)

		alertify.confirm("¿Deseas registrar la información?", function (info) {

			if(info){
				$http({
					method:'POST',
					url:'../web/includes/sql/Control/GestionServicios/Registrar_TipoServicio_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:tiposervicio
				}).success(function(respuesta){
					$scope.tiposervicios = respuesta;
					if($scope.tiposervicios == 1){
						alert('<i class="fa fa-exclamation-triangle error fs50 animated wobble"></i><br>¡Ya existe un servicio con este nombre!');
						alertify.error("<b>Info: </b>¡Ya existe este servicio!");
						$('#creartiposervicio').focus();
					}
					if($scope.tiposervicios == 2){
						// console.log(respuesta);
						$scope.acordeon();
						$scope.closeModal();
						alertify.success("Información registrada con éxito");
						$scope.ListadoTipoServicioSelect();
					}
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}


// FUNCION PARA CONSULTAR LOS PRODUCTOS DE SERVICIO SEGUN TIPO DE SERVICIO SELECCIONADO PARA MOSTRAR EN ACORDEON DE GESTION DE SERVICIOS *****************************************

	$scope.ProductoServicio = function(tiposerv){

		ListGenerales.ProductService(tiposerv).success(function(data){

			$scope.elementos = data;
			// console.log(data);

			if($scope.elementos == ''){
				$scope.vistanoproduct = true;
				// console.log('no hay elementos')
			}
			else{
				$scope.vistanoproduct = false;
			}

		}).error(function(error){
			console.log("Error."+error);
		});
	}



// FUNCION PARA AGREGAR PRODUCTO A UN TIPO DE SERVICIO
	
	$scope.AgregarProductService = function(){

		var idtipo_servicio = $scope.idtipo_servicio
		var idproductos = $scope.idproductos

		// console.log(datos);

		alertify.confirm("¿Deseas registrar la información?", function (info) {
				
			if(info){

				$http({
					method:'POST',
					url:'../web/includes/sql/Control/GestionServicios/Registrar_ProductoaServicio_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idtipo_servicio:idtipo_servicio, idproductos:idproductos}
					}).success(function(respuesta){

						// console.log(respuesta);

						$scope.productoservicio = respuesta

						if($scope.productoservicio == 1){

							$scope.ListadoProductoServicioSelect();
							alert('<i class="fa fa-exclamation-triangle error fs50 animated wobble"></i><br>¡Este producto ya existe en el servicio!');
							alertify.error("<b>Info: </b>¡Ya existe este producto!");

						}

						if($scope.productoservicio == 2){

							alert('¡Espere un momento! <br>Se está guardando la información. <br><i class=" mt10 fa fa-spinner fa-spin fa-3x fa-fw"></i>')
							$('.alertuse').css('display','none')
							$scope.acordeon();
							$scope.ProductoServicio(idtipo_servicio);

							setTimeout(function(){
								$scope.closeModal();
								this._alertOverlay.remove();
	        					this._alertWindow.remove();
								alertify.success("<b>Info: </b>Información registrada con éxito");
								$scope.ListadoProductoServicioSelect();
							 }, 2000);

						}

					}).error(function(err){
						console.log("Error: "+err);
				});	
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}


// FUNCION PARA ELIMINAR UN PRODUCTO DEL SERVICIO - GESTION DE SERVICIOS ************************************************************

	$scope.DeleteProductServicio = function(idtipo_servicio, idproductos){

	// console.log('idServicio: ' + idtipo_servicio + ' idProductos: ' +idproductos);

		alertify.confirm("¿Esta seguro de eliminar el registro?", function (info) {

			if(info){

				alert('<i class="fa fa-hand-paper-o fs50 error" aria-hidden="true"></i><br>Espere un momento... <br>Se está eliminando la información<br><i class=" mt10 fa fa-spinner fa-pulse fa-3x fa-fw"></i>')
				$('.alertuse').css('display','none')

				$http({
					method:'POST',
					url:'../web/includes/sql/Control/GestionServicios/Delete_Producto_de_Servicio_sql.php',
					header:{
					'Content-Type': undefined
				},
					data:{idtipo_servicio:idtipo_servicio, idproductos:idproductos}
				}).success(function(respuesta){
					$scope.deleteproductoserv = respuesta;
						// console.log(respuesta)
						setTimeout(function(){
							$('.alertuse').css('display','block')
							$('.textoalert').html('<p class="textoalert">¡Registro Eliminado!<br><i class="fa fa-thumbs-o-up fs50 perfect mt10" aria-hidden="true"></i></p>')
							$scope.ProductoServicio(idtipo_servicio);
							$scope.acordeon();
						 }, 2000);
				}).error(function(err){
						console.log("Error: "+err);
					});
			}else{
				alertify.error("<b>Info: </b>¡Proceso cancelado!");
			}
		});
	}


// FUNCION PARA MOSTRAR NOMBRE Y COD DE TIPO DE SERVICIOS EN MODAL - GESTION DE SERVICIOS ************************************************************

	$scope.MostrarNombreServicio = function(idtipo_servicio, nom_tipo){

		$scope.nom_tiposerv = nom_tipo;
		$scope.idtipo_serv = idtipo_servicio;

		$('.fa-check').hide();

		// console.log('idtipo_servicio: ' + idtipo_servicio +' nom_tipo::::: ' + $scope.nom_tiposerv);

	}	

	// End funcion $scope.MostrarNombreServicio



// FUNCION PARA EDITAR EL NOMBRE DE UN TIPO DE SERVICIO ************************************************************

	$('.fa-pulse, #update, .fa-check').hide();

	$scope.EditTipoServicio = function(){
		
		var nom_tipo = $scope.nom_tiposerv;
		var idtipo_servicio = $scope.idtipo_serv;

		// console.log('EDITAR: idtipo_servicio: ' + idtipo_servicio +' nom_tipo::::: ' + $scope.nom_tiposerv);

		$('.fa-pulse, #update').show();
		$('.fa-check').hide();

		datos = {
			nom_tipo:nom_tipo,
			idtipo_servicio:idtipo_servicio
		}

		// console.log(datos);

		$http({
			method:'POST',
			url:'../web/includes/sql/Control/GestionServicios/Editar_TipoServicio_sql.php',
			header:{
			'Content-Type': undefined
		},
			data:datos
		}).success(function(respuesta){

			$scope.tiposervicio = respuesta;
			// console.log(respuesta);

			if($scope.tiposervicio == 1){

				setTimeout(function(){
					$('.fa-pulse, #update').hide();
					$('.fa-check').show();
					$('#cantidad, #total').val('');
					alertify.success("Información actualizada con éxito");
					$scope.ListadoTipoServicioSelect();
					$scope.acordeon();
				 }, 2000);
			}else{
			      alertify.error("Error al actualizar");

			}

		}).error(function(err){
				console.log("Error: "+err);
			});
	}	










}]); // End Controller ControlCtrl

// Controller CONTROL //*********************************************************************************************