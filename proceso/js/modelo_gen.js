var appGeneral = angular.module("PruebaMVC",['ngRoute']);

    // Variable donde se almacenan los sql
    var request = '../request/sql/';

    appGeneral.config(function($routeProvider) {
        
        $routeProvider

        .when("/", {
            templateUrl : "lista_procesos.php"
            ,controller : "CtrlProcesos"
        })

        .when("/nuevo_proceso/", {
            templateUrl : "nuevo_proceso.php"
            ,controller : "CtrlProcesos"
        })

        .otherwise({
            templateUrl : "../"
        });

});


// SERVICIOS DE PROCESOS

appGeneral.factory('Proceso',['$http', function($http){

    return {

        list_proceso_usuario: function(id_usuario){
            return $http({
                method:'POST',
                url: request + 'proceso/proceso_usuario.php',
                headers:{
                    'Content-Type': undefined
                },
                data:{id_usuario:id_usuario}
            })
        },

        listado_procesos_gen: function(){
            return $http({
                method:'POST',
                url: request + 'proceso/listado_procesos_gen.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

        ultimo_proceso: function(){
            return $http({
                method:'POST',
                url: request + 'proceso/ultimo_proceso_registrado.php',
                headers:{
                    'Content-Type': undefined
                },
            })
        },

    } // End Return

}]);