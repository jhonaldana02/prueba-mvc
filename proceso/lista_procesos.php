<?php include('../request/sql/sesion.php'); ?>
<div class="wrapper">
  <nav class="navbar navbar-inverse">
    <input type="hidden" id="id_usuario" value="<?php echo $session ?>" name="">
    <div class="container-fluid">
      <div class="navbar-header txtwhite">
          <h3><span class="glyphicon glyphicon-home"></span>Bienvenido</h3>
      </div>
    </div>
  </nav>

  <!-- Mi proceso -->
  <div class="container" ng-init="proceso_usuario()">
    <div class="panel panel-default">
      <div class="panel-body">
        <h3>Mi proceso</h3>     
        <table class="table table-striped mt30" ng-show="viewproceso">
          <thead >
            <tr>
              <th scope="col">N° PROCESO</th>
              <th scope="col">FECHA Y HORA</th>
              <th scope="col">SEDE</th>
              <th scope="col">PRESUPUESTO</th>
              <th scope="col">ACCIÓN</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat = "procesouser in procesousers">
              <!-- <th scope="row">1</th> -->
              <td>{{procesouser.NUM_PROCESO}}</td>
              <td>{{procesouser.FECHA_CREACION}}</td>
              <td>{{procesouser.SEDE}}</td>
              <td>$ {{procesouser.PRESUPUESTO}}</td>
              <td>
                <a class="btn btn-primary active" role="button" aria-pressed="true">Ver</a>
                <a class="btn btn-primary active" role="button" aria-pressed="true">Editar</a>
              </td>
            </tr>
          </tbody>
        </table>
        <span ng-show="noproceso">No ha registrado proceso
          <a href="#/nuevo_proceso" class="btn btn-primary active" role="button" aria-pressed="true">Crear proceso</a>
        </span>
      </div>
    </div>
  </div>

  <!-- Lista de procesos -->
  <div class="container" ng-init="listado_procesos()">
    <div class="panel panel-default">
      <div class="panel-body">
        <h3>Lista de procesos</h3>    
        <table class="table table-striped" ng-show="viewprocesogen">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">N° PROCESO</th>
              <th scope="col">NOMBRES Y APELLIDOS</th>
              <th scope="col">FECHA Y HORA</th>
              <th scope="col">SEDE</th>
              <th scope="col">PRESUPUESTO</th>
              <th scope="col">ACCIÓN</th>
            </tr>
          </thead>
          <tbody>
            <tr ng-repeat = "listadoproceso in listadoprocesos">
              <th scope="row">{{$index+1}}</th>
              <td>{{listadoproceso.NUM_PROCESO}}</td>
              <td>{{listadoproceso.NOMBRES}} {{listadoproceso.APELLIDOS}}</td>
              <td>{{listadoproceso.FECHA_CREACION}}</td>
              <td>{{listadoproceso.SEDE}}</td>
              <td>$ {{listadoproceso.PRESUPUESTO}}</td>
              <td><a class="btn btn-primary active" role="button" aria-pressed="true">Ver</a></td>
            </tr>
          </tbody>
        </table>
        <span ng-show="noprocesogen">No existen procesos registrados.        
        </span>
      </div>
    </div>
  </div>
</div>