<?php include('../request/sql/sesion.php'); ?>
 <div class="wrapper">
    <nav class="navbar navbar-inverse">
      <input type="hidden" id="id_usuario" value="<?php echo $session ?>" name="">
  <div class="container-fluid">
    <div class="navbar-header txtwhite">
        <!-- <img alt="Cognox" src="http://www.cognox.com/sitios/Cognox/IMG/logCx.png"> -->
        <h3><span class="glyphicon glyphicon-pencil"></span> Crear nuevo proceso</h3>
    </div>
  </div>
</nav>

<div class="container">
  <div class="panel panel-default">
    <div class="panel-body">
      <form class="form-horizontal">
        <div class="form-group" ng-init="validar_ultimo_proceso()">
          <label for="proceso" class="col-sm-5 control-label">Proceso N°</label>
          <div class="col-sm-4">
            <input type="text" class="form-control" id="proceso" readonly="readonly" value="{{ultimo_proceso}}">
          </div>
        </div>
        <div class="form-group">
          <label for="descipcion" class="col-sm-5 control-label">Descripción</label>
          <div class="col-sm-4">
            <textarea class="form-control" rows="5" id="descripcion" ng-model="descripcion" maxlength="200"></textarea>
          Máximo 200 caracteres
          </div>
        </div>
        <div class="form-group">
          <label for="sede" class="col-sm-5 control-label">Sede</label>
          <div class="col-sm-4">
            <select name="sede" id="sede" class="form-control" ng-model="sede">
              <option value="Bogotá">Bogotá</option>
              <option value="México">México</option>
              <option value="Perú">Perú</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="presupuesto" class="col-sm-5 control-label">Presupuesto</label>
          <div class="col-sm-4">
            <input type="text" class="form-control valor" id="presupuesto" ng-model="presupuesto">
          </div>
        </div>    
        
        <div class="form-group">
          <div class="col-sm-12 text-center">
              <a href="" class="btn btn-primary active" role="button" aria-pressed="true" ng-click="nuevo_proceso()">Guardar</a>
          </div>
        </div>
      </form>
    </div>
  </div>


  <script>
    function conComas(valor) {

      var nums = new Array();
      var simb = ","; //Éste es el separador
      valor = valor.toString();
      valor = valor.replace(/\D/g, "");   //Ésta expresión regular solo permitira ingresar números
      nums = valor.split(""); //Se vacia el valor en un arreglo
      var long = nums.length - 1; // Se saca la longitud del arreglo
      var patron = 3; //Indica cada cuanto se ponen las comas
      var prox = 2; // Indica en que lugar se debe insertar la siguiente coma
      var res = "";
   
      while (long > prox) {
          nums.splice((long - prox),0,simb); //Se agrega la coma
          prox += patron; //Se incrementa la posición próxima para colocar la coma
      }
   
      for (var i = 0; i <= nums.length-1; i++) {
          res += nums[i]; //Se crea la nueva cadena para devolver el valor formateado
      }
   
      return res;
  }


  $(".valor").keyup(function () {
        var valActual = $(this).val();
        var nuevoValor = conComas(valActual);
        // console.log(nuevoValor);
        $(this).val(nuevoValor);
    });
  </script>